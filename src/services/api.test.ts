/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the api interface
 * @copyright &copy; 2018 R. Matt McCann
 */

import mockAxios from 'mocks/mock-axios';
import MockFirestore from 'mocks/mock-firestore';

import apiImpl, {IApi, IGaEvent, IMessage, IProject, ITestimonial} from './api';


describe('services/api', () => {
  let api: IApi;
  let axios: any;
  let firestore: MockFirestore;

  beforeEach(() => {
    axios = mockAxios();
    firestore = new MockFirestore();
    api = apiImpl(axios, firestore);
  });

  describe('googleAnalytics', () => {
    describe('list', () => {
      it('fetches the mappings', async () => {
        const entries: IGaEvent[] = [{
          action: 'build',
          category: 'home',
          type: 'bob_villa',
        }, {
          action: 'bid',
          category: 'detergent',
          type: 'bob_barker',
        }];
        const expectedMapping = new Map<string, IGaEvent>();
        let key = 'bob_barker';
        expectedMapping[key] = entries[1];
        key = 'bob_villa';
        expectedMapping[key] = entries[0];
        firestore.data = { googleAnalytics: entries };

        const mappings = await api.googleAnalytics().list();

        expect(mappings).toEqual(expectedMapping);
      });
    });
  });

  describe('messages', () => {
    describe('create', () => {
      it('creates the message', async () => {
        const message: IMessage = {
          email: 'bob@thisoldehome.org',
          message: 'Something something',
          name: 'Bob Villa',
        };

        await api.messages().create(message);

        expect(firestore.added).toEqual([message]);
      });
    });
  });

  describe('projects', () => {
    describe('list', () => {
      it('fetches the projects', async () => {
        const expectedProjects: IProject[] = [{
          $id: '0',
          backgroundImageType: 'png',
          backgroundImageUrl: 'https://imgs.com/backgroundImageUrl2',
          briefDescription: 'A brief description2',
          company: 'This Olde Home2',
          companyTitle: 'Chief Handyman2',
          dateRange: 'September 2050 - Present2',
          description: 'Something something lorem ipsum2',
          tags: ['juggling', 'striving', 'hacking2'],
          title: 'Rebuilt This Olde Home2',
        }];
        firestore.data = { projects: expectedProjects };

        const projects = await api.projects().list();

        expect(projects).toEqual(expectedProjects);
      });
    });
  });

  describe('tags', () => {
    describe('suggest', () => {
      it('fetches the tag suggestions', async () => {
        axios.get.mockReturnValue(Promise.resolve({ data: ['tag1', 'tag2'] }));

        const tags = await api.tags().suggest('C+');

        expect(axios.get).toHaveBeenCalledWith('/tags/C+');
        expect(tags).toEqual(['tag1', 'tag2']);
      });
    });
  });

  describe('testimonials', () => {
    describe('list', () => {
      it('fetches the testimonials', async () => {
        const expectedTestimonials: ITestimonial[] = [{
          '$id': '0',
          author: 'Bob Villa',
          company: 'This Olde Home',
          photo: 'bob.jpg',
          quote: 'Something something',
          title: 'Master Craftsmen',
        }];
        firestore.data = { testimonials: expectedTestimonials };

        const testimonials = await api.testimonials().list();

        expect(testimonials).toEqual(expectedTestimonials);
      });
    });
  });
});
