/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Firestore helpers for standard CRUD operations
 * @copyright &copy; 2018 R. Matt McCann
 */

// TODO(mmccann): Get proper typing for the collectionRef
/**
 * Generic implementation of listing all records in a Firestore collection.
 *
 * @template T The Type of the collection record
 * @param collectionRef Reference to the collection to be listed
 * @returns The list function
 */
export function list<T>(collectionRef: any) {
  return async (): Promise<T[]> => {
    const listedResource = [] as T[];
    const query = await collectionRef.get();

    query.forEach((record: any) => listedResource.push({
      ...record.data(),
      $id: record.id,
    }));

    return listedResource;
  };
}
