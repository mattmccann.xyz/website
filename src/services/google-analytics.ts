/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Interface for dispatching Google Analytics events
 */


export interface IGa {
  event: (event: any) => void;
  initialize: (trackingId: string, options?: any) => void;
  pageview: (pageview: string) => void;
  set: (map: any) => void;
}

export default class GoogleAnalytics {
  private ga: IGa;

  constructor(ga: IGa) {
    this.ga = ga;

    const trackingId = process.env.NODE_ENV === 'production' ? 'UA-130186226-1' : 'UA-130186226-2';
    const isTestMode = process.env.NODE_ENV === 'test';
    ga.initialize(trackingId, { testMode: isTestMode });
  }

  /**
   * Sends the event to Google Analytics.
   *
   * @param category The event category label e.g. 'register_account'
   * @param action e.g. 'submit'
   */
  public sendEvent(category: string, action: string): void {
    this.ga.event({
      action: String(action),
      category: String(category),
      dimension3: new Date().getTime() ,
    });
  }

  /**
   * Sends a page view to Google Analytics.
   */
  public sendPageView(pageview: string): void {
    this.ga.pageview(pageview);
  }

  /**
   * Sets a tracker property.
   *
   * @param key Property to be set
   * @param value Value to set the property to
   */
  public set(key: string, value: string): void {
    const map = {};
    map[key] = value;

    this.ga.set(map);
  }
}
