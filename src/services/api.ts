/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Service interface for making requests of the mattmccann.xyz api
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as crudHelpers from './crud-helpers';


export interface IApi {
  googleAnalytics: () => IGoogleAnalytics;
  messages: () => IMessages;
  projects: () => IProjects;
  tags: () => ITags;
  testimonials: () => ITestimonials;
}

export interface IGoogleAnalytics {
  list: () => Promise<Map<string, IGaEvent>>;
}

export interface IGaEvent {
  action: string;
  category: string;
  type: string;
}

export interface IMessage {
  email: string;
  message: string;
  name: string;
}

export interface IMessages {
  create: (message: IMessage) => Promise<void>;
}

export interface IProject {
  $id: string;
  backgroundImageType: string;
  backgroundImageUrl: string;
  briefDescription: string;
  company?: string;
  companyTitle?: string;
  dateRange: string;
  description: string;
  tags: string[];
  title: string;
}

export interface IProjects {
  list: () => Promise<IProject[]>;
}

export interface ITags {
  suggest: (tagFragment: string) => Promise<string[]>;
}

export interface ITestimonial {
  /** Tracking ID of the record within Firebase */
  $id: string;

  /** Name of the author */
  author: string;

  /** Company the author works for */
  company: string;

  /** Photo of the author */
  photo: string;

  /** The testimonial quote itself */
  quote: string;

  /** Title of the author at her company */
  title: string;
}

export interface ITestimonials {
  /** Lists the testimonial records */
  list: () => Promise<ITestimonial[]>;
}

/**
 * Interface for making requests of the mattmccann.xyz api.
 *
 * @param axiosHttp Axios HTTP request library
 * @param firestore Interface for directly querying the Firestore database
 */
export default (axios: any, firestore: any): IApi => ({
  googleAnalytics: (): IGoogleAnalytics => {
    const ref = firestore.collection('googleAnalytics');

    return {
      list: async (): Promise<Map<string, IGaEvent>> => {
        const events: IGaEvent[] = await crudHelpers.list<IGaEvent>(ref)();

        const mappedEvents: Map<string, IGaEvent> = new Map<string, IGaEvent>();
        events.forEach((event: IGaEvent) => {
          mappedEvents[event.type] = event;
        });

        return mappedEvents;
      },
    };
  },

  messages: (): IMessages => {
    const ref = firestore.collection('messages');

    return {
      create: async (message: IMessage): Promise<void> => {
        return ref.add(message);
      }
    }
  },

  projects: (): IProjects => {
    const ref = firestore.collection('projects');

    return {
      list: async () => {
        const projects = [] as IProject[];
        const query = await ref
          .where('visible', '==', true)
          .orderBy('startDate', 'desc')
          .get();

        query.forEach((record: any) => projects.push({
          ...record.data(),
          $id: record.id,
        }));

        return projects;
      }
    };
  },

  tags: (): ITags => ({
    /**
     * Returns a list of tags matching the provided tag fragment.
     *
     * @param tagFragment A partial tag label to match
     * @returns A list of matching tag labels
     */
    suggest: async (tagFragment: string): Promise<string[]> => {
      const response = await axios.get(`/tags/${tagFragment}`);

      return response.data;
    },
  }),

  testimonials: (): ITestimonials => {
    const ref = firestore.collection('testimonials');

    return {
      list: crudHelpers.list<ITestimonial>(ref),
    };
  },
});
