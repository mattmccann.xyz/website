/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the GoogleAnalytics service
 */

import GoogleAnalytics, { IGa } from './google-analytics';


describe('services/google-analytics', () => {
  let ga: IGa;
  let googleAnalytics: GoogleAnalytics;

  beforeEach(() => {
    ga = {
      event: jest.fn(),
      initialize: jest.fn(),
      pageview: jest.fn(),
      set: jest.fn(),
    };
    googleAnalytics = new GoogleAnalytics(ga);
  });

  it('initialize ga', () => {
    expect((ga.initialize as any)).toHaveBeenCalledWith('UA-130186226-2', { testMode: true });
  });

  it('sends events', () => {
    googleAnalytics.sendEvent('some-category', 'some-action');

    expect((ga.event as any).mock.calls[0][0].category).toEqual('some-category');
    expect((ga.event as any).mock.calls[0][0].action).toEqual('some-action');
    expect((ga.event as any).mock.calls[0][0].dimension3).toBeDefined();
  });

  it('sends pageviews', () => {
    googleAnalytics.sendPageView('/thePage');

    expect((ga.pageview as any).mock.calls[0][0]).toEqual('/thePage');
  });

  it('sets properties', () => {
    googleAnalytics.set('bob', 'villa');

    expect((ga.set as any).mock.calls[0][0]).toEqual({ bob: 'villa'});
  });
});
