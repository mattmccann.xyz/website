/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the App container
 * @copyright &copy; 2018 R. Matt McCann
 */

import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';

import { App, AppContainer } from 'app';


describe('app/container', () => {
  let component: ShallowWrapper;
  let instance: AppContainer;
  let listenForResize: any;
  let listenForScroll: any;
  let unlistenForResize: any;
  let unlistenForScroll: any;

  beforeEach(() => {
    listenForResize = jest.fn();
    listenForScroll = jest.fn();
    unlistenForResize = jest.fn();
    unlistenForScroll = jest.fn();

    component = shallow(<AppContainer
      listenForResize={listenForResize}
      listenForScroll={listenForScroll}
      location={"/derp"}
      unlistenForResize={unlistenForResize}
      unlistenForScroll={unlistenForScroll}
    />);
    instance = component.instance() as AppContainer;
  });

  it('app renders as expected', () => {
    expect(shallow(<App />)).toMatchSnapshot();
  });

  it('AppContainer renders as expected', () => {
    expect(component).toMatchSnapshot();
  });

  it('attaches listeners on mount', () => {
    instance.componentDidMount();

    expect(listenForResize).toHaveBeenCalled();
    expect(listenForScroll).toHaveBeenCalled();
  });

  it('detaches listeners on unmount', () => {
    instance.componentWillUnmount();

    expect(listenForResize).toHaveBeenCalled();
    expect(listenForScroll).toHaveBeenCalled();
  });
});
