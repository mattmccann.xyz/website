/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Container for the App
 * @copyright &copy; 2018 R. Matt McCann
 */

import { ConnectedRouter } from 'connected-react-router';
import * as React from 'react';
import { connect, Provider } from 'react-redux';

import { configureStore, history } from 'state/configure-store';
import * as deviceActions from 'state/device/device.actions';
import { IState } from 'state/state.type';

import AppView, { IViewProps } from './app.view';


interface IAppProps extends IViewProps {
  listenForResize: () => void;
  listenForScroll: () => void;
  unlistenForResize: () => void;
  unlistenForScroll: () => void;
}

export class AppContainer extends React.Component<IAppProps> {
  public componentDidMount() {
    this.props.listenForResize();
    this.props.listenForScroll();
  }

  public componentWillUnmount() {
    this.props.unlistenForResize();
    this.props.unlistenForScroll();
  }

  public render() {
    return <AppView {...this.props} />;
  }
}

const mapDispatchToProps = {
  ...deviceActions,
};

const mapStateToProps = (state: IState) => ({
  location: state.router.location.pathname,
});

const ConnectedAppContainer = connect(mapStateToProps, mapDispatchToProps)(AppContainer);

// Build the store we will use with the app
const store = configureStore();

export default () => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <ConnectedAppContainer />
    </ConnectedRouter>
  </Provider>
);
