import ConnectedAppContainer, { AppContainer } from './app.container';
import AppView from './app.view';

export {
  ConnectedAppContainer as App,
  AppContainer,
  AppView,
}
