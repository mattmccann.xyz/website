/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief View for the App
 * @copyright &copy; 2018 R. Matt McCann
 */

import Radium from 'radium';
import * as React from 'react';
import {Route, Switch} from 'react-router';

import { FourOhFourScreen } from 'screens/four-oh-four-screen';
import { LandingScreen } from 'screens/landing-screen';
import { LetsChatScreen } from 'screens/lets-chat-screen';


export interface IViewProps {
  location: string;
}

export default (props: IViewProps) => (
  <Radium.StyleRoot>
    <Switch>
      <Route exact path="/" component={LandingScreen} key={1} />
      <Route exact path="/about" component={LandingScreen} key={1} />
      <Route exact path="/projects" component={LandingScreen} key={1} />
      <Route exact path="/testimonials" component={LandingScreen} key={1} />
      <Route exact path="/lets-chat" component={LetsChatScreen} />
      <Route component={FourOhFourScreen} />
    </Switch>
  </Radium.StyleRoot>
);
