/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the App views
 * @copyright &copy; 2018 R. Matt McCann
 */

import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';

import { AppView } from 'app';


describe('app/view', () => {
  let component: ShallowWrapper;

  beforeEach(() => {
    component = shallow(<AppView location={"/derp"} />);
  });

  it('renders as expected', () => {
    expect(component).toMatchSnapshot();
  });
});
