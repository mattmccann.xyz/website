/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Various style definitions used through the app
 * @copyright &copy; 2018 R. Matt McCann
 */

export const COLOR_DARK_BASIC = 'rgba(10,10,10,0.9)';
export const COLOR_DARK_PRIMARY = '#8C43FF';
export const COLOR_DARK_SECONDARY = '#293347';
export const COLOR_GRAY_PRIMARY = 'rgb(230, 236, 248)';
export const COLOR_GRAY_DARK = '#4a4a4a';
export const COLOR_LIGHT_PRIMARY = '#FFFFFF';
export const COLOR_LIGHT_SECONDARY = 'rgba(255, 255, 255, 0.7)';
export const COLOR_LIGHT_THIRD = 'rgba(255, 255, 255, 0.3)';
export const COLOR_RED_PRIMARY = 'red';
