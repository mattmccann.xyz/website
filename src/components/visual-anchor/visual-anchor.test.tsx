/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the VisualAnchor component
 * @copyright &copy; 2018 R. Matt McCann
 */

import { shallow } from 'enzyme';
import * as React from 'react';

import { VisualAnchor } from 'components/visual-anchor';


describe('component/visual-anchor', () => {
  it('renders with dark styling', () => {
    const component = shallow(<VisualAnchor />);

    expect(component).toMatchSnapshot();
  });

  it('renders with light styling', () => {
    const component = shallow(<VisualAnchor light />);

    expect(component).toMatchSnapshot();
  });

  it('passes font styling to the icon', () => {
    const component = shallow(<VisualAnchor fontStyle={{color: 'white'}} />);

    expect(component).toMatchSnapshot();
  });

  it('passes container styling to the icon', () => {
    const component = shallow(<VisualAnchor containerStyle={{float: 'left'}} />);

    expect(component).toMatchSnapshot();
  });
});
