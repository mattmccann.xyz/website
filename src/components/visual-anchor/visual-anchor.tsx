/**
 * @blame R. Matt McCann
 * @brief The logo used as a visual anchor at various places in the app
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as React from 'react';

import * as v from 'styles/variables';


/** Properties definition for the VisualAnchor component */
interface IPropTypes {
  /** Additional styling for the visual anchor container */
  containerStyle?: React.CSSProperties;

  /** Additional styling for the visual anchor font */
  fontStyle?: React.CSSProperties;

  /** The DOM id of the Link */
  id?: string;

  /** If True, uses the light styling */
  light?: boolean;

  /** Callback handler called when the visual anchor is clicked */
  onClick?: () => void;
}

export default (props: IPropTypes) => {
  // Default to dark styling
  const style = {
    color: v.COLOR_DARK_PRIMARY,
    cursor: 'pointer',
    fontSize: '3em',
  } as React.CSSProperties;

  // If requested, use the light styling
  if (props.light) {
    style.color = v.COLOR_LIGHT_PRIMARY;
  }

  return (
    <div id={props.id} onClick={props.onClick} style={props.containerStyle} >
      <i className="fas fa-code" style={{...style, ...props.fontStyle}}/>
    </div>
  );
}
