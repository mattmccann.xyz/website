/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the contracted ProjectCard view
 * @copyright &copy; 2019 R. Matt McCann
 */

import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';

import { IProject } from 'services/api';

import ContractedProjectCard from './contracted-project-card';


describe('components/project-card/contracted-project-card', () => {
  let component: ShallowWrapper;
  let onClickReadMore: (projectId: string) => void;
  let onMouseEnter: (projectId: string) => void;
  let onMouseLeave: (projectId: string) => void;
  let project: IProject;

  beforeEach(() => {
    onClickReadMore = jest.fn();
    onMouseEnter = jest.fn();
    onMouseLeave = jest.fn();
    project = {
      $id: 'projectId',
      backgroundImageType: 'png',
      backgroundImageUrl: 'https://imgs.com/backgroundImageUrl',
      briefDescription: 'A brief description',
      company: 'This Olde Home',
      companyTitle: 'Chief Handyman',
      dateRange: 'September 2050 - Present',
      description: 'Something something lorem ipsum',
      tags: ['juggling', 'striving', 'hacking'],
      title: 'Rebuilt This Olde Home',
    };

    component = shallow(<ContractedProjectCard
      isExpanded={false} isOverlayRevealed={false} onClickReadMore={onClickReadMore}
      onMouseEnter={onMouseEnter} onMouseLeave={onMouseLeave} project={project}
    />);
  });

  describe('is contracted', () => {
    beforeEach(() => {
      component.setProps({ isExpanded: false });
    });

    describe('is overlayed', () => {
      beforeEach(() => {
        component.setProps({ isOverlayRevealed: true });
      });

      it('renders the overlay when overlayed', () => {
        expect(component).toMatchSnapshot();
      });

      it('dispatches the onClickReadMore handler when Read More button is clicked', () => {
        component.find('[id="readMore"]').simulate('click');

        expect(onClickReadMore).toHaveBeenCalledWith('projectId');
      });

      it('dispatches mouse leave handler when the mouse leaves project card', () => {
        component.find('[id="mouseHandler"]').simulate('mouseLeave');

        expect(onMouseLeave).toBeCalledWith('projectId');
      });
    });

    describe('is not overlayed', () => {
      beforeEach(() => {
        component.setProps({ isOverlayRevealed: false });
      });

      it('renders the non-overlayed view', () => {
        expect(component).toMatchSnapshot();
      });

      it('dispatches mouse enter handler when the user clicks the project card', () => {
        component.find('[id="mouseHandler"]').simulate('click');

        expect(onMouseEnter).toHaveBeenCalledWith('projectId');
      });

      it('dispatches mouse enter handler when the mouse enters project card', () => {
        component.find('[id="mouseHandler"]').simulate('mouseEnter');

        expect(onMouseEnter).toHaveBeenCalledWith('projectId');
      });

      it('dispatches mouse leave handler when the mouse leaves project card', () => {
        component.find('[id="mouseHandler"]').simulate('mouseLeave');

        expect(onMouseLeave).toBeCalledWith('projectId');
      });
    });
  });

  describe('is expanded', () => {
    beforeEach(() => {
      component.setProps({ isExpanded: true });
    });

    it('should set the display as none', () => {
      expect(component).toMatchSnapshot();
    });
  })
});
