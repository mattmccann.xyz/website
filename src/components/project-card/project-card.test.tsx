/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the ProjectCard component
 * @copyright &copy; 2019 R. Matt McCann
 */

import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';

import ProjectCard from 'components/project-card';
import { IProject } from 'services/api';


describe('components/project-card', () => {
  let component: ShallowWrapper;
  let onClickReadMore: (projectId: string) => void;
  let onMouseEnter: (projectId: string) => void;
  let onMouseLeave: (projectId: string) => void;
  let project: IProject;

  beforeEach(() => {
    onClickReadMore = jest.fn();
    onMouseEnter = jest.fn();
    onMouseLeave = jest.fn();
    project = {
      $id: 'projectId',
      backgroundImageType: 'png',
      backgroundImageUrl: 'https://imgs.com/backgroundImageUrl',
      briefDescription: 'A brief description',
      company: 'This Olde Home',
      companyTitle: 'Chief Handyman',
      dateRange: 'September 2050 - Present',
      description: 'Something something lorem ipsum',
      tags: ['juggling', 'striving', 'hacking'],
      title: 'Rebuilt This Olde Home',
    };

    component = shallow(<ProjectCard
      isExpanded={false} isOverlayRevealed={false} onClickReadMore={onClickReadMore}
      onMouseEnter={onMouseEnter} onMouseLeave={onMouseLeave} project={project}
    />);
  });

  it('should render both the expanded and contract project cards', () => {
    expect(component).toMatchSnapshot();
  });
});
