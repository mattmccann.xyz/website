/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the expanded ProjectCard component
 * @copyright &copy; 2019 R. Matt McCann
 */

import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';

import { IProject } from 'services/api';

import ExpandedProjectCard from './expanded-project-card';


describe('components/project-card/expanded-project-card', () => {
  let component: ShallowWrapper;
  let project: IProject;

  beforeEach(() => {
    project = {
      $id: 'projectId',
      backgroundImageType: 'png',
      backgroundImageUrl: 'https://imgs.com/backgroundImageUrl',
      briefDescription: 'A brief description',
      company: 'This Olde Home',
      companyTitle: 'Chief Handyman',
      dateRange: 'September 2050 - Present',
      description: 'Something something lorem ipsum',
      tags: ['juggling', 'striving', 'hacking'],
      title: 'Rebuilt This Olde Home',
    };

    component = shallow(<ExpandedProjectCard isExpanded={false} project={project} />);
  });

  describe('is contracted', () => {
    beforeEach(() => {
      component.setProps({ isExpanded: false });
    });

    it('should render with display set to none', () => {
      expect(component).toMatchSnapshot();
    });
  });

  describe('is expanded', () => {
    beforeEach(() => {
      component.setProps({ isExpanded: true });
    });

    it('should render with display set to block', () => {
      expect(component).toMatchSnapshot();
    });

    describe('details', () => {
      it('does not render the company detail when no company', () => {
        const newProject = { ...project };
        delete newProject.company;
        component.setProps({ project: newProject });

        expect(component).toMatchSnapshot();
      });

      it('does not render the company title when no company title detail', () => {
        const newProject = { ...project };
        delete newProject.companyTitle;
        component.setProps({ project: newProject });

        expect(component).toMatchSnapshot();
      });
    });

    describe('description', () => {
      it('renders single paragraph description', () => {
        expect(component).toMatchSnapshot();
      });

      it('renders multiple paragraph description', () => {
        const newProject = {
          ...project,
          description: "p1<br />p2<br />p3",
        };
        component.setProps({ project: newProject });

        expect(component).toMatchSnapshot();
      });
    });

    describe('skill tags', () => {
      it('sorts the tags before rendering', () => {
        expect(component).toMatchSnapshot();
      });
    });
  });
});
