/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Styles for the contracted ProjectCard component
 * @copyright &copy; 2018 R. Matt McCann
 */

import { CSSProperties } from 'react';


export default {
  colAdjust: {
    paddingLeft: '12px',
    paddingRight: '12px',
  } as CSSProperties,

  containerInner: (backgroundImageUrl: string) => ({
    backgroundImage: `url(${backgroundImageUrl})`,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    borderRadius: '15px',
    height: '100%',
    width: '100%',
  } as CSSProperties),

  containerOuter: {
    height: '100%',
    left: 0,
    paddingBottom: '12px',
    paddingTop: '12px',
    position: 'absolute',
    top: 0,
    width: '100%',
  } as CSSProperties,

  containerRatio: {
    minHeight: '227px',
    paddingTop: '73%',
    position: 'relative',
  } as CSSProperties,

  overlay: {
    briefDescription: {
      color: 'white',
      fontFamily: 'Quicksand, sans-serif',
      fontSize: '1.3em',
      marginBottom: '1em',
      textAlign: 'center',
    } as CSSProperties,

    containerInner: {
      left: '50%',
      position: 'absolute',
      top: '50%',
      transform: 'translate(-50%, -50%)',
      width: '80%',
    } as CSSProperties,

    containerOuter: {
      backgroundColor: 'rgba(0, 0, 0, 0.7)',
      borderRadius: '15px',
      height: '100%',
      position: 'relative',
      width: '100%',
    } as CSSProperties,

    readMoreButton: {
      color: 'white',
      marginLeft: 'auto',
      marginRight: 'auto',
      width: '10em',
    } as CSSProperties,
  },
}
