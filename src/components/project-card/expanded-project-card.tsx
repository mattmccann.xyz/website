/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Expanded view for the ProjectCard component
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as React from 'react';
import * as ReactMarkdown from 'react-markdown';

import Image from 'components/image';
import SkillTag from 'components/skill-tag';
import { IProject } from 'services/api';

import styles from './expanded-project-card.styles';


interface IExpandedProjectCardProps {
  isExpanded: boolean;
  project: IProject,
}

export default class ExpandedProjectCard extends React.PureComponent<IExpandedProjectCardProps> {
  public render() {
    const {
      backgroundImageType, backgroundImageUrl, company, companyTitle, dateRange, description, title
    } = this.props.project;

    const descriptionAdj = description.replace(/<br \/>/gm, '\n');

    const style = {
      ...styles.colAdjust,
      display: this.props.isExpanded ? 'block': 'none',
    };

    return (
      <div className="col-12" style={ style }>
        <div style={styles.expanded.containerOuter}>
          <div style={styles.expanded.containerInner}>
            <div className="row">
              <div className="col-sm-12 col-md-5 order-2 order-md-1">
                <div className="row">
                  <div className="col-12 d-none d-sm-none d-md-block">
                    <Image style={styles.expanded.image} url={backgroundImageUrl} alt={title} type={backgroundImageType} />
                  </div>
                </div>
                <div className="row">
                  <div className="col-12" style={styles.expanded.detail.container}>
                    <span style={styles.expanded.detail.label}>Dates Worked:</span> { dateRange }
                  </div>
                </div>
                { !!company && <div className="row">
                  <div className="col-12" style={styles.expanded.detail.container}>
                    <span style={styles.expanded.detail.label}>Company:</span> { company }
                  </div>
                </div> }
                { !!companyTitle && <div className="row">
                  <div className="col-12" style={styles.expanded.detail.container}>
                    <span style={styles.expanded.detail.label}>Title:</span> { companyTitle }
                  </div>
                </div> }
                { this.renderTags() }
              </div>
              <div className="col-sm-12 col-md-7 order-1 order-md-2">
                <div className="row">
                  <div className="col-12 text-center">
                    <h2 style={styles.expanded.title}>{ title }</h2>
                  </div>
                </div>
                <div className="col-12 d-block d-md-none">
                  <Image style={styles.expanded.image} url={backgroundImageUrl} alt={title} type={backgroundImageType} />
                </div>
                <div className="row">
                  <div className="col-12" style={styles.expanded.description}>
                    <ReactMarkdown source={descriptionAdj} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  private renderTags = () => {
    const { tags } = this.props.project;

    const sortedTags = [...tags].sort();

    return (
      <div className="row">
        <div className="col-12" style={styles.expanded.tagsContainer}>
          { sortedTags.map(tag => <SkillTag key={tag} skill={tag} />) }
        </div>
      </div>
    );
  };
}
