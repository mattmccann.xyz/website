/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief View for the ProjectCard component
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as React from 'react';

import { IProject } from 'services/api';

import ContractedProjectCard from './contracted-project-card';
import ExpandedProjectCard from './expanded-project-card';


interface IProjectCardProps {
  isExpanded: boolean;
  isOverlayRevealed: boolean;
  onClickReadMore: (projectId: string) => void;
  onMouseEnter: (projectId: string) => void;
  onMouseLeave: (projectId: string) => void;
  project: IProject,
}

export default class ProjectCard extends React.PureComponent<IProjectCardProps> {
  public render() {
    return (
      [
        <ExpandedProjectCard {...this.props} key={0} />,
        <ContractedProjectCard {...this.props} key={1} />
      ]
    );
  }
};

