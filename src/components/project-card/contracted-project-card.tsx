/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Contracted view for the ProjectCard component
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as React from 'react';

import { Button } from 'components/button';
import { IProject } from 'services/api';

import styles from './contracted-project-card.styles';


interface IContractedProjectCardProps {
  isExpanded: boolean;
  isOverlayRevealed: boolean;
  onClickReadMore: (projectId: string) => void;
  onMouseEnter: (projectId: string) => void;
  onMouseLeave: (projectId: string) => void;
  project: IProject,
}

export default class ContractedProjectCard extends React.PureComponent<IContractedProjectCardProps> {
  public render() {
    const backgroundImageUrl = this.props.project.backgroundImageUrl.replace(/%TYPE/, 'png');

    const style = {
      ...styles.colAdjust,
      display: this.props.isExpanded ? 'none': 'block',
    };

    return (
      <div className="col-sm-6 col-lg-4" style={style}>
        <div id="mouseHandler" style={styles.containerRatio} onClick={this.onMouseEnter}
             onMouseEnter={this.onMouseEnter} onMouseLeave={this.onMouseLeave}>
          <div style={styles.containerOuter}>
            <div style={styles.containerInner(backgroundImageUrl)}>
              {this.renderOverlay()}
            </div>
          </div>
        </div>
      </div>
    );
  };

  private onClickReadMore = () => {
    this.props.onClickReadMore(this.props.project.$id);
  };

  private onMouseEnter = (event: React.FormEvent<HTMLDivElement>) => {
    this.props.onMouseEnter(this.props.project.$id);
  };

  private onMouseLeave = (event: React.FormEvent<HTMLDivElement>) => {
    this.props.onMouseLeave(this.props.project.$id);
  };

  private renderOverlay = () => {
    const { project } = this.props;

    if (this.props.isOverlayRevealed) {
      return (
        <div style={styles.overlay.containerOuter}>
          <div style={styles.overlay.containerInner}>
            <div style={styles.overlay.briefDescription}>
              { project.briefDescription }
            </div>
            <div>
              <Button id="readMore" onClick={this.onClickReadMore} style={styles.overlay.readMoreButton}>
                Read More
              </Button>
            </div>
          </div>
        </div>
      );
    } else {
      return undefined;
    }
  };
}
