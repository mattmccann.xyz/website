/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Styles for the expanded ProjectCard component
 * @copyright &copy; 2018 R. Matt McCann
 */

import { CSSProperties } from 'react';

import * as v from 'styles/variables';


export default {
  colAdjust: {
    paddingLeft: '12px',
    paddingRight: '12px',
  } as CSSProperties,

  expanded: {
    containerInner: {
      borderRadius: '15px',
      height: '100%',
      padding: '15px',
      width: '100%',
    } as CSSProperties,

    containerOuter: {
      backgroundColor: v.COLOR_LIGHT_PRIMARY,
      borderRadius: '15px',
      boxShadow: '0 5px 5px 0 rgba(233,240,243,0.5),0 0 0 1px #E6ECF8',
      marginBottom: '7.5px',
      marginTop: '7.5px',
    } as CSSProperties,

    description: {
      fontFamily: 'Quicksand, sans-serif',
      textAlign: 'justify',
    } as CSSProperties,

    detail: {
      container: {
        paddingBottom: '3px',
        paddingTop: '3px',
      } as CSSProperties,

      label: {
        fontWeight: 800,
      } as CSSProperties,
    },

    image: {
      marginBottom: '10px',
      width: '100%',
    } as CSSProperties,

    tagsContainer: {
      marginLeft: '-5px',
      marginTop: '10px',
    } as CSSProperties,

    title: {
      fontFamily: 'Quicksand, sans-serif',
      marginBottom: '20px',
    } as CSSProperties,
  },
}
