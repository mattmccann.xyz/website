/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief View for the SkillTag component
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as React from 'react';

import styles from './skill-tag.styles';


interface ISkillTag {
  skill: string;
}


export default (props: ISkillTag) => <div style={styles}>{props.skill}</div>;
