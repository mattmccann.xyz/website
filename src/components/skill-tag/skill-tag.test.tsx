/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the SkillTag component
 * @copyright &copy; 2018 R. Matt McCann
 */

import { shallow } from 'enzyme';
import * as React from 'react';

import SkillTag from 'components/skill-tag';


describe('component/skill-tag', () => {
  const props = {
    skill: 'writing-trivial-unit-tests',
  };

  it('renders as expected', () => {
    const component = shallow(<SkillTag {...props} />);

    expect(component).toMatchSnapshot();
  });
});
