/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Styles for the SkillTag component
 * @copyright &copy; 2018 R. Matt McCann
 */

import { CSSProperties } from 'react';

export default {
  backgroundColor: 'green',
  borderRadius: '123345678px',
  color: 'white',
  float: 'left',
  fontSize: '0.8em',
  margin: '5px',
  paddingBottom: '5px',
  paddingLeft: '10px',
  paddingRight: '10px',
  paddingTop: '5px',
} as CSSProperties;
