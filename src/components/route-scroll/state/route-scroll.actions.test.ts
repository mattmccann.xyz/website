/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the RouteScroll redux actions
 * @copyright &copy; 2018 R. Matt McCann
 */

import { push } from 'connected-react-router';

import { CLEAR_REQUEST_SCROLL } from 'features/menu/state/menu.constants';
import mockDispatch from 'mocks/mock-dispatch';
import mockServices from 'mocks/mock-services';
import { Dispatch, GetState, IScroller, IServices } from 'state/thunk.type';

import * as actions from './route-scroll.actions';


describe('components/route-scroll/state/actions', () => {
  let dispatch: Dispatch;
  let domNode: any;
  let getState: GetState;
  let reactDom: any;
  let scroller: IScroller;
  let scrollNode: any;
  let services: IServices;
  let state: any;

  beforeEach(() => {
    getState = jest.fn();
    services = mockServices();
    dispatch = mockDispatch(getState, services);

    state = {
      features: {
        menu: {
          requestedRouteScroll: '/derp',
        },
      },
      router: {
        location: {
          pathname: '/activePath',
        },
      },
    };
    (getState as any).mockReturnValue(state);

    domNode = {
      getBoundingClientRect: jest.fn(),
      scrollIntoView: jest.fn(),
    };
    reactDom = {
      findDOMNode: () => domNode,
    };
    services.reactDOM = reactDom;

    scroller = {
      scrollTo: jest.fn(),
    };
    services.scroller = scroller;

    scrollNode = 'scrollNode!';
  });

  describe('checkForScrolledInto', () => {
    describe('element top is above window', () => {
      describe('element bottom is above window', () => {
        beforeEach(() => {
          domNode.getBoundingClientRect.mockReturnValue({
            bottom: 50,
            top: -1000,
          });
        });

        it('dispatches nothing', () => {
          dispatch(actions.checkForScrolledInto('/myRoute', scrollNode));

          expect(dispatch).toHaveBeenCalledTimes(1);
        });
      });

      describe('element bottom is not above window', () => {
        beforeEach(() => {
          domNode.getBoundingClientRect.mockReturnValue({
            bottom: 500,
            top: 50,
          });
        });

        describe('route scroll is in progress', () => {
          beforeEach(() => {
            state.features.menu.requestedRouteScroll = '/myRoute';
          });

          describe('route scroll is matching', () => {
            it('dispatches the clear request scroll action', () => {
              dispatch(actions.checkForScrolledInto('/myRoute', scrollNode));

              expect(dispatch).toHaveBeenCalledWith({ type: CLEAR_REQUEST_SCROLL });
            });
          });

          describe('route scroll is not matching', () => {
            it('dispatches nothing', () => {
              dispatch(actions.checkForScrolledInto('/notMatching', scrollNode));

              expect(dispatch).toHaveBeenCalledTimes(1);
            });
          });
        });

        describe('route scroll is not in progress', () => {
          beforeEach(() => {
            delete state.features.menu.requestedRouteScroll;
          });

          describe('route is already matched', () => {
            beforeEach(() => {
              state.router.location.pathname = '/myRoute';
            });

            it('dispatches nothing', () => {
              dispatch(actions.checkForScrolledInto('/myRoute', scrollNode));

              expect(dispatch).toHaveBeenCalledTimes(1);
            });
          });

          describe('route is not already matched', () => {
            it('dispatches the route update', () => {
              dispatch(actions.checkForScrolledInto('/newRoute', scrollNode));

              expect(dispatch).toHaveBeenCalledWith(push('/newRoute'));
            });
          });
        });
      });
    });

    describe('element top is not above window', () => {
      beforeEach(() => {
        domNode.getBoundingClientRect.mockReturnValue({
          bottom: 1000,
          top: 250,
        });
      });

      it('dispatches nothing', () => {
        dispatch(actions.checkForScrolledInto('/myRoute', scrollNode));

        expect(dispatch).toHaveBeenCalledTimes(1);
      });
    });
  });

  describe('checkForScrollRequest', () => {
    beforeEach(() => {
      state.features.menu.requestedRouteScroll = '/myRoute';
    });

    describe('route is requested route scroll', () => {
      it('scrolls the node into view', () => {
        dispatch(actions.checkForScrollRequest('/myRoute'));

        expect(scroller.scrollTo).toHaveBeenCalledWith('myRoute-element', {
          duration: 1500,
          offset: -50,
          smooth: true,
        });
      });
    });

    describe('route is not requested route scroll', () => {
      it('does nothing', () => {
        dispatch(actions.checkForScrollRequest('/notMe'));

        expect(dispatch).toHaveBeenCalledTimes(1);
      });
    });
  });
});
