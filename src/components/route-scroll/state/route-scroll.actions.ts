/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Redux actions for RouteScroll
 * @copyright &copy; 2018 R. Matt McCann
 */

import { push } from 'connected-react-router';
import * as React from 'react';

import { clearRequestScroll } from 'features/menu/state/menu.actions';
import { Dispatch, GetState, IServices } from 'state/thunk.type';


export const checkForScrolledInto = (route: string, scrollNode: React.ReactNode) =>
  (dispatch: Dispatch, getState: GetState, services: IServices) => {
    const routeLocation = getState().router.location.pathname;
    const requestedRouteScroll = getState().features.menu.requestedRouteScroll;
    const nodeRef = services.reactDOM.findDOMNode(scrollNode) as HTMLElement;
    const bounds = nodeRef.getBoundingClientRect() as DOMRect;

    // Adding the +50 adjustment to trigger the detection a bit early - looks better
    const isElemTopAboveWindow = bounds.top < 100;
    const isElemBottomAboveWindow = bounds.bottom < 100;

    // If the component is within the viewport
    if (isElemTopAboveWindow && !isElemBottomAboveWindow) {
      const isRouteAlreadyMatched = routeLocation === route;
      const isRouteScrollInProgress = requestedRouteScroll !== undefined;
      const isRouteScrollMatching = requestedRouteScroll === route;

      // If the route scroll is in progress and it's scroll to me
      if (isRouteScrollInProgress && isRouteScrollMatching) {
        // Clear the request now that we'v handled it
        dispatch(clearRequestScroll());
      // If the scroll is not in progress and the route isn't already me
      } else if (!isRouteScrollInProgress && !isRouteAlreadyMatched) {
        dispatch({ type: 'ROUTE_SCROLL_UPDATING_ROUTE' });

        // Update the route to me
        dispatch(push(route));
      }
    }
  };

export const checkForScrollRequest = (route: string) =>
  (dispatch: Dispatch, getState: GetState, services: IServices) => {
    const requestedRouteScroll = getState().features.menu.requestedRouteScroll;

    // If someone has requested we scroll into view
    if (route === requestedRouteScroll) {
      // Pop off the leading forward slash
      const section = requestedRouteScroll.substr(1);
      const scrollElement = section + '-element';

      // Let's scroll into view
      services.scroller.scrollTo(scrollElement, {
        duration: 1500,
        offset: -50,
        smooth: true,
      });
    }
  };
