/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the RouteScroll component
 * @copyright &copy; 2018 R. Matt McCann
 */

import { ShallowWrapper } from 'enzyme';
import * as React from 'react';

import routeScroll from 'components/route-scroll';
import shallowWithStore from 'mocks/shallow-with-store';


describe('components/route-scroll', () => {
  let component: ShallowWrapper;
  let instance: any;

  const wrappedComponent = () => (<div>Derp!</div>);

  beforeEach(() => {
    const MyRouteScroll = routeScroll('/theRoute', wrappedComponent);

    component = shallowWithStore(<MyRouteScroll/>, {
      device: {
        pageYOffset: 50,
      },
      features: {
        menu: {},
      },
      router: {
        location: {
          pathname: 'derp',
        },
      },
    });
    instance = component.instance();
  });

  it('renders as expected', () => {
    expect(component).toMatchSnapshot();
  });

  it('fires checks on update', () => {
    const checkForScrolledInto = jest.fn();
    const checkForScrollRequest = jest.fn();

    component.setProps({
      checkForScrollRequest,
      checkForScrolledInto,
    });

    instance.componentDidUpdate();

    expect(checkForScrolledInto).toHaveBeenCalledWith('/theRoute', instance);
    expect(checkForScrollRequest).toHaveBeenCalledWith('/theRoute', instance);
  });
});
