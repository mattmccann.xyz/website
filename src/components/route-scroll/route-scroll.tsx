/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief HOC that handles scrolling the feature into view based on a matching route
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as React from 'react'
import { connect } from 'react-redux';

import { IState } from 'state/state.type';

import * as actions from './state/route-scroll.actions';


const routeScroll = (route: string, WrappedComponent: typeof React.Component | React.SFC) => {
  interface IRouteScrollProps {
    /** Triggered to check for scroll position relative to this component */
    checkForScrolledInto: (route: string, scrollNode: React.ReactNode) => void;

    /** Triggered to check for scroll requests referencing this component */
    checkForScrollRequest: (route: string, scrollNode: React.ReactNode) => void;

    /** Watched to trigger a component update */
    location: string;
    pageYOffset: number;
    requestedRouteScroll?: string;
  }

  class RouteScroll extends React.Component<IRouteScrollProps> {
    public componentDidUpdate() {
      this.props.checkForScrolledInto(route, this);
      this.props.checkForScrollRequest(route, this);
    }

    public render() {
      return <WrappedComponent />;
    }
  }

  const mapDispatchToProps = {
    ...actions,
  };

  const mapStateToProps = (state: IState) => ({
    location: state.router.location.pathname,
    pageYOffset: state.device.pageYOffset,
    requestedRouteScroll: state.features.menu.requestedRouteScroll,
  });

  return connect(mapStateToProps, mapDispatchToProps)(RouteScroll);
};

export default routeScroll;
