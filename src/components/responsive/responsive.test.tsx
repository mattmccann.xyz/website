/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the Responsive component
 * @copyright &copy; 2018 R. Matt McCann
 */

import { ShallowWrapper } from 'enzyme';
import * as React from 'react';

import Responsive from 'components/responsive';
import shallowWithStore from 'mocks/shallow-with-store';


describe('components/responsive', () => {
  let component: ShallowWrapper;

  beforeEach(() => {
    component = shallowWithStore(<Responsive>Derp</Responsive>, {
      device: {
        innerWidth: 0,
      },
    });
  });

  describe('is xxs', () => {
    beforeEach(() => {
      component.setProps({ innerWidth: 200 });
    });

    it('renders when marked xxs', () => {
      component.setProps({ xxs: true });

      expect(component).toMatchSnapshot();
    });

    it('does not render when not marked xxs', () => {
      expect(component).toMatchSnapshot();
    });
  });

  describe('is xs', () => {
    beforeEach(() => {
      component.setProps({ innerWidth: 767 });
    });

    it('renders when marked xs', () => {
      component.setProps({ xs: true });

      expect(component).toMatchSnapshot();
    });

    it('does not render when not marked xs', () => {
      expect(component).toMatchSnapshot();
    });
  });

  describe('is sm', () => {
    beforeEach(() => {
      component.setProps({ innerWidth: 991 });
    });

    it('renders when marked sm', () => {
      component.setProps({ sm: true });

      expect(component).toMatchSnapshot();
    });

    it('does not render when not marked sm', () => {
      expect(component).toMatchSnapshot();
    });
  });

  describe('is md', () => {
    beforeEach(() => {
      component.setProps({ innerWidth: 1199 });
    });

    it('renders when marked md', () => {
      component.setProps({ md: true });

      expect(component).toMatchSnapshot();
    });

    it('does not render when not marked md', () => {
      expect(component).toMatchSnapshot();
    });
  });

  describe('is lg', () => {
    beforeEach(() => {
      component.setProps({ innerWidth: 1200 });
    });

    it('renders when marked lg', () => {
      component.setProps({ lg: true });

      expect(component).toMatchSnapshot();
    });

    it('does not render when not marked lg', () => {
      expect(component).toMatchSnapshot();
    });
  });
});
