/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Wrapper component that handles conditionally rendering based on the screen size
 * @copyright &copy; 2018 R. Matt McCann
 */

import { connect } from 'react-redux';

import { IState } from 'state/state.type';


const xxsMax = 499;
const xsMin = 500;
const xsMax = 767;
const smMin = 768;
const smMax = 991;
const mdMin = 992;
const mdMax = 1199;
const lgMin = 1200;

/** The props exposed to the public aka the caller */
interface IResponsivePublicProps {
  children: any;

  /** True if the children should be rendered for xxs devices */
  xxs?: boolean;

  /** True if the children should be rendered for xs devices */
  xs?: boolean;

  /** True if the children should be rendered for sm devices */
  sm?: boolean;

  /** True if the children should be rendered for md devices */
  md?: boolean;

  /** True if the children should be rendered for lg devices */
  lg?: boolean;
}

/** Additional props injected by connect */
interface IResponsiveProps extends IResponsivePublicProps {
  /** The inner width of the screen */
  innerWidth: number;
}

/** The responsive component wrapper */
const Responsive = (props: IResponsiveProps) => {
  const isXxs = props.innerWidth <= xxsMax;
  const isXs = xsMin <= props.innerWidth && props.innerWidth <= xsMax;
  const isSm = smMin <= props.innerWidth && props.innerWidth <= smMax;
  const isMd = mdMin <= props.innerWidth && props.innerWidth <= mdMax;
  const isLg = lgMin <= props.innerWidth;

  // If the screen is sized such that this responsive set is active
  if ((props.xxs && isXxs) || (props.xs && isXs) || (props.sm && isSm) || (props.md && isMd) || (props.lg && isLg)) {
    // Render the children
    return props.children;
  // Otherwise, don't render the children
  } else {
    return null;
  }
};

/** Used to connect the state to the component */
const mapStateToProps = (state: IState, ownProps: IResponsivePublicProps) => ({
  ...ownProps,
  innerWidth: state.device.innerWidth,
});

// Inject the required state props
export default connect(mapStateToProps)(Responsive);


