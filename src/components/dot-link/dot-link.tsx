/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief A simple icon-in-dot link component
 * @copyright &copy; 2018 R. Matt McCann
 */

import Radium from 'radium';
import * as React from 'react';

import styles from './dot-link.styles';


/** Property definitions for the component */
interface IPropTypes {
  /** Where the link leads to when clicked */
  href: string;

  /** The FontAwesome icon name to render inside the dot */
  icon: string;

  /** Additional styling for the component */
  style?: React.CSSProperties,
}

/** Render the component */
const dotLink = (props: IPropTypes) => (
  <div style={{...props.style}}>
    <a href={props.href}>
      <div style={styles.dot}>
        <i className={props.icon} style={styles.icon} />
      </div>
    </a>
  </div>
);

/** Wrap with Radium for :hover support */
export default Radium(dotLink);
