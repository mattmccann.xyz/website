/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Styles for the DotLink component
 * @copyright &copy; 2018 R. Matt McCann
 */

import { CSSProperties } from "react";

import * as v from 'styles/variables';


export default {
  dot: {
    ':hover': {
      backgroundColor: v.COLOR_LIGHT_PRIMARY,
      borderColor: v.COLOR_LIGHT_PRIMARY,
      color: v.COLOR_DARK_PRIMARY,
    },
    borderColor: v.COLOR_LIGHT_THIRD,
    borderRadius: 85000,
    borderStyle: 'solid',
    borderWidth: '2px',
    color: v.COLOR_LIGHT_PRIMARY,
    height: '3em',
    width: '3em',
  } as CSSProperties,

  icon: {
    fontSize: '1.5em',
    marginTop: '50%',
    transform: 'translateY(-50%)',
  } as CSSProperties,
}
