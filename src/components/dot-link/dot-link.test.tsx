/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the DotLink component
 * @copyright &copy; 2018 R. Matt McCann
 */

import { shallow } from 'enzyme';
import * as React from 'react';

import { DotLink } from 'components/dot-link';


describe('component/visual-anchor', () => {
  it('renders as expected', () => {
    const component = shallow(<DotLink href="/derp" icon="fab fa-gitlab" />);

    expect(component).toMatchSnapshot();
  });

  it('passes styling to the icon', () => {
    const component = shallow(<DotLink href="/derp" icon="fab fa-gitlab" style={{color: 'white'}} />);

    expect(component).toMatchSnapshot();
  });
});
