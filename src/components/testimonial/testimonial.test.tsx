/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the Testimonial component
 * @copyright &copy; 2018 R. Matt McCann
 */

import { shallow } from 'enzyme';
import * as React from 'react';

import { Testimonial } from 'components/testimonial';


describe('component/testimonial', () => {
  const props = {
    author: 'Bob Villa',
    company: 'This Olde Home',
    photo: 'https://thisoldehome.org/bob',
    quote: 'The best, stupendous!',
    title: 'Handyman',
  };

  it('renders as expected', () => {
    const component = shallow(<Testimonial {...props} />);

    expect(component).toMatchSnapshot();
  });

  it('passes styling to the component', () => {
    const component = shallow(<Testimonial {...props} style={{color: 'white'}} />);

    expect(component).toMatchSnapshot();
  });
});
