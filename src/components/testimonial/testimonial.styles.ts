/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Styling for the Testimonial component
 * @copyright &copy; 2018 R. Matt McCann
 */

import { CSSProperties } from 'react';

import * as v from 'styles/variables';


export default {
  author: {
    color: v.COLOR_DARK_SECONDARY,
    display: 'block',
    fontSize: '1.1em',
    fontWeight: 800,
    marginTop: '1em',
  } as CSSProperties,

  container: {
    marginLeft: 'auto',
    marginRight: 'auto',
    maxWidth: '40em',
  } as CSSProperties,

  photo: {
    borderRadius: '15px',
    height: '140px',
    marginBottom: '3em',
    marginTop: '4em',
    width: '140px',
  } as CSSProperties,

  quote: {
    color: v.COLOR_DARK_SECONDARY,
    fontFamily: 'Quicksand, sans-serif',
    fontSize: '1.25em',
    lineHeight: '1.5em',
    minHeight: '4.5em',
  } as CSSProperties,

  titleCompany: {
    color: v.COLOR_GRAY_DARK,
    fontFamily: 'Quicksand, sans-serif',
    fontSize: '1.0em',
  } as CSSProperties,
}
