/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief A testimonial with quote and profile picture
 * @copyright &copy; 2018 R. Matt McCann
 */

import Radium from 'radium';
import * as React from 'react';

import Image from 'components/image';

import styles from './testimonial.styles';


/** Property types of the component */
interface IPropTypes {
  /** Who gave the testimonial */
  author: string;

  /** Where the author works */
  company: string;

  /** Photo of the testifier */
  photo: string;

  /** The quote text for the testimonial */
  quote: string;

  /** Additional styling for the testimonial object */
  style?: object,

  /** The author's title at the company */
  title: string;
}

const Testimonial = (props: IPropTypes) => {
  const quote = `"${props.quote}"`;
  const titleCompany = `${props.title}, ${props.company}`;

  return (
    <div style={{...styles.container, ...props.style}}>
      <div className="row">
        <div className="col-12">
          <Image alt={props.author} style={styles.photo} type="jpg" url={props.photo} />
        </div>
      </div>

      <div className="row">
        <div className="col-12">
          <p style={styles.quote}>{quote}</p>
        </div>
      </div>

      <div className="row">
        <div className="col-12">
          <span style={styles.author}>{props.author}</span>
        </div>
      </div>

      <div className="row">
        <div className="col-12">
          <span style={styles.titleCompany}>{titleCompany}</span>
        </div>
      </div>
    </div>
  );
};

export default Radium(Testimonial);
