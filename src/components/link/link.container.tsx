/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Container for the Link component
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as React from 'react';
import { connect } from 'react-redux';

import LinkView, { ILinkProps } from './link.view';


interface ILinkContainerProps extends ILinkProps {
  /** The current router location */
  pathname: string;
}

class Link extends React.Component<ILinkContainerProps> {
  /** Renders the component */
  public render() {
    return (
      <LinkView isMatch={this.isMatch()} {...this.props} />
    )
  }

  /**
   * @returns True if the current router location matches the target for the link
   */
  private isMatch(): boolean {
    return (this.props.pathname === this.props.to);
  }
}

/** Maps the Redux store to the component props */
const mapStateToProps = (state: any) => ({ // TODO(mmccann): Type this properly
  /** The current router path */
  pathname: state.router.location.pathname,
});


// Wrap our Link component in Radium for use of :hover tag
export default connect(mapStateToProps)(Link);
