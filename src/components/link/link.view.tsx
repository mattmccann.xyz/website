/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief View for the Link component
 * @copyright &copy; 2018 R. Matt McCann
 */

import Radium from 'radium';
import * as React from 'react';
import { Link as ReactLink } from 'react-router-dom';

import styles from './link.styles';


// Wrap the Router Link with Radium so that we can apply :hover styles
const RadiumReactLink = Radium(ReactLink);

export interface ILinkProps {
  /** The text of the link */
  children: any;

  /** The DOM id of the Link */
  id?: string;

  /** Click handler triggered to produce link side-effects */
  onClick?: () => void;

  /** Additional styling for the link */
  style?: object;

  /** The route to link to */
  to: string;
}

interface ILinkViewProps extends ILinkProps {
  /** True if the link's target is the current router location */
  isMatch: boolean;
}

const Link = (props: ILinkViewProps) => {
  const style = {...styles.link, ...props.style};

  // If the link's target is active
  if (props.isMatch) {
    // Underline the link even when not hovered
    style.textDecoration = 'underline';
  }

  return (
    <RadiumReactLink id={props.id} onClick={props.onClick} to={props.to} style={style}>
      {props.children}
    </RadiumReactLink>
  );
};

export default Radium(Link);
