/**
 * @blame R. Matt McCann
 * @brief Unit tests for the Link container
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as React from 'react';

import { Link } from 'components/link';
import shallowWithStore from 'mocks/shallow-with-store';


describe('components/link/container', () => {
  const state = {
    router: {
      location: {
        pathname: '/derp',
      },
    },
  };

  it('renders with isMatch true on path match', () => {
    const component = shallowWithStore(<Link to="/derp">Derp</Link>, state);

    expect(component).toMatchSnapshot();
  });

  it('renders with isMatch false on path mismatch', () => {
    const component = shallowWithStore(<Link to="/not-derp">Derp</Link>, state);

    expect(component).toMatchSnapshot();
  });
});
