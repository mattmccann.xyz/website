/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Styling for the Link component
 * @copyright &copy; 2018 R. Matt McCann
 */

import { CSSProperties } from 'react';

import * as v from 'styles/variables';


export default {
  link: {
    ':hover': {
      textDecoration: 'underline',
    },
    color: v.COLOR_DARK_SECONDARY,
    textDecoration: 'none',
  } as CSSProperties,
};
