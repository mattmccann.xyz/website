import LinkContainer from './link.container';
import LinkView from './link.view';

export {
  LinkContainer as Link,
  LinkContainer,
  LinkView,
};
