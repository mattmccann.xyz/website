/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the Link component
 * @copyright &copy; 2018 R. Matt McCann
 */

import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';

import Link from 'components/link/link.view';


describe('component/link/view', () => {
  let component: ShallowWrapper;

  beforeEach(() => {
    component = shallow(<Link isMatch={true} to="/derp">Derp</Link>);
  });

  it('renders without underline when not matched', () => {
    component.setProps({
      isMatch: false,
    });

    expect(component).toMatchSnapshot();
  });

  it('renders with underline when matched', () => {
    component.setProps({
      isMatch: true,
    });

    expect(component).toMatchSnapshot();
  });

  it('renders the children', () => {
    component = shallow(<Link isMatch={true} to="/derp"><h1>Derp</h1></Link>);

    expect(component).toMatchSnapshot();
  });

  it('passes on the styling', () => {
    component.setProps({
      style: {
        color: 'red',
      },
    });

    expect(component).toMatchSnapshot();
  });
});
