/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Styles for the Button component
 * @copyright &copy; 2018 R. Matt McCann
 */

import { CSSProperties } from "react";

import * as v from 'styles/variables';


export default {
  button: {
    ':hover': {
      backgroundColor: v.COLOR_DARK_PRIMARY,
      color: v.COLOR_LIGHT_PRIMARY,
      cursor: 'pointer',
    },
    borderColor: v.COLOR_DARK_PRIMARY,
    borderRadius: '13212123px',
    borderStyle: 'solid',
    borderWidth: '2px',
    color: v.COLOR_DARK_PRIMARY,
    paddingBottom: '8px',
    paddingLeft: '15px',
    paddingRight: '15px',
    paddingTop: '8px',
    textAlign: 'center',
  } as CSSProperties,
};
