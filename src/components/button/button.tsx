/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief A standard stylized button
 * @copyright &copy; 2018 R. Matt McCann
 */

import Radium from 'radium';
import * as React from 'react';


import styles from './button.styles';


interface IPropTypes {
  children: any;
  id?: string;
  light?: boolean;
  onClick: () => void;
  style?: object;
}

const Button = (props: IPropTypes) => (
  <div id={props.id} style={{...styles.button, ...props.style}} onClick={props.onClick}>
    {props.children}
  </div>
);

export default Radium(Button);
