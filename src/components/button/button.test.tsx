/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the Button component
 * @copyright &copy; 2018 R. Matt McCann
 */

import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';

import Button from 'components/button/button';


describe('component/Button', () => {
  let clickHandler: () => void;
  let component: ShallowWrapper;

  beforeEach(() => {
    clickHandler = jest.fn();
    component = shallow(<Button onClick={clickHandler}>Button</Button>);
  });

  it('renders without crashing', () => {
    expect(component).toBeTruthy();
  });

  it('renders the children', () => {
    expect(component).toMatchSnapshot();
  });

  it('passes on prop styles', () => {
    component.setProps({
      style: {
        backgroundColor: 'black',
      }
    });

    expect(component).toMatchSnapshot();
  });

  it('calls the handler on click', () => {
    component.simulate('click');

    expect(clickHandler).toHaveBeenCalled();
  });
});


