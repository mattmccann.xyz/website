/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the Image component
 * @copyright &copy; 2019 R. Matt McCann
 */

import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';

import Image from 'components/image';


describe('components/image', () => {
  let component: ShallowWrapper;
  let style: React.CSSProperties;

  beforeEach(() => {
    style = {
      float: 'left'
    };

    component = shallow(<Image
      alt="Alternative Description" style={style} type="png" url="url.com/image.%TYPE"
    />);
  });

  it('renders as expected', () => {
    expect(component).toMatchSnapshot();
  });
});
