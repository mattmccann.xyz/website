/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief A simple wrapper for supporting WebP images
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as React from 'react';


interface IImageProps {
  /** Alt tag for the image */
  alt: string;

  /** Styles to apply to the image */
  style: React.CSSProperties;

  /** Non-WebP image type */
  type: string;

  /** Root url of the image */
  url: string;
}

export default (props: IImageProps) => {
  const { alt, style, type, url } = props;

  const nonWebpUrl = url.replace(/%TYPE/, type);
  const nonWebpType = `image/${type}`;
  const webpUrl = url.replace(/%TYPE/, 'webp');

  return (
    <picture>
      <source srcSet={webpUrl} style={style} type="image/webp" />
      <source srcSet={nonWebpUrl} style={style} type={nonWebpType} />
      <img alt={alt} src={nonWebpUrl} style={style} />
    </picture>
  );
};
