/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the MyWork feature state reducer
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as actions from './my-work.actions';
import { initialState, reducer } from './my-work.reducer';


describe('features/my-work/state/reducer', () => {
  it('initializes the state', () => {
    const expectedState = initialState();
    const newState = reducer(undefined, { type: 'NOT_HANDLED' });

    expect(newState).toEqual(expectedState);
  });

  it('reduces CLICKED_READ_MORE', () => {
    const expectedState = Object.assign(initialState(), { expandedProjectCard: 'myProjectId'});
    const oldState = Object.assign(initialState(), { revealedOverlay: 'someProject' });

    const newState = reducer(oldState, actions.onClickReadMore('myProjectId'));

    expect(newState).toEqual(expectedState);
  });

  it('reduces MOUSE_ENTERED_PROJECT_CARD', () => {
    const expectedState = Object.assign(initialState(), { revealedOverlay: 'myProjectId' });

    const newState = reducer(undefined, actions.onMouseEnterProjectCard('myProjectId'));

    expect(newState).toEqual(expectedState);
  });

  it('reduces MOUSE_LEFT_PROJECT_CARD', () => {
    const expectedState = initialState();
    const oldState = Object.assign(initialState(), { revealedOverlay: 'someProject' });

    const newState = reducer(oldState, actions.onMouseLeaveProjectCard('derp!'));

    expect(newState).toEqual(expectedState);
  });

  it('reduces SET_IS_RENDERED', () => {
    const expectedState = Object.assign(initialState(), { isRendered: true });

    const newState = reducer(undefined, actions.setIsRendered());

    expect(newState).toEqual(expectedState);
  });

  it('passes along state for unmatched action type', () => {
    const oldState = Object.assign(initialState(), { revealedOverlay: 'someProject' });

    const newState = reducer(oldState, { type: 'NOT_HANDLED' });

    expect(newState).toEqual(oldState);
  });
});
