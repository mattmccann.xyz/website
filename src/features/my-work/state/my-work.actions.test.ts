/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the MyWork feature redux actions
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as actions from './my-work.actions';
import * as c from './my-work.constants';


describe('features/my-work/state/actions', () => {
  it('onClickReadMore', () => {
    const action = actions.onClickReadMore('myProjectId');

    expect(action).toEqual({
      payload: 'myProjectId', type: c.CLICKED_READ_MORE,
    })
  });

  it('onMouseEnterProjectCard', () => {
    const action = actions.onMouseEnterProjectCard('myProjectId');

    expect(action).toEqual({
      payload: 'myProjectId', type: c.MOUSE_ENTERED_PROJECT_CARD,
    });
  });

  it('onMouseLeaveProjectCard', () => {
    const action = actions.onMouseLeaveProjectCard('myProjectId');

    expect(action).toEqual({
      payload: 'myProjectId', type: c.MOUSE_LEFT_PROJECT_CARD,
    });
  });

  it('setIsRendered', () => {
    const action = actions.setIsRendered();

    expect(action).toEqual({
      type: c.SET_IS_RENDERED,
    });
  });
});
