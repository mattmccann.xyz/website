/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Redux reducer for the MyWork feature state
 * @copyright &copy; 2018 R. Matt McCann
 */

import { IAction } from 'state/action.type';

import * as c from './my-work.constants';


export interface IMyWorkState {
  /** The currently expanded project card $id, or undefined if none expanded */
  expandedProjectCard?: string;

  /** True if the MyWork feature is rendered */
  isRendered: boolean;

  /** The currently overlay revealed project card $id, or undefined if none revealed */
  revealedOverlay?: string;
}

/** Initial state for the feature */
export const initialState = (): IMyWorkState => ({
  isRendered: false,
});

/**
 * Reducer for the feature state.
 *
 * @param state The state we are updating
 * @param action The action we are reducing
 * @returns The updated state
 */
export const reducer = (state = initialState(), action: IAction): IMyWorkState => {
  switch (action.type) {
    case c.CLICKED_READ_MORE: {
      const newState = { ...state, expandedProjectCard: action.payload };
      delete newState.revealedOverlay;

      return newState;
    }

    case c.MOUSE_ENTERED_PROJECT_CARD: {
      return {
        ...state,
        revealedOverlay: action.payload,
      };
    }

    case c.MOUSE_LEFT_PROJECT_CARD: {
      const newState = { ...state };
      delete newState.revealedOverlay;

      return newState;
    }

    case c.SET_IS_RENDERED: {
      return {
        ...state,
        isRendered: true,
      };
    }

    default:
      return state;
  }
};
