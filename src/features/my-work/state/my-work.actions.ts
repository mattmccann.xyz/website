/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief View for the MyWork actions
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as c from './my-work.constants';


export const onClickReadMore = (projectId: string) => ({
  payload: projectId, type: c.CLICKED_READ_MORE,
});

export const onMouseEnterProjectCard = (projectId: string) => ({
  payload: projectId, type: c.MOUSE_ENTERED_PROJECT_CARD,
});

export const onMouseLeaveProjectCard = (projectId: string) => ({
  payload: projectId, type: c.MOUSE_LEFT_PROJECT_CARD,
});

export const setIsRendered = () => ({ type: c.SET_IS_RENDERED });
