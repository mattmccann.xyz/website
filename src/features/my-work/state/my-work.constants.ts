/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Redux constants for the MyWork feature state
 * @copyright &copy; 2018 R. Matt McCann
 */

export const CLICKED_READ_MORE = 'MY_WORK_CLICKED_READ_MORE';
export const MOUSE_ENTERED_PROJECT_CARD = 'MY_WORK_MOUSE_ENTERED_PROJECT_CARD';
export const MOUSE_LEFT_PROJECT_CARD = 'MY_WORK_MOUSE_LEFT_PROJECT_CARD';
export const SET_IS_RENDERED = 'MY_WORK_SET_IS_RENDERED';
