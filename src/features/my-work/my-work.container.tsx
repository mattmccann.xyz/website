/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Container for the MyWork feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as React from 'react';
import { connect } from 'react-redux';

import routeScroll from 'components/route-scroll';
import { list as listProjects } from 'state/resources/projects/projects.actions';
import { IState } from 'state/state.type';

import * as actions from './state/my-work.actions'
import { IMyWorkViewProps, MyWorkView } from './view';


interface IMyWorkContainerProps extends IMyWorkViewProps {
  /** Action that fetches a list of projects */
  listProjects: () => void;
}

export class MyWorkContainer extends React.Component<IMyWorkContainerProps> {
  public componentDidMount() {
    // Fetch a list of the projects on mount
    this.props.listProjects();
  }

  public render() {
    return <MyWorkView {...this.props} />;
  }
}

/** Maps the actions needed to the component props */
const mapDispatchToProps = {
  ...actions,
  listProjects,
};

/** Maps the redux store to the component props */
const mapStateToProps = (state: IState) => ({
  ...state.features.myWork,
  projects: [...state.resources.projects.value],
});

// Connect the Redux store to the component
export const ConnectedMyWorkContainer = connect(mapStateToProps, mapDispatchToProps)(MyWorkContainer);

// Attach a route scroll to th component
export default routeScroll('/projects', ConnectedMyWorkContainer);
