/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the MyWork feature container
 * @copyright &copy; 2018 R. Matt McCann
 */

import { ShallowWrapper } from 'enzyme';
import * as React from 'react';

import { ConnectedMyWorkContainer, MyWorkContainer } from 'features/my-work/my-work.container';
import shallowWithStore from 'mocks/shallow-with-store';


describe('features/my-work/container', () => {
  let component: ShallowWrapper;

  beforeEach(() => {
    const state = {
      features: {
        myWork: {
          expandedProjectCard: true,
        },
      },
      resources: {
        projects: {
          value: [],
        },
      },
    };
    component = shallowWithStore(<ConnectedMyWorkContainer />, state);
  });

  it('maps the state to the props', () => {
    const instance = component.instance() as MyWorkContainer;

    expect(instance.props.expandedProjectCard).toBe(true);
    expect(instance.props.projects).toEqual([]);
  });

  it('fires the listProjects request when mounted', () => {
    const listProjects = jest.fn();
    component.setProps({ listProjects });

    const instance = component.instance() as MyWorkContainer;
    instance.componentDidMount();

    expect(listProjects).toHaveBeenCalled();
  });

  it('renders as expected', () => {
    expect(component).toMatchSnapshot();
  });
});
