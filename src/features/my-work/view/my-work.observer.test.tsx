/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the MyWork view observer
 * @copyright &copy; 2019 R. Matt McCann
 */

import { ShallowWrapper } from 'enzyme';
import * as React from 'react';

import shallowWithStore from 'mocks/shallow-with-store';
import { IProject } from 'services/api';

import MyWorkObserver from './my-work.observer';


describe('features/my-work/view/my-work.observer', () => {
  let component: ShallowWrapper;
  let onClickReadMore: (projectId: string) => void;
  let onMouseEnterProjectCard: (projectId: string) => void;
  let onMouseLeaveProjectCard: (projectId: string) => void;
  let projects: IProject[];

  beforeEach(() => {
    onClickReadMore = jest.fn();
    onMouseEnterProjectCard = jest.fn();
    onMouseLeaveProjectCard = jest.fn();

    projects = [
      {
        $id: 'projectId',
        backgroundImageType: 'png',
        backgroundImageUrl: 'https://imgs.com/backgroundImageUrl',
        briefDescription: 'A brief description',
        company: 'This Olde Home',
        companyTitle: 'Chief Handyman',
        dateRange: 'September 2050 - Present',
        description: 'Something something lorem ipsum',
        tags: ['juggling', 'striving', 'hacking'],
        title: 'Rebuilt This Olde Home',
      },
      {
        $id: 'projectId2',
        backgroundImageType: 'png',
        backgroundImageUrl: 'https://imgs.com/backgroundImageUrl2',
        briefDescription: 'A brief description2',
        company: 'This Olde Home2',
        companyTitle: 'Chief Handyman2',
        dateRange: 'September 2050 - Present2',
        description: 'Something something lorem ipsum2',
        tags: ['juggling', 'striving', 'hacking2'],
        title: 'Rebuilt This Olde Home2',
      }
    ];

    const state = {
      features: {
        myWork: {
          isRendered: true,
        },
      },
    };

    component = shallowWithStore(<MyWorkObserver
      expandedProjectCard={''} onClickReadMore={onClickReadMore}
      onMouseEnterProjectCard={onMouseEnterProjectCard} onMouseLeaveProjectCard={onMouseLeaveProjectCard}
      projects={projects} revealedOverlay={undefined}
    />, state);
  });

  it('renders as expected', () => {
    console.warn(
      'The message that precedes is due to the fact that module resolution behaves differently between' +
      'react-scripts start and react-scripts test-once. The MyWorkObserver is not being meaningfully tested as ' +
      'a result. I\'m leaving this error in here as I need to come back to this and address it.'
    );
    expect(component).toMatchSnapshot();
  });
});
