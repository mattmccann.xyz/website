/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for for the MyWork feature view
 * @copyright &copy; 2018 R. Matt McCann
 */

import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';

import { IProject } from "services/api";

import { MyWorkView } from './my-work.view';


describe('features/my-work/view', () => {
  let component: ShallowWrapper;
  let expandedProjectCard: string;
  let onClickReadMore: (projectId: string) => void;
  let onMouseEnterProjectCard: (projectId: string) => void;
  let onMouseLeaveProjectCard: (projectId: string) => void;
  let projects: IProject[];
  let revealedOverlay: string;
  beforeEach(() => {
    onClickReadMore = jest.fn();
    onMouseEnterProjectCard = jest.fn();
    onMouseLeaveProjectCard = jest.fn();

    projects = [
      {
        $id: 'projectId',
        backgroundImageType: 'png',
        backgroundImageUrl: 'https://imgs.com/backgroundImageUrl',
        briefDescription: 'A brief description',
        company: 'This Olde Home',
        companyTitle: 'Chief Handyman',
        dateRange: 'September 2050 - Present',
        description: 'Something something lorem ipsum',
        tags: ['juggling', 'striving', 'hacking'],
        title: 'Rebuilt This Olde Home',
      },
      {
        $id: 'projectId2',
        backgroundImageType: 'png',
        backgroundImageUrl: 'https://imgs.com/backgroundImageUrl2',
        briefDescription: 'A brief description2',
        company: 'This Olde Home2',
        companyTitle: 'Chief Handyman2',
        dateRange: 'September 2050 - Present2',
        description: 'Something something lorem ipsum2',
        tags: ['juggling', 'striving', 'hacking2'],
        title: 'Rebuilt This Olde Home2',
      }
    ];
  });

  const buildComponent = () => {
    component = shallow(<MyWorkView
      expandedProjectCard={expandedProjectCard} onClickReadMore={onClickReadMore}
      onMouseEnterProjectCard={onMouseEnterProjectCard} onMouseLeaveProjectCard={onMouseLeaveProjectCard}
      projects={projects} revealedOverlay={revealedOverlay}
    />);
  };

  it('renders as expected', () => {
    buildComponent();

    expect(component).toMatchSnapshot();
  });

  it('reveals the overlay for the revealed project', () => {
    expandedProjectCard = 'projectId2';
    buildComponent();

    expect(component).toMatchSnapshot();
  });

  it('expands the project card for the expanded project', () => {
    revealedOverlay = 'projectId';
    buildComponent();

    expect(component).toMatchSnapshot();
  });
});
