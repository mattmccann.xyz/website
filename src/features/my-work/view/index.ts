import MyWorkObserver from './my-work.observer';
import { IMyWorkViewProps} from './my-work.view';

export {
  MyWorkObserver as MyWorkView,
  IMyWorkViewProps,
}
