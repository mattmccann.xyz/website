/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Styles for the MyWork feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import { CSSProperties } from 'react';

import * as v from 'styles/variables';


export default {
  container: {
    marginLeft: 'auto',
    marginRight: 'auto',
    maxWidth: '1200px',
  } as CSSProperties,

  projects: {
    container: {
      padding: '24px',
    } as CSSProperties,
  },

  title: (isMobile: boolean) => ({
    color: v.COLOR_DARK_SECONDARY,
    fontFamily: '"Permanent Marker", sans-serif',
    fontSize: '2.5em',
    fontWeight: 800,
    marginBottom: isMobile ? '0.5em' : '1em',
    marginTop: isMobile ? '3.5em' : '5em',
    paddingLeft: '1em',
    paddingRight: '1em',
  } as CSSProperties),
};
