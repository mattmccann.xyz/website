/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief View for the MyWork feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as React from 'react';

import ProjectCardView from 'components/project-card';
import Responsive from 'components/responsive';
import { IProject } from 'services/api';

import styles from './my-work.styles';


export interface IMyWorkViewProps {
  expandedProjectCard?: string;
  onClickReadMore: (projectId: string) => void;
  onMouseEnterProjectCard: (projectId: string) => void;
  onMouseLeaveProjectCard: (projectId: string) => void;
  projects: IProject[];
  revealedOverlay?: string;
}

export class MyWorkView extends React.PureComponent<IMyWorkViewProps> {
  public render() {
    const projects = this.renderProjects();
    const title = (isMobile: boolean) => (
      <h2 style={styles.title(isMobile)}>Explore My Work</h2>
    );

    return (
      <div style={styles.container} className="container">
        <div className="row">
          <div className="col-12 text-center">
            <Responsive xxs xs sm>
              {title(true)}
            </Responsive>
            <Responsive md lg>
              {title(false)}
            </Responsive>
          </div>
        </div>
        <div className="row" style={styles.projects.container}>
          { projects }
        </div>
      </div>
    );
  }

  private renderProjects() {
    const {
      expandedProjectCard, onClickReadMore, onMouseEnterProjectCard, onMouseLeaveProjectCard, revealedOverlay,
    } = this.props;

    return this.props.projects.map(project => {
      return (
        <ProjectCardView
          key={project.$id}
          project={project}
          isExpanded={expandedProjectCard === project.$id}
          isOverlayRevealed={revealedOverlay === project.$id}
          onClickReadMore={onClickReadMore}
          onMouseEnter={onMouseEnterProjectCard}
          onMouseLeave={onMouseLeaveProjectCard}
        />
      )
    });
  }
}
