/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Visibility wrapper around the MyWork feature that delays rendering until
 *   the feature is actually visible. I don't want the above-the-fold content waiting
 *   on render for the project images to load.
 * @copyright &copy; 2019 R. Matt McCann
 */

import * as React from 'react';
import Observer from 'react-intersection-observer';
import { connect } from 'react-redux';
import { Element } from 'react-scroll';

import { IState } from 'state/state.type';

import * as actions from '../state/my-work.actions';
import { IMyWorkViewProps, MyWorkView } from './my-work.view';


interface IMyWorkObserverProps extends IMyWorkViewProps {
  /** True if the MyWork observed content has been rendered */
  isRendered: boolean;

  /** Sets the isRendered state */
  setIsRendered: () => void;
}

const MyWorkObserver = (props: IMyWorkObserverProps) => {
  const onChange = (inView: boolean) => {
    // If this is the first render of the observed content
    if (!props.isRendered && inView) {
      // Update the state to pin the observed content as visible
      props.setIsRendered();
    }
  };

  return (
    <Observer onChange={onChange}>
    {
      ({ref, inView}) => {
        // TODO(mmccann): I need to come back and put in a more sophisticated delay of image fetch,
        // as this basic approach breaks the SEO prerender
        return (
          <div>
            <Element name="projects-element"/>
            <div ref={ref}>
              { (props.isRendered || inView) && <MyWorkView {...props} /> }
            </div>
          </div>
        );
      }
    }
    </Observer>
  );
};

/** Maps the actions needed to the component props */
const mapDispatchToProps = {
  ...actions,
};

/** Maps the redux store to the component props */
const mapStateToProps = (state: IState, ownProps: IMyWorkViewProps) => ({
  ...ownProps,
  ...state.features.myWork,
});

// Connect the Redux store to the component
export default connect(mapStateToProps, mapDispatchToProps)(MyWorkObserver);
