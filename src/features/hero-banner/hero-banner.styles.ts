/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Styles for the HeroBanner feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import { CSSProperties } from "react";

import * as v from 'styles/variables';


export default {
  avatar: {
    height: '18em',
    marginTop: '4em',
    width: '18em',
  } as CSSProperties,

  container: {
    marginLeft: 'auto',
    marginRight: 'auto',
    maxWidth: '60em',
    paddingLeft: '1em',
    paddingRight: '1em',
    textAlign: 'center',
  } as CSSProperties,

  subTitle: {
    color: v.COLOR_DARK_BASIC,
    fontFamily: 'Quicksand, sans-serif',
    fontSize: '1.25em',
    fontWeight: 200,
    marginTop: '2em',
  } as CSSProperties,

  title: (isMobile: boolean) => ({
    color: v.COLOR_DARK_SECONDARY,
    fontFamily: '"Permanent Marker", sans-serif',
    fontSize: '2.5em',
    fontWeight: 400,
    marginTop: isMobile ? '4em' : '6em',
  } as CSSProperties),
}
