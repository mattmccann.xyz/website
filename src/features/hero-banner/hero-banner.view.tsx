/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief View for the HeroBanner feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as React from 'react';
import { Element } from 'react-scroll';

import Responsive from 'components/responsive';
import routeScroll from 'components/route-scroll';

import avatar from './avatar.svg';
import styles from './hero-banner.styles';


export const Avatar = () => (
  <div className="row">
    <div className="col-12">
      <img alt="My Avatar" src={avatar} style={styles.avatar} />
    </div>
  </div>
);
const RouteScrollAvatar = routeScroll('/about', Avatar);

export const TitleAndSubtitle = () => {
  const title = (isMobile: boolean) => (
    <h1 style={styles.title(isMobile)}>
      Software Engineer, Entrepreneur, Dev For Hire
    </h1>
  );

  return (
    <div>
      <div className="row">
        <div className="col-12">
          <Responsive xxs xs>
            { title(true) }
          </Responsive>
          <Responsive sm md lg>
            { title(false) }
          </Responsive>
        </div>
      </div>

      <div className="row">
        <div className="col-12">
          <h2 style={styles.subTitle}>
            I solve problems with software, and I love what I do.
          </h2>
        </div>
      </div>
    </div>
  );
};
const RouteScrollTitleAndSubtitle = routeScroll('/', TitleAndSubtitle);

export default () => (
  <div>
    <Element name="-element" />
    <div style={styles.container}>
      <RouteScrollTitleAndSubtitle />
      <RouteScrollAvatar />
    </div>
  </div>
);
