/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the HeroBanner view
 * @copyright &copy; 2018 R. Matt McCann
 */

import { shallow } from 'enzyme';
import * as React from 'react';

import { HeroBanner } from 'features/hero-banner';

import { Avatar, TitleAndSubtitle } from './hero-banner.view';


describe('features/hero-banner/view', () => {
  it('renders as expected', () => {
    expect(shallow(<HeroBanner />)).toMatchSnapshot();
  });

  it('avatar renders as expected', () => {
    expect(shallow(<Avatar />)).toMatchSnapshot();
  });

  it('title and subtitle renders as expected', () => {
    expect(shallow(<TitleAndSubtitle />)).toMatchSnapshot();
  });
});
