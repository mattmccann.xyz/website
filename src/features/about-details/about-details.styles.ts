/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Styles for the AboutDetails feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import { CSSProperties } from "react";

import * as v from 'styles/variables';


export default {
  container: {
    backgroundColor: v.COLOR_LIGHT_PRIMARY,
    borderRadius: '12px',
    boxShadow: '0 5px 5px 0 rgba(233,240,243,0.5),0 0 0 1px #E6ECF8',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: '-8em',
    width: '70em',
  } as CSSProperties,

  column: {
    paddingLeft: '3em',
    paddingRight: '3em',
    paddingTop: '4em',
    textAlign: 'center',
  } as CSSProperties,

  columnBorders: {
    borderBottomWidth: '0px',
    borderColor: v.COLOR_GRAY_PRIMARY,
    borderLeftWidth: '1px',
    borderRightWidth: '1px',
    borderStyle: 'solid',
    borderTopWidth: '0px',
  } as CSSProperties,

  description: {
    fontFamily: 'Quicksand, sans-serif',
    lineHeight: '1.5em',
    marginBottom: '4em',
  } as CSSProperties,

  icon: {
    color: v.COLOR_DARK_PRIMARY,
    fontSize: '3em',
  } as CSSProperties,

  subTitle: {
    color: v.COLOR_DARK_PRIMARY,
    fontFamily: 'Quicksand, sans-serif',
    fontSize: '1.125em',
  } as CSSProperties,

  title: {
    fontFamily: 'Quicksand, sans-serif',
    fontSize: '1.25em',
    fontWeight: 800,
    marginBottom: '2em',
    marginTop: '2em',
  } as CSSProperties,
}
