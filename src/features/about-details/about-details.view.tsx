/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief View for the AboutDetails feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as React from 'react';

import styles from './about-details.styles';


export default () => (
  <div style={styles.container} className="container">
    <div className="row">
      <div style={styles.column} className="col-4">
        <i className="fas fa-cogs" style={styles.icon} />

        <h3 style={styles.title}>
          Software Engineer
        </h3>

        <p style={styles.description}>
          I am obsessed with solving problems with code and
          continuously strive for perfection in my craft.
        </p>

        <h4 style={styles.subTitle}>
          Something derpity de do:
        </h4>

        <p style={styles.description}>
          I am obsessed with solving problems
        </p>

        <h4 style={styles.subTitle}>
          Something derpity de do:
        </h4>

        <p style={styles.description}>
          Derp<br />
          Derp2<br />
          Derp3<br />
          Derp4<br />
          Derp5<br />
          Derp7<br />
        </p>
      </div>

      <div style={{...styles.column, ...styles.columnBorders}} className="col-4">
        <i className="fas fa-user-tie" style={styles.icon} />

        <h3 style={styles.title}>
          Entrepreneur
        </h3>

        <p style={styles.description}>
          I am obsessed with solving problems with code and
          continuously strive for perfection in my craft.
        </p>

        <h4 style={styles.subTitle}>
          Something derpity de do:
        </h4>

        <p style={styles.description}>
          I am obsessed with solving problems
        </p>

        <h4 style={styles.subTitle}>
          Something derpity de do:
        </h4>

        <p style={styles.description}>
          Derp<br />
          Derp2<br />
          Derp3<br />
          Derp6<br />
          Derp7<br />
        </p>
      </div>

      <div style={styles.column} className="col-4">
        <i className="fas fa-hand-holding-usd" style={styles.icon} />

        <h3 style={styles.title}>
          Dev For Hire
        </h3>

        <p style={styles.description}>
          I am obsessed with solving problems with code and
          continuously strive for perfection in my craft.
        </p>

        <h4 style={styles.subTitle}>
          Something derpity de do:
        </h4>

        <p style={styles.description}>
          I am obsessed with solving problems
        </p>

        <h4 style={styles.subTitle}>
          Something derpity de do:
        </h4>

        <p style={styles.description}>
          Derp<br />
          Derp2<br />
          Derp3<br />
          Derp4<br />
          Derp5<br />
          Derp6<br />
          Derp7<br />
          Derp4<br />
        </p>
      </div>
    </div>
  </div>
);
