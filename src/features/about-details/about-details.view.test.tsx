/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the AboutDetails view
 * @copyright &copy; 2018 R. Matt McCann
 */

import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';

import { AboutDetails } from 'features/about-details';


describe('features/about-details/view', () => {
  let component: ShallowWrapper;

  beforeEach(() => {
    component = shallow(<AboutDetails />);
  });

  it('renders as expected', () => {
    expect(component).toMatchSnapshot();
  });
});
