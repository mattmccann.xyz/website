/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the Companies feature reducer
 * @copyright &copy; 2019 R. Matt McCann
 */

import * as actions from './companies.actions';
import { initialState, reducer } from './companies.reducer';


describe('features/companies/state/reducer', () => {
  it('initializes the state', () => {
    const expectedState = initialState();

    const newState = reducer(undefined, { type: 'NOT_HANDLED' });

    expect(newState).toEqual(expectedState);
  });

  it('reduces SET_IS_RENDERED', () => {
    const expectedState = Object.assign(initialState(), { isRendered: true });

    const newState = reducer(undefined, actions.setIsRendered());

    expect(newState).toEqual(expectedState);
  });

  it('passes along state for unmatched action type', () => {
    const oldState = Object.assign(initialState(), { revealedOverlay: 'someProject' });

    const newState = reducer(oldState, { type: 'NOT_HANDLED' });

    expect(newState).toEqual(oldState);
  });
});
