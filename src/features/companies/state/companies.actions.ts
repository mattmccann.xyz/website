/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Actions for the Companies feature
 * @copyright &copy; 2019 R. Matt McCann
 */

import * as c from './companies.constants';


export const setIsRendered = () => ({ type: c.SET_IS_RENDERED });
