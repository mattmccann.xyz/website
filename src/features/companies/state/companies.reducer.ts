/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Redux reducer for the Companies feature state
 * @copyright &copy; 2019 R. Matt McCann
 */

import { IAction } from 'state/action.type';

import * as c from './companies.constants';


export interface ICompaniesState {
  /** True if the Companies feature is rendered */
  isRendered: boolean;
}

/** Initial state for the feature */
export const initialState = (): ICompaniesState => ({
  isRendered: false,
});

/**
 * Reducer for the feature state.
 *
 * @param state The state we are updating
 * @param action The action we are reducing
 * @returns The updated state
 */
export const reducer = (state = initialState(), action: IAction): ICompaniesState => {
  switch (action.type) {
    case c.SET_IS_RENDERED: {
      return {
        ...state,
        isRendered: true,
      };
    }

    default:
      return state;
  }
};
