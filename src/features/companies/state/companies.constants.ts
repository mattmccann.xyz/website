/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Redux constants for the Companies feature state
 * @copyright &copy; 2019 R. Matt McCann
 */

export const SET_IS_RENDERED = 'COMPANIES_SET_IS_RENDERED';
