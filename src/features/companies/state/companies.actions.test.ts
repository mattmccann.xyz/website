/**
 * @blame R. MAtt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the Companies feature actions
 * @copyright &copy; 2019 R. Matt McCann
 */

import * as actions from './companies.actions';
import * as c from './companies.constants';


describe('features/companies/state/actions', () => {
  it('setIsRendered', () => {
    const action = actions.setIsRendered();

    expect(action).toEqual({
      type: c.SET_IS_RENDERED,
    });
  });
});
