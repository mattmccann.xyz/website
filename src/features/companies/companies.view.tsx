/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief View for the Companies feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as React from 'react';

import Image from 'components/image';

import styles from './companies.styles';


export default class CompaniesObserverView extends React.PureComponent {

  private static renderLogos() {
    const urlRoot = 'https://firebasestorage.googleapis.com/v0/b/mattmccann-xyz.appspot.com/o/companies%2F';
    const ads = 'ads.%TYPE?alt=media';
    const battelle = 'battelle.%TYPE?alt=media';
    const nasa = 'nasa.%TYPE?alt=media';
    const northropGrumman = 'northrop-grumman.%TYPE?alt=media';
    const pixelement = 'pixelement.%TYPE?alt=media';
    const taivara = 'taivara.%TYPE?alt=media';

    return (
      <div className="row">
        <div className="col-12 col-md-6 col-lg-4 text-center">
          <Image alt="NASA Jet Propulsion Laboratory" style={styles.logo} type='png' url={`${urlRoot}${nasa}`}/>
        </div>

        <div className="col-12 col-md-6 col-lg-4 text-center">
          <Image alt="Battelle Memorial Institute" style={styles.logo} type='png' url={`${urlRoot}${battelle}`}/>
        </div>

        <div className="col-12 col-md-6 col-lg-4 text-center">
          <Image alt="Northrop Grumman" style={styles.logo} type='png' url={`${urlRoot}${northropGrumman}`}/>
        </div>

        <div className="col-12 col-md-6 col-lg-4 text-center">
          <Image alt="PixElement" style={styles.logo} type='png' url={`${urlRoot}${pixelement}`}/>
        </div>

        <div className="col-12 col-md-6 col-lg-4 text-center">
          <Image alt="Taivara" style={styles.logo} type='png' url={`${urlRoot}${taivara}`}/>
        </div>

        <div className="col-12 col-md-6 col-lg-4 text-center">
          <Image alt="Alliance Data Systems" style={styles.logo} type='png' url={`${urlRoot}${ads}`}/>
        </div>
      </div>
    );
  }

  public render() {
    const logos = CompaniesObserverView.renderLogos();

    return (
      <div style={styles.containerOuter}>
        <div style={styles.containerInner}>
          <div className="row">
            <div className="col-12 text-center">
              <h2 style={styles.title}>
                I'm proud to have worked with some awesome companies:
              </h2>
            </div>
          </div>
          { logos }
        </div>
      </div>
    );
  }
}
