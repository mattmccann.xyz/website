/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit test for the Companies feature view
 * @copyright &copy; 2018 R. Matt McCann
 */

import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';

import CompaniesView from './companies.view';


describe('features/companies/view', () => {
  let component: ShallowWrapper;

  beforeEach(() => {
    component = shallow(<CompaniesView />);
  });

  it('renders as expected', () => {
    expect(component).toMatchSnapshot();
  });
});
