/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the Companies feature view observer
 * @copyright &copy; 2019 R. Matt McCann
 */

import { ShallowWrapper } from 'enzyme';
import * as React from 'react';

import shallowWithStore from 'mocks/shallow-with-store';

import { Companies } from 'features/companies';


describe('features/companies/companies-observer.view', () => {
  let component: ShallowWrapper;

  beforeEach(() => {
    const state = {
      features: {
        companies: {
          isRendered: true,
        },
      },
    };

    component = shallowWithStore(<Companies />, state);
  });

  it('renders as expected', () => {
    console.warn(
      'The message that precedes is due to the fact that module resolution behaves differently between' +
      'react-scripts start and react-scripts test-once. The CompaniesObserver is not being meaningfully tested as ' +
      'a result. I\'m leaving this error in here as I need to come back to this and address it.'
    );
    expect(component).toMatchSnapshot();
  });
});
