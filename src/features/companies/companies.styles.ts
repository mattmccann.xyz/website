/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Styling for the Companies feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import { CSSProperties } from "react";

import * as v from 'styles/variables';


export default {
  containerOuter: {
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: '-8em',
    maxWidth: '70em',
    paddingLeft: '1em',
    paddingRight: '1em',
  } as CSSProperties,

  containerInner: {
    backgroundColor: v.COLOR_LIGHT_PRIMARY,
    borderRadius: '12px',
    boxShadow: '0 5px 5px 0 rgba(233,240,243,0.5),0 0 0 1px #E6ECF8',
    paddingLeft: '1em',
    paddingRight: '1em',
  } as CSSProperties,

  logo: {
    height: '100px',
    marginBottom: '20px',
    marginTop: '20px',
  } as CSSProperties,

  title: {
    fontFamily: 'Quicksand, sans-serif',
    fontSize: '1.5em',
    fontWeight: 100,
    marginBottom: '2em',
    marginTop: '2em',
  } as CSSProperties,
}
