/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Visibility wrapper around the Companies feature that delays rendering until
 *   the feature is actually visible. I don't want the above-the-fold content waiting
 *   on render for the company logo images to load.
 * @copyright &copy; 2019 R. Matt McCann
 */

import * as React from 'react';
import Observer from 'react-intersection-observer';
import { connect } from 'react-redux';

import { IState } from 'state/state.type';

import CompaniesView from './companies.view';
import * as actions from './state/companies.actions';



interface ICompanyProps {
  /** True if the Companies observed content has been rendered */
  isRendered: boolean;

  /** Sets the isRendered state */
  setIsRendered: () => void;
}

const CompaniesObserver = (props: ICompanyProps) => {
  const onChange = (inView: boolean) => {
    // If this is the first render of the observed content
    if (!props.isRendered && inView) {
      // Update the state to pin the observed content as visible
      props.setIsRendered();
    }
  };

  return (
    <Observer onChange={onChange}>
      {
        ({ ref, inView }) => {
          // TODO(mmccann): I need to come back and put in a more sophisticated delay of image fetch,
          // as this basic approach breaks the SEO prerender
          return (
            <div ref={ref}>
              { (props.isRendered || inView) && <CompaniesView/> }
            </div>
          );
        }
      }
    </Observer>
  )
};

/** Maps the actions needed to the component props */
const mapDispatchToProps = {
  ...actions,
};

/** Maps the redux store to the component props */
const mapStateToProps = (state: IState) => ({
  ...state.features.companies,
});

// Connect the Redux store to the component
export default connect(mapStateToProps, mapDispatchToProps)(CompaniesObserver);
