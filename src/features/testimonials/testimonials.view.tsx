/**
 * @blame R. Matt McCann
 * @brief View for the Testimonials feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import Radium from 'radium';
import * as React from 'react';
import { Element } from 'react-scroll';

import { Testimonial } from 'components/testimonial';
import { ITestimonial } from 'services/api';

import styles from './testimonials.styles';


export interface IViewProps {
  /** Index of the currently selected testimonial */
  activeTestimonialIdx: number;

  /** Callback handler called when the user clicks on of the dot controls */
  onClickDot: (testimonialIdx: number) => void;

  /** The testimonials to be displayed */
  testimonials: ITestimonial[];
}

class TestimonialsView extends React.PureComponent<IViewProps> {
  /** Renders the view */
  public render() {
    return (
      <div>
        <Element name="testimonials-element" />
        <div style={styles.container}>
          <h2 style={styles.title}>
            Testimonials
          </h2>

          <h3 style={styles.tagLine}>
            Nice things said by those I've worked with...
          </h3>

          {this.renderTestimonials()}

          <div>
            { this.renderDots() }
          </div>
        </div>
      </div>
    );
  }

  /**
   * Renders the dot controls for the Testimonials
   *
   * @returns A List of dot components
   */
  private renderDots(): JSX.Element[] {
    // Map the testimonials to their dot controls
    return this.props.testimonials.map((testimonial, idx) => {
      let className = 'far fa-circle';

      // If this testimonial is currently selected
      if (idx === this.props.activeTestimonialIdx) {
        className = 'fas fa-circle';
      }

      // Pack the dot component
      const clickHandler = () => this.props.onClickDot(idx);
      return <i className={className} onClick={clickHandler} style={styles.dot} key={idx} />;
    });
  }

  /**
   * Packs the testimonials into Testimonial components for rendering.
   *
   * @returns List of Testimonial components or null if Testimonial is not active
   */
  private renderTestimonials(): React.ReactNodeArray {
    return this.props.testimonials.map((testimonial, idx) => {
      if (idx === this.props.activeTestimonialIdx) {
        return <Testimonial {...testimonial} style={styles.testimonial} key={idx} />;
      }

      return null;
    });
  }
}

// Wrap with Radium so we can use the :hover selector
export default Radium(TestimonialsView);
