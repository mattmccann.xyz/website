import Testimonials, { TestimonialsContainer } from './testimonials.container';

export {
  Testimonials,
  TestimonialsContainer,
};
