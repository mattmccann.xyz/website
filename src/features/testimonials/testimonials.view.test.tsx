/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the Testimonials view
 * @copyright &copy; 2018 R. Matt McCann
 */

import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';

import TestimonialsView from 'features/testimonials/testimonials.view';


describe('features/testimonials/view', () => {
  let component: ShallowWrapper;
  let onClickDot: () => void;
  const testimonials = [
    {
      '$id': '0',
      author: 'Bob Villa',
      company: 'This Olde Home',
      photo: 'https://thisoldehome.org/bob',
      quote: 'The best, stupendous!',
      title: 'Handyman',
    }, {
      '$id': '1',
      author: 'Bob Villa2',
      company: 'This Olde Home2',
      photo: 'https://thisoldehome.org/bob2',
      quote: 'The best, stupendous!2',
      title: 'Handyman2',
    }
  ];

  beforeEach(() => {
    onClickDot = jest.fn();
    component = shallow(<TestimonialsView
      activeTestimonialIdx={0}
      onClickDot={onClickDot}
      testimonials={testimonials}
    />);
  });

  it('renders as expected', () => {
    expect(component).toMatchSnapshot();
  });

  it('renders the right testimonial', () => {
    component.setProps({ activeTestimonialIdx: 1 });

    expect(component).toMatchSnapshot();
  });

  it('binds the onClickDot handler', () => {
    const dots = component.find('i');

    dots.forEach((dot, idx) => {
      dot.simulate('click');

      expect(onClickDot).toHaveBeenLastCalledWith(idx);
    });
  });
});
