/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Container for the Testimonials feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as React from 'react';
import { connect } from 'react-redux';

import routeScroll from 'components/route-scroll';
import { list as listTestimonials } from 'state/resources/testimonials/testimonials.actions';
import { IState } from 'state/state.type';

import * as actions from './state/testimonials.actions';
import TestimonialsView, { IViewProps } from './testimonials.view';


/** Type definition for the component's props */
interface IContainerProps extends IViewProps {
  /** Action that activates a transition interval timer */
  activateTransitionInterval: () => void;

  /** Action that clears the transition interval timer */
  clearTransitionInterval: () => void;

  /** Action that fetches a list of testimonials */
  listTestimonials: () => void;

  /** Action that resets the feature state */
  resetFeatureState: () => void;
}

export class TestimonialsContainer extends React.Component<IContainerProps> {
  /** Handles the component being mounted */
  public componentDidMount() {
    // Reset the feature state to avoid dangling state
    this.props.resetFeatureState();

    // Fetch a list of the testimonials on mount
    this.props.listTestimonials();

    // Activate the transition interval
    this.props.activateTransitionInterval();
  }

  /** Handles the component being unmounted */
  public componentWillUnmount() {
    // Disable our transition interval
    this.props.clearTransitionInterval();
  }

  public render() {
    return (
      <TestimonialsView {...this.props} />
    );
  }
}

/** Maps the actions needed to the component props */
const mapDispatchToProps = {
  ...actions,
  listTestimonials,
};

/** Maps the reduc store to the component props */
const mapStateToProps = (state: IState) => ({
  ...state.features.testimonials,
  testimonials: [...state.resources.testimonials.value],
});

// Connect the Redux store to the component
export const ConnectedTestimonialsContainer = connect(mapStateToProps, mapDispatchToProps)(TestimonialsContainer);

export default routeScroll('/testimonials', ConnectedTestimonialsContainer);
