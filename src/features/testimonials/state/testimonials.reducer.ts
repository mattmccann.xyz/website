/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief State reducer for the Testimonials feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import { IAction } from 'state/action.type';

import * as c from './testimonials.constants';


/** Type definition for the Testimonials feature state */
export interface ITestimonialsState {
  /** Index of the currently selected testimonial */
  activeTestimonialIdx: number;

  /** If true, the user has manually selected a testimonial to view */
  isManuallySelected: boolean;

  /** The tracking ID of the interval callback used to transition between testimonials */
  transitionIntervalId?: number;
}

/** Initial state of for the feature state */
export const initialState = () => ({
  activeTestimonialIdx: 0,
  isManuallySelected: false,
});

/**
 * Reducer for the feature state.
 *
 * @param state The state we are updating
 * @param action The action we are reducing
 * @returns The updated state
 */
export const reducer = (state = initialState(), action: IAction): ITestimonialsState => {
  switch (action.type) {
    case c.CLICKED_DOT:
      return {
        ...state,
        activeTestimonialIdx: action.payload,
        isManuallySelected: true,
      };

    case c.RESET_FEATURE_STATE:
      return initialState();

    case c.SET_TRANSITION_INTERVAL_ID:
      return {
        ...state,
        transitionIntervalId: action.payload,
      };

    case c.TO_NEXT_TESTIMONIAL: {
      const testimonialsLength = action.payload;

      // If we don't have any testimonials or the user has manually selected a testimonial, do nothing
      if (!testimonialsLength || state.isManuallySelected) {
        return state;
      }

      const activeTestimonialIdx = (state.activeTestimonialIdx + 1) % testimonialsLength;

      return {
        ...state,
        activeTestimonialIdx,
      };
    }

    default:
      return state;
  }
};
