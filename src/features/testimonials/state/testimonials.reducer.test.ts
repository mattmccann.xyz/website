/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the Testimonials feature state reducer
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as actions from './testimonials.actions';
import * as c from './testimonials.constants';
import { initialState, reducer } from './testimonials.reducer';


describe('features/testimonials/state/reducer', () => {
  it('initializes the state', () => {
    const expectedState = initialState();
    const newState = reducer(undefined, { type: 'NOT_HANDLED' });

    expect(newState).toEqual(expectedState);
  });

  it('reduces CLICKED_DOT', () => {
    const expectedState = Object.assign(initialState(), { activeTestimonialIdx: 1, isManuallySelected: true });

    const newState = reducer(undefined, actions.onClickDot(1));

    expect(newState).toEqual(expectedState);
  });

  it('reduces RESET_FEATURE_STATE', () => {
    const expectedState = initialState();
    const oldState = Object.assign(initialState(), { isManuallySelected: true });

    const newState = reducer(oldState, actions.resetFeatureState());

    expect(newState).toEqual(expectedState);
  });

  it('reduces SET_TRANSITION_INTERVAL_ID', () => {
    const expectedState = Object.assign(initialState(), { transitionIntervalId: 3 });

    const newState = reducer(undefined, { payload: 3, type: c.SET_TRANSITION_INTERVAL_ID });

    expect(newState).toEqual(expectedState);
  });

  describe('reduces TO_NEXT_TESTIMONIAL', () => {
    describe('testimonials length defined', () => {
      describe('is manually selected', () => {
        it('does not change the state', () => {
          const expectedState = Object.assign(initialState(), { isManuallySelected: true });
          const oldState = Object.assign(initialState(), { isManuallySelected: true });

          const newState = reducer(oldState, { payload: 4, type: c.TO_NEXT_TESTIMONIAL });

          expect(newState).toEqual(expectedState);
        });
      });

      describe('is not manually selected', () => {
        it('rolls over idx', () => {
          const expectedState = Object.assign(initialState(), { activeTestimonialIdx: 0 });
          const oldState = Object.assign(initialState(), { activeTestimonialIdx: 3 });

          const newState = reducer(oldState, { payload: 4, type: c.TO_NEXT_TESTIMONIAL });

          expect(newState).toEqual(expectedState);
        });

        it('advances the idx', () => {
          const expectedState = Object.assign(initialState(), { activeTestimonialIdx: 1 });

          const newState = reducer(undefined, { payload: 4, type: c.TO_NEXT_TESTIMONIAL });

          expect(newState).toEqual(expectedState);
        });
      });
    });

    describe('testimonials length not defined', () => {
      describe('is manually selected', () => {
        it('does not change the state', () => {
          const expectedState = Object.assign(initialState(), { isManuallySelected: true });
          const oldState = Object.assign(initialState(), { isManuallySelected: true });

          const newState = reducer(oldState, {  type: c.TO_NEXT_TESTIMONIAL });

          expect(newState).toEqual(expectedState);
        });
      });

      describe('is not manually selected', () => {
        it('does not change the state', () => {
          const expectedState = initialState();

          const newState = reducer(undefined, {  type: c.TO_NEXT_TESTIMONIAL });

          expect(newState).toEqual(expectedState);
        });
      });
    });
  });

  it('passes along state for unmatched action type', () => {
    const oldState = Object.assign(initialState(), { isManuallySelected: true });

    const newState = reducer(oldState, { type: 'NOT_HANDLED' });

    expect(newState).toEqual(oldState);
  });
});
