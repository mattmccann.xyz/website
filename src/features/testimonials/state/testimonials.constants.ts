/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief State constants for the Testimonials feature
 * @copyright &copy; 2018 R. Matt McCann
 */

export const CLICKED_DOT = 'TESTIMONIALS_CLICKED_DOT';
export const RESET_FEATURE_STATE = 'TESTIMONIALS_RESET_FEATURE_STATE';
export const SET_TRANSITION_INTERVAL_ID = 'TESTIMONIALS_SET_TRANSITION_INTERVAL_ID';
export const TO_NEXT_TESTIMONIAL = 'TESTIMONIALS_TO_NEXT_TESTIMONIAL';
