/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for Testimonials feature state actions
 * @copyright &copy; 2018 R. Matt McCann
 */

import mockDispatch from 'mocks/mock-dispatch';
import mockServices from 'mocks/mock-services';
import { Dispatch, GetState, IServices } from 'state/thunk.type';

import * as actions from './testimonials.actions';
import * as c from './testimonials.constants';



describe('features/testimonials/state/actions', () => {
  let dispatch: Dispatch;
  let getState: GetState;
  let services: IServices;

  beforeEach(() => {
    getState = jest.fn();
    services = mockServices();
    dispatch = mockDispatch(getState, services);
  });

  describe('activateTransitionInterval', () => {
    beforeEach(() => {
      services.window.setInterval = jest.fn().mockReturnValue(5);
    });

    it('activates the transition interval', () => {
      const transitionPeriod = 5000;

      dispatch(actions.activateTransitionInterval());

      expect((services.window.setInterval as any).mock.calls[0][1]).toEqual(transitionPeriod);
    });

    it('dispatches the transition interval id action', () => {
      dispatch(actions.activateTransitionInterval());

      expect(dispatch).toHaveBeenCalledWith({ payload: 5, type: c.SET_TRANSITION_INTERVAL_ID });
    });
  });

  describe('clearTransitionInterval', () => {
    beforeEach(() => {
      (getState as any).mockReturnValue({
        features: { testimonials: { transitionIntervalId: 5 }, },
      });
      services.window.clearInterval = jest.fn();
    });

    it('clears the interval timer', () => {
      dispatch(actions.clearTransitionInterval());

      expect(services.window.clearInterval).toHaveBeenCalledWith(5);
    });
  });

  it('onClickDot', () => {
    const action = actions.onClickDot(3);

    expect(action).toEqual({ payload: 3, type: c.CLICKED_DOT });
  });

  it('resetFeatureState', () => {
    const action = actions.resetFeatureState();

    expect(action).toEqual({ type: c.RESET_FEATURE_STATE });
  });

  describe('toNextTestimonial', () => {
    describe('testimonials is defined', () => {
      beforeEach(() => {
        (getState as any).mockReturnValue({
          resources: { testimonials: { value: [], }, },
        });
      });

      it('creates the action', () => {
        dispatch(actions.toNextTestimonial());

        expect(dispatch).toHaveBeenCalledWith({ payload: 0, type: c.TO_NEXT_TESTIMONIAL });
      });
    });

    describe('testimonials is not defined', () => {
      beforeEach(() => {
        (getState as any).mockReturnValue({
          resources: { testimonials: {}, },
        });
      });

      it('creates the action', () => {
        dispatch(actions.toNextTestimonial());

        expect(dispatch).toHaveBeenCalledWith({ type: c.TO_NEXT_TESTIMONIAL });
      });
    });
  });
});
