/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief State actions for the Testimonials feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import { IAction } from 'state/action.type';
import { Dispatch, GetState, IServices } from 'state/thunk.type';

import * as c from './testimonials.constants';


/**
 * Thunk that activates a transition interval timer.
 */
export const activateTransitionInterval = () => (dispatch: Dispatch, getState: GetState, services: IServices) => {
  const transitionPeriod = 5000;

  // Activate the transition interval
  const transitionIntervalId = services.window.setInterval(() => dispatch(toNextTestimonial()), transitionPeriod);

  // Store interval id in the state for later use
  dispatch(setTransitionIntervalId(transitionIntervalId));
};

/**
 * Thunk that clears the transition interval timer.
 */
export const clearTransitionInterval = () => (dispatch: Dispatch, getState: GetState, services: IServices) => {
  // Clear the interval timer
  services.window.clearInterval(getState().features.testimonials.transitionIntervalId);
};

/**
 * Action creator that creates the Clicked Dot action
 *
 * @param testimonialIdx Index of the clicked testimonial
 * @returns The created action
 */
export const onClickDot = (testimonialIdx: number): IAction => ({ payload: testimonialIdx, type: c.CLICKED_DOT });

/**
 * Action creator that resets the feature state.
 *
 * @returns The created action
 */
export const resetFeatureState = (): IAction => ({ type: c.RESET_FEATURE_STATE });

/**
 * Action creator that creates the Set Transition Interval ID action
 *
 * @param id Tracking ID of the interval timer
 * @returns The created action
 */
const setTransitionIntervalId = (id: number): IAction => ({ payload: id, type: c.SET_TRANSITION_INTERVAL_ID });

/**
 * Action creator that produces the To Next Testimonial action
 *
 * @returns The created action
 */
export const toNextTestimonial = () => (dispatch: Dispatch, getState: GetState) => {
  const testimonials = getState().resources.testimonials.value;
  const payload = testimonials !== undefined ? testimonials.length : undefined;

  dispatch({ payload, type: c.TO_NEXT_TESTIMONIAL });
};
