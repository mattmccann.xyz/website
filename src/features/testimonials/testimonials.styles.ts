/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Styling for the Testimonials feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import Radium from 'radium';
import { CSSProperties} from 'react';
import { fadeInRight } from 'react-animations';

import * as v from 'styles/variables';

export default {
  container: {
    borderTopColor: v.COLOR_GRAY_PRIMARY,
    borderTopStyle: 'solid',
    borderTopWidth: '1px',
    paddingLeft: '1em',
    paddingRight: '1em',
    paddingTop: '5em',
    textAlign: 'center',
  } as CSSProperties,

  dot: {
    ':hover': {
      cursor: 'pointer',
    },
    color: v.COLOR_DARK_PRIMARY,
    paddingLeft: '0.5em',
    paddingRight: '0.5em',
    paddingTop: '2em',
  } as CSSProperties,

  tagLine: {
    color: v.COLOR_GRAY_DARK,
    fontFamily: 'Quicksand, sans-serif',
    fontSize: '1.0em',
    marginTop: '1.75em',
  } as CSSProperties,

  testimonial: {
    animationDuration: '0.2s',
    animationName: Radium.keyframes(fadeInRight, 'fadeIn'),
    animationTimingFunction: 'linear',
  } as CSSProperties,

  title: {
    color: v.COLOR_DARK_SECONDARY,
    fontFamily: '"Permanent Marker", sans-serif',
    fontSize: '2em',
    fontWeight: 800,
  } as CSSProperties,
};
