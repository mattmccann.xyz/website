/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the Testimonials container
 * @copyright &copy; 2018 R. Matt McCann
 */

import { ShallowWrapper } from 'enzyme';
import * as React from 'react';

import { TestimonialsContainer } from 'features/testimonials';
import shallowWithStore from 'mocks/shallow-with-store';

import { ConnectedTestimonialsContainer } from './testimonials.container';


describe('features/testimonials/container', () => {
  let component: ShallowWrapper;
  let instance: TestimonialsContainer;

  beforeEach(() => {
    component = shallowWithStore(<ConnectedTestimonialsContainer />, {
      features: {
        testimonials: {
          activeTestimonialIdx: 1,
        },
      },
      resources: {
        testimonials: {
          value: [
            {
              author: 'Bob Villa',
              company: 'This Olde Home',
              photo: 'https://thisoldehome.org/bob',
              quote: 'The best, stupendous!',
              title: 'Handyman',
            }, {
              author: 'Bob Villa2',
              company: 'This Olde Home2',
              photo: 'https://thisoldehome.org/bob2',
              quote: 'The best, stupendous!2',
              title: 'Handyman2',
            }
          ]
        }
      },
    });

    instance = component.instance() as TestimonialsContainer;
  });

  describe('componentDidMount', () => {
    it('resets the feature state', () => {
      const resetFeatureState = jest.fn();
      component.setProps({ resetFeatureState });

      instance.componentDidMount();

      expect(resetFeatureState).toHaveBeenCalled();
    });

    it('fetches the testimonials', () => {
      const listTestimonials = jest.fn();
      component.setProps({ listTestimonials });

      instance.componentDidMount();

      expect(listTestimonials).toHaveBeenCalled();
    });

    it('activates the transition interval timer', () => {
      const activateTransitionInterval = jest.fn();
      component.setProps({ activateTransitionInterval });

      instance.componentDidMount();

      expect(activateTransitionInterval).toHaveBeenCalled();
    });
  });

  describe('componentWillUnmount', () => {
    it('clears the transition interval', () => {
      const clearTransitionInterval = jest.fn();
      component.setProps({ clearTransitionInterval });

      instance.componentWillUnmount();

      expect(clearTransitionInterval).toHaveBeenCalled();
    });
  });

  describe('render', () => {
    it('renders as expected', () => {
      expect(component).toMatchSnapshot();
    });
  });
});
