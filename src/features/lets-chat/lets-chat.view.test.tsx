/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the LetsChat feature view
 * @copyright &copy; 2018 R. Matt McCann
 */

import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';

import LetsChatView from 'features/lets-chat/lets-chat.view';


describe('features/lets-chat/view', () => {
  let component: ShallowWrapper;
  let onChangeEmail: (email: string) => void;
  let onChangeMessage: (message: string) => void;
  let onChangeName: (name: string) => void;
  let onClickSubmit: () => void;

  beforeEach(() => {
    onChangeEmail = jest.fn();
    onChangeMessage = jest.fn();
    onChangeName = jest.fn();
    onClickSubmit = jest.fn();

    component = shallow(<LetsChatView
      email={{value: ''}}
      message={{value: ''}}
      name={{value: ''}}
      onChangeEmail={onChangeEmail}
      onChangeMessage={onChangeMessage}
      onChangeName={onChangeName}
      onClickSubmit={onClickSubmit}
      submit={{isComplete: false, isError: false}}
    />);
  });

  describe('render', () => {
    describe('is complete and is not error', () => {
      beforeEach(() => {
        component.setProps({
          submit: { isComplete: true, isError: false },
        });
      });

      it('renders the complete message', () => {
        expect(component).toMatchSnapshot();
      });
    });

    describe('is not complete or is error', () => {
      describe('name', () => {
        it('renders value', () => {
          component.setProps({
            name: { value: 'Bob Villa' },
          });

          expect(component).toMatchSnapshot();
        });

        it('renders error', () => {
          component.setProps({
            name: { error: 'Please provide your name'},
          });

          expect(component).toMatchSnapshot();
        });
      });

      describe('email', () => {
        it('renders value', () => {
          component.setProps({
            email: { value: 'bob@thisoldehome.org' },
          });

          expect(component).toMatchSnapshot();
        });

        it('renders error', () => {
          component.setProps({
            email: { error: 'Please provide your email' },
          });

          expect(component).toMatchSnapshot();
        });
      });

      describe('message', () => {
        it('renders value', () => {
          component.setProps({
            message: { value: 'Hello!' },
          });

          expect(component).toMatchSnapshot();
        });

        it('renders error', () => {
          component.setProps({
            message: { error: 'Please provide a message' },
          });

          expect(component).toMatchSnapshot();
        });
      });

      describe('is unexpected error', () => {
        it('renders error message', () => {
          component.setProps({
            submit: { isComplete: true, isError: true },
          });

          expect(component).toMatchSnapshot();
        });
      });
    });
  });

  it('binds the onChangeName handler', () => {
    const input = component.find('[id="name"]');

    input.simulate('change', {currentTarget: {value: 'bob'}});

    expect(onChangeName).toHaveBeenCalledWith('bob');
  });

  it('binds the onChangeEmail handler', () => {
    const input = component.find('[id="email"]');

    input.simulate('change', {currentTarget: {value: 'bob@'}});

    expect(onChangeEmail).toHaveBeenCalledWith('bob@');
  });

  it('binds the onChangeMessage handler', () => {
    const input = component.find('[id="message"]');

    input.simulate('change', {currentTarget: {value: 'hi!'}});

    expect(onChangeMessage).toHaveBeenCalledWith('hi!');
  });

  it('binds the onClickSubmit handler', () => {
    const button = component.find('[id="submit"]');

    button.simulate('click');

    expect(onClickSubmit).toHaveBeenCalled();
  });
});
