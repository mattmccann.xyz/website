/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Container for the LetsChat feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as React from 'react';
import { connect } from 'react-redux';

import { IState } from 'state/state.type';

import LetsChatView, { ILetsChatViewProps } from './lets-chat.view';
import * as actions from './state/lets-chat.actions';


interface ILetsChatProps extends ILetsChatViewProps {
  /** Used to trigger a reset of the feature state */
  resetFeatureState: () => void;
}

export class LetsChatContainer extends React.Component<ILetsChatProps> {
  public componentDidMount() {
    // Reset the feature state
    this.props.resetFeatureState();
  }

  public render() {
    return <LetsChatView {...this.props} />;
  }
}

/** Maps the actions needed to the component props */
const mapDispatchToProps = {
  ...actions,
};

/** Maps the redux store to the component props */
const mapStateToProps = (state: IState) => ({
  ...state.features.letsChat,
});

// Connect the Redux store to the component
export default connect(mapStateToProps, mapDispatchToProps)(LetsChatContainer);
