/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief View for the LetsChat feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as React from 'react';

import { Button } from 'components/button';

import styles from './lets-chat.styles';


export interface ILetsChatViewProps {
  email: {
    /** Description of the error with the email value */
    error?: string;

    /** The current email input value */
    value: string;
  };

  message: {
    /** Description of the error with the message value */
    error?: string;

    /** The current message input value */
    value: string;
  };

  name: {
    /** Description of the error with the name value */
    error?: string;

    /** The current name input value */
    value: string;
  }

  /** Change handler triggered when the user modifies the email input */
  onChangeEmail: (email: string) => void;

  /** Change handler triggered when the user modifies the message input */
  onChangeMessage: (message: string) => void;

  /** Change handler triggered when the user modifies the name input */
  onChangeName: (name: string) => void;

  /** Change handler triggered when the user clicks the Submit button */
  onClickSubmit: () => void;

  submit: {
    /** True if the submit is complete */
    isComplete: boolean;

    /** True when an unexpected submit error occurred */
    isError: boolean;
  };
}

export default class extends React.Component<ILetsChatViewProps> {
  public render() {
    const { submit } = this.props;

    return (
      <div style={styles.container}>
        <h1>
          I'm glad you want to reach out! <br/> What can I do for you today?
        </h1>

        {
          (!submit.isComplete || submit.isError) &&
          this.renderForm()
        }

        {
          submit.isComplete && !submit.isError &&
          this.renderComplete()
        }
      </div>
    );
  }

  private onChangeEmail = (event: React.FormEvent<HTMLInputElement>) => {
    const email = event.currentTarget.value;

    this.props.onChangeEmail(email);
  };

  private onChangeMessage = (event: React.FormEvent<HTMLTextAreaElement>) => {
    const message = event.currentTarget.value;

    this.props.onChangeMessage(message);
  };

  private onChangeName = (event: React.FormEvent<HTMLInputElement>) => {
    const name = event.currentTarget.value;

    this.props.onChangeName(name);
  };

  private renderComplete = () => (
    <div style={styles.form.container}>
      <div className="row">
        <div className="col-12" style={styles.complete}>
          <h2>
            Thanks for reaching out - I'll get back to you shortly!
          </h2>
        </div>
      </div>
    </div>
  );

  private renderForm = () => (
    <div style={styles.form.container}>
      <div className="row">
        <div className="col-12 col-sm-6" style={styles.form.field}>
          <label style={styles.form.label}>Name</label>
          <input id="name" onChange={this.onChangeName} style={styles.form.input}
                 type="text" value={this.props.name.value} />
          {
            this.props.name.error &&
            <label style={styles.form.error}>{ this.props.name.error }</label>
          }
        </div>

        <div className="col-12 col-sm-6" style={styles.form.field}>
          <label style={styles.form.label}>Email</label>
          <input id="email" onChange={this.onChangeEmail} style={styles.form.input}
          type="text" value={this.props.email.value} />
          {
            this.props.email.error &&
            <label style={styles.form.error}>{ this.props.email.error }</label>
          }
        </div>

        <div className="col-12" style={styles.form.field}>
          <label style={styles.form.label}>Message</label>
          <textarea id="message" onChange={this.onChangeMessage} style={styles.form.input}
          rows={5} value={this.props.message.value} />
          {
            this.props.message.error &&
            <label style={styles.form.error}>{ this.props.message.error }</label>
          }
          {
            this.props.submit.isError &&
            <label style={styles.form.error}>An unexpected error occurred - oh no!</label>
          }
        </div>

        <div className="col-12">
          <Button id="submit" onClick={this.props.onClickSubmit} style={styles.form.button}>
            Submit
          </Button>
        </div>
      </div>
    </div>
  );
}
