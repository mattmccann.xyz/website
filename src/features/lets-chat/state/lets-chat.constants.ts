/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Redux constants for the LetsChat feature
 * @copyright &copy; 2018 R. Matt McCann
 */

export const CHANGED_EMAIL = 'LETS_CHAT_CHANGED_EMAIL';
export const CHANGED_MESSAGE = 'LETS_CHAT_CHANGED_MESSAGE';
export const CHANGED_NAME = 'LETS_CHAT_CHANGED_NAME';
export const MALFORMED_EMAIL = 'LETS_CHAT_MALFORMED_EMAIL';
export const MISSING_EMAIL = 'LETS_CHAT_MISSING_EMAIL';
export const MISSING_MESSAGE = 'LETS_CHAT_MISSING_MESSAGE';
export const MISSING_NAME = 'LETS_CHAT_MISSING_NAME';
export const RESET_FEATURE_STATE = 'LETS_CHAT_RESET_FEATURE_STATE';
export const SUBMIT_COMPLETED = 'LETS_CHAT_SUBMIT_COMPLETED';
export const SUBMIT_FAILED = 'LETS_CHAT_SUBMIT_FAILED';
export const SUBMIT_STARTED = 'LETS_CHAT_SUBMIT_STARTED';
