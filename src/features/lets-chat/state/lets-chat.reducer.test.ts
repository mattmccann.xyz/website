/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the LetsChat feature state reducer
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as actions from './lets-chat.actions';
import * as c from './lets-chat.constants';
import { initialState, reducer } from './lets-chat.reducer';


describe('features/lets-chat/state/reducer', () => {
  it('initializes the state', () => {
    const expectedState = initialState();
    const newState = reducer(undefined, { type: 'NOT_HANDLED' });

    expect(newState).toEqual(expectedState);
  });

  it('reduces CHANGED_EMAIL', () => {
    const expectedState = Object.assign(initialState(), { email: { value: 'bob@thisoldehome.org' }});

    const newState = reducer(undefined, actions.onChangeEmail('bob@thisoldehome.org'));

    expect(newState).toEqual(expectedState);
  });

  it('reduces CHANGED_MESSAGE', () => {
    const expectedState = Object.assign(initialState(), { message: { value: 'Hello!' }});

    const newState = reducer(undefined, actions.onChangeMessage('Hello!'));

    expect(newState).toEqual(expectedState);
  });

  it('reduces CHANGED_NAME', () => {
    const expectedState = Object.assign(initialState(), { name: { value: 'Bob Villa' }});

    const newState = reducer(undefined, actions.onChangeName('Bob Villa'));

    expect(newState).toEqual(expectedState);
  });

  it('reduces MALFORMED_EMAIL', () => {
    const expectedState = Object.assign(initialState(), {
      email: { error: 'Please provide a valid email', value: 'bob@' },
    });

    let newState = reducer(undefined, actions.onChangeEmail('bob@'));
    newState = reducer(newState, { type: c.MALFORMED_EMAIL });

    expect(newState).toEqual(expectedState);
  });

  it('reduces MISSING_EMAIL', () => {
    const expectedState = Object.assign(initialState(), {
      email: { error: 'Please provide your email', value: '' },
    });

    const newState = reducer(undefined, { type: c.MISSING_EMAIL });

    expect(newState).toEqual(expectedState);
  });

  it('reduces MISSING_MESSAGE', () => {
    const expectedState = Object.assign(initialState(), {
      message: { error: 'Please provide a message', value: '' },
    });

    const newState = reducer(undefined, { type: c.MISSING_MESSAGE });

    expect(newState).toEqual(expectedState);
  });

  it('reduces MISSING_NAME', () => {
    const expectedState = Object.assign(initialState(), {
      name: { error: 'Please provide your name', value: '' },
    });

    const newState = reducer(undefined, { type: c.MISSING_NAME });

    expect(newState).toEqual(expectedState);
  });

  it('reduces RESET_FEATURE_STATE', () => {
    const oldState = Object.assign(initialState(), {
      name: { error: 'Please provide your name', value: '' },
    });

    const newState = reducer(oldState, { type: c.RESET_FEATURE_STATE });

    expect(newState).toEqual(initialState());
  });

  it('reduces SUBMIT_COMPLETED', () => {
    const expectedState = Object.assign(initialState(), {
      submit: {
        isComplete: true,
        isError: false,
        isInProgress: false,
      },
    });

    const newState = reducer(undefined, { type: c.SUBMIT_COMPLETED });

    expect(newState).toEqual(expectedState);
  });

  it('reduces SUBMIT_FAILED', () => {
    const error = new Error('Oh no!');
    const expectedState = Object.assign(initialState(), {
      submit: {
        error,
        isComplete: true,
        isError: true,
        isInProgress: false,
      },
    });

    const newState = reducer(undefined, { payload: error, type: c.SUBMIT_FAILED });

    expect(newState).toEqual(expectedState);
  });

  it('reduces SUBMIT_STARTED', () => {
    const expectedState = Object.assign(initialState(), {
      submit: {
        isComplete: false,
        isError: false,
        isInProgress: true,
      },
    });

    const newState = reducer(undefined, { type: c.SUBMIT_STARTED });

    expect(newState).toEqual(expectedState);
  });

  it('passes along state for unmatched action type', () => {
    const oldState = Object.assign(initialState(), { unexpectedError: true });

    const newState = reducer(oldState, { type: 'NOT_HANDLED' });

    expect(newState).toEqual(oldState);
  });
});
