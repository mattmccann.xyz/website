/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Redux reducer for the LetsChat feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import { IAction } from 'state/action.type';

import * as c from './lets-chat.constants';


export interface ILetsChatState {
  email: {
    /** Description of the error with the email value */
    error?: string;

    /** The value of the email field */
    value: string;
  };

  message: {
    /** Description of the error with the message value */
    error?: string;

    /** The value of the message field */
    value: string;
  };

  name: {
    /** Description of the error with the name value */
    error?: string;

    /** The value of the message name */
    value: string;
  };

  /* State of the submit network request */
  submit: {
    error?: Error;
    isComplete: boolean;
    isError: boolean;
    isInProgress: boolean;
  };
}

/** Initial state for the feature */
export const initialState = (): ILetsChatState => ({
  email: {
    value: '',
  },
  message: {
    value: '',
  },
  name: {
    value: '',
  },
  submit: {
    isComplete: false,
    isError: false,
    isInProgress: false,
  },
});

/**
 * Reducer for the feature state.
 *
 * @param state The state we are updating
 * @param action The action we are reducing
 * @returns The updated state
 */
export const reducer = (state = initialState(), action: IAction): ILetsChatState => {
  switch (action.type) {
    case c.CHANGED_EMAIL:
      return {
        ...state,
        email: {
          value: action.payload,
        },
      };

    case c.CHANGED_MESSAGE:
      return {
        ...state,
        message: {
          value: action.payload,
        },
      };

    case c.CHANGED_NAME:
      return {
        ...state,
        name: {
          value: action.payload,
        },
      };

    case c.MALFORMED_EMAIL:
      return {
        ...state,
        email: {
          error: 'Please provide a valid email',
          value: state.email.value,
        },
      };

    case c.MISSING_EMAIL:
      return {
        ...state,
        email: {
          error: 'Please provide your email',
          value: state.email.value,
        },
      };

    case c.MISSING_MESSAGE:
      return {
        ...state,
        message: {
          error: 'Please provide a message',
          value: state.message.value,
        },
      };

    case c.MISSING_NAME:
      return {
        ...state,
        name: {
          error: 'Please provide your name',
          value: state.name.value,
        },
      };

    case c.RESET_FEATURE_STATE:
      return initialState();

    case c.SUBMIT_COMPLETED:
      return {
        ...state,
        submit: {
          isComplete: true,
          isError: false,
          isInProgress: false,
        },
      };

    case c.SUBMIT_FAILED:
      return {
        ...state,
        submit: {
          error: action.payload,
          isComplete: true,
          isError: true,
          isInProgress: false,
        },
      };

    case c.SUBMIT_STARTED:
      return {
        ...state,
        submit: {
          isComplete: false,
          isError: false,
          isInProgress: true,
        },
      };

    default:
      return state;
  }
};
