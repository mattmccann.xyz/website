/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the LetsChat state actions
 * @copyright &copy; 2018 R. Matt McCann
 */

import mockDispatch from 'mocks/mock-dispatch';
import mockServices from 'mocks/mock-services';
import { IState } from 'state/state.type';
import { Dispatch, GetState, IServices } from 'state/thunk.type';

import * as actions from './lets-chat.actions';
import * as c from './lets-chat.constants';
import { initialState } from './lets-chat.reducer';


describe('features/lets-chat/state/actions', () => {
  let dispatch: Dispatch;
  let getState: GetState;
  let services: IServices;
  let state: IState;

  beforeEach(() => {
    state = {
      features: {
        letsChat: initialState(),
      }
    } as IState;
    getState = () => state;

    services = mockServices();
    (services.api.messages().create as any).mockReturnValue(Promise.resolve());
    services.window.scrollTo = jest.fn();

    dispatch = mockDispatch(getState, services);
  });

  it('onChangeEmail', () => {
    const action = actions.onChangeEmail('bob@thisoldehome.org');

    expect(action).toEqual({ payload: 'bob@thisoldehome.org', type: c.CHANGED_EMAIL });
  });

  it('onChangeMessage', () => {
    const action = actions.onChangeMessage('message!');

    expect(action).toEqual({ payload: 'message!', type: c.CHANGED_MESSAGE });
  });

  it('onChangeName', () => {
    const action = actions.onChangeName('Bob Villa');

    expect(action).toEqual({ payload: 'Bob Villa', type: c.CHANGED_NAME });
  });

  describe('onClickSubmit', () => {
    describe('on bad inputs', () => {
      it('dispatches missing name', async () => {
        await dispatch(actions.onClickSubmit());

        expect(dispatch).toHaveBeenCalledWith({ type: c.MISSING_NAME });
      });

      it('dispatches missing email', async () => {
        await dispatch(actions.onClickSubmit());

        expect(dispatch).toHaveBeenCalledWith({ type: c.MISSING_EMAIL });
      });

      it('dispatches malformed email', async () => {
        state.features.letsChat.email.value = 'bademail@';

        await dispatch(actions.onClickSubmit());

        expect(dispatch).toHaveBeenCalledWith({ type: c.MALFORMED_EMAIL });
      });

      it('dispatches missing message', async () => {
        await dispatch(actions.onClickSubmit());

        expect(dispatch).toHaveBeenCalledWith({ type: c.MISSING_MESSAGE });
      });

      it('does not dispatch submit started', async () => {
        await dispatch(actions.onClickSubmit());

        expect(dispatch).not.toHaveBeenCalledWith({ type: c.SUBMIT_STARTED });
      });
    });

    describe('on good inputs', () => {
      beforeEach(() => {
        state.features.letsChat.email.value = 'me@mattmccann.xyz';
        state.features.letsChat.message.value = 'What is crackin?';
        state.features.letsChat.name.value = 'Bob Villa';
      });

      it('immediately dispatches submit started', async () => {
        const dispatchPromise = await dispatch(actions.onClickSubmit());

        expect(dispatch).toHaveBeenCalledWith({ type: c.SUBMIT_STARTED });

        await dispatchPromise;
      });

      it('fires the message create api call', async () => {
        await dispatch(actions.onClickSubmit());

        expect(services.api.messages().create).toHaveBeenCalledWith({
          email: 'me@mattmccann.xyz', message: 'What is crackin?', name: 'Bob Villa',
        });
      });

      describe('on success', () => {
        it('dispatches submit completed', async () => {
          await dispatch(actions.onClickSubmit());

          expect(dispatch).toHaveBeenCalledWith({ type: c.SUBMIT_COMPLETED });
        });
      });

      describe('on failure', () => {
        const error = new Error('Oh no!');

        beforeEach(() => {
          (services.api.messages().create as any).mockReturnValue(Promise.reject(error));
        });

        it('dispatches submit failed and propagates', async () => {
          try {
            await dispatch(actions.onClickSubmit());

            expect(false).toBe(true); // Should have propagated error
          } catch (caughtError) {
            expect(caughtError).toBe(error);

            expect(dispatch).toHaveBeenCalledWith({
              payload: error, type: c.SUBMIT_FAILED,
            });
          }
        });
      });
    });

  });

  describe('resetFeatureState', () => {
    it('resets the state', () => {
      dispatch(actions.resetFeatureState());

      expect(dispatch).toHaveBeenCalledWith({ type: c.RESET_FEATURE_STATE });
    });

    it('resets the scroll position', () => {
      dispatch(actions.resetFeatureState());

      expect(services.window.scrollTo).toHaveBeenCalledWith(0, 0);
    });
  });
});
