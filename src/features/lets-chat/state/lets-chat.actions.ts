/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Redux actions for the LetsChat feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import { IAction } from 'state/action.type';
import { Dispatch, GetState, IServices } from 'state/thunk.type';

import * as c from './lets-chat.constants';


const malformedEmail = () => ({ type: c.MALFORMED_EMAIL });

const missingEmail = () => ({ type: c.MISSING_EMAIL });

const missingMessage = () => ({ type: c.MISSING_MESSAGE });

const missingName = () => ({ type: c.MISSING_NAME });

export const onChangeEmail = (email: string): IAction => ({
  payload: email, type: c.CHANGED_EMAIL,
});

export const onChangeMessage = (message: string): IAction => ({
  payload: message, type: c.CHANGED_MESSAGE,
});

export const onChangeName = (name: string): IAction => ({
  payload: name, type: c.CHANGED_NAME,
});

export const onClickSubmit = () => async (dispatch: Dispatch, getState: GetState, services: IServices) => {
  let isInputError = false;
  const letsChatState = getState().features.letsChat;

  // Check that the name field is not empty
  if (letsChatState.name.value === '') {
    // Dispatch that the name field is missing
    dispatch(missingName());

    isInputError = true;
  }

  // Check that the email field is not empty
  if (letsChatState.email.value === '') {
    // Dispatch that the email field is missing
    dispatch(missingEmail());

    isInputError = true;
  }

  // Check that the email field is not malformed
  const emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (letsChatState.email.value && !emailRegex.test(letsChatState.email.value)) {
    // Dispatch that the email field is malformed
    dispatch(malformedEmail());

    isInputError = true;
  }

  // Check that the message field is not missing
  if (letsChatState.message.value === '') {
    // Dispatch that the email field is missing
    dispatch(missingMessage());

    isInputError = true;
  }

  // If there was malformed input
  if (isInputError) {
    return;
  }

  // Dispatch that we are starting to submit the message
  dispatch(submitStarted());

  try {
    // Submit the message
    await services.api.messages().create({
      email: letsChatState.email.value,
      message: letsChatState.message.value,
      name: letsChatState.name.value,
    });

    // Dispatch that we successfully submitted the message
    dispatch(submitCompleted());
  } catch (error) {
    // Dispatch the we failed to failed to submit the message
    dispatch(submitFailed(error));

    // Propogate up the error
    throw error;
  }
};

/** Action creator triggered to reset the feature state */
export const resetFeatureState = () => (dispatch: Dispatch, getState: GetState, services: IServices) => {
  // Make sure we are scrolled to the top
  services.window.scrollTo(0, 0);

  // Reset the feature state
  dispatch({ type: c.RESET_FEATURE_STATE });
};

const submitCompleted = () => ({ type: c.SUBMIT_COMPLETED });

const submitFailed = (error: any) => ({
  payload: error, type: c.SUBMIT_FAILED,
});

const submitStarted = () => ({ type: c.SUBMIT_STARTED });
