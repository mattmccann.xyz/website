/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the LetsChat feature container
 * @copyright &copy; 2018 R. Matt McCann
 */

import { ShallowWrapper } from 'enzyme';
import * as React from 'react';

import { LetsChat, LetsChatContainer } from 'features/lets-chat';
import shallowWithStore from 'mocks/shallow-with-store';


describe('features/lets-caht/container', () => {
  let component: ShallowWrapper;

  beforeEach(() => {
    const state = {
      features: {
        letsChat: {
          email: {
            value: 'bob@thisoldehome.org',
          },
          message: {
            value: 'Hi!',
          },
          name: {
            value: 'Bob Villa',
          },
          submit: {
            isComplete: true,
            isError: true,
            isInProgress: false,
          },
        },
      },
    };

    component = shallowWithStore(<LetsChat />, state);
  });

  it('maps the state to the props', () => {
    expect(component.prop('email')).toEqual({ value: 'bob@thisoldehome.org' });
    expect(component.prop('message')).toEqual({ value: 'Hi!' });
    expect(component.prop('name')).toEqual({ value: 'Bob Villa' });
    expect(component.prop('submit')).toEqual({ isComplete: true, isError: true, isInProgress: false });
  });

  it('maps the actions to the props', () => {
    expect(component.prop('onChangeEmail')).toBeDefined();
    expect(component.prop('onChangeMessage')).toBeDefined();
    expect(component.prop('onChangeName')).toBeDefined();
    expect(component.prop('onClickSubmit')).toBeDefined();
    expect(component.prop('resetFeatureState')).toBeDefined();
  });

  it('fires the reset feature state action when mounted', () => {
    const resetFeatureState = jest.fn();
    component.setProps({ resetFeatureState });

    const instance = component.instance() as LetsChatContainer;
    instance.componentDidMount();

    expect(resetFeatureState).toHaveBeenCalled();
  });
});
