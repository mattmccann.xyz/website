/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Styles for the LetsChat feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import { CSSProperties } from 'react';

import * as v from 'styles/variables';


export default {
  complete: {
    paddingBottom: '3em',
    paddingTop: '3em',
    textAlign: 'center',
  } as CSSProperties,

  container: {
    color: v.COLOR_DARK_BASIC,
    fontFamily: 'Quicksand, sans-serif',
    marginBottom: '5em',
    marginLeft: 'auto',
    marginRight: 'auto',
    maxWidth: '40em',
    paddingLeft: '1em',
    paddingRight: '1em',
    paddingTop: '10em',
    textAlign: 'center',
  } as CSSProperties,

  form: {
    button: {
      marginLeft: 'auto',
      marginRight: 'auto',
      width: '10em',
    } as CSSProperties,

    container: {
      fontSize: '1.2em',
      marginTop: '5em',
      textAlign: 'left',
    } as CSSProperties,

    error: {
      color: v.COLOR_RED_PRIMARY,
    } as CSSProperties,

    field: {
      marginBottom: '1.5em',
    } as CSSProperties,

    input: {
      borderColor: v.COLOR_GRAY_PRIMARY,
      borderRadius: '4px',
      borderStyle: 'solid',
      borderWidth: '1px',
      boxShadow: 'none',
      paddingBottom: '0.5em',
      paddingLeft: '0.75em',
      paddingRight: '0.75em',
      paddingTop: '0.5em',
      width: '100%',
    } as CSSProperties,

    label: {
      color: v.COLOR_GRAY_DARK,
    } as CSSProperties,

    messageInput: {
      minHeight: '5em',
    } as CSSProperties,
  }
};
