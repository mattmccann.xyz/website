/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief View for the Menu feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as React from 'react';

import { Button } from 'components/button/index';
import { Link } from 'components/link';
import Responsive from 'components/responsive';
import { VisualAnchor } from 'components/visual-anchor';

import styles from './menu.styles';


export interface IMenuViewProps {
  /** True if the menu is in back mode */
  isBackMode?: boolean;

  /** True if the menu is in sticky mode */
  isSticky: boolean;

  /** Click handler for the About link */
  onClickAbout: () => void;

  /** Click handler for the anchor link */
  onClickAnchor: () => void;

  /** Click handler for the back button */
  onClickBack: () => void;

  /** Click handler for the Let's Chat button */
  onClickLetsChat: () => void;

  /** Click handler for the Projects link */
  onClickProjects: () => void;

  /** Click handler for the Testimonials link */
  onClickTestimonials: () => void;
}

export default class MenuView extends React.PureComponent<IMenuViewProps> {
  public render() {
    if (this.props.isSticky) {
      return this.renderSticky();
    } else {
      return this.renderNotSticky();
    }
  }

  private renderLeftSide = (isSticky: boolean) => (
    <div style={styles.leftSide}>
      {/* Visual anchor that links back to home */}
      <VisualAnchor id="anchor"
        containerStyle={styles.anchorContainer} fontStyle={styles.anchorFont(isSticky)}
        onClick={this.props.onClickAnchor} />

      {
        !this.props.isBackMode &&
        <Responsive xs sm md lg>
          <Link to="/about" id="about" onClick={this.props.onClickAbout} style={styles.link(isSticky)}>About</Link>
          <Link to="/projects" id="projects" onClick={this.props.onClickProjects}
                style={styles.link(isSticky)}>Projects</Link>
          <Link to="/testimonials" id="testimonials" onClick={this.props.onClickTestimonials}
                style={styles.link(isSticky)}>Testimonials</Link>
        </Responsive>
      }
    </div>
  );

  private renderRightSide = (isSticky: boolean) => (
    <div style={styles.rightSide(isSticky)}>
      {
        !this.props.isBackMode &&
        <Button light={isSticky} onClick={this.props.onClickLetsChat} style={styles.letsChatButton(isSticky)}>
          Let's Chat
        </Button>
      }
      {
        this.props.isBackMode &&
        <Button light={isSticky} onClick={this.props.onClickBack} style={styles.backButton(isSticky)}>
          <i className="fas fa-times" />
        </Button>
      }
    </div>
  );

  private renderSticky() {
    return (
      <div style={styles.sticky.containerOuter}>
        <div style={styles.sticky.containerInner}>
          { this.renderLeftSide(true) }
          { this.renderRightSide(true) }
        </div>
      </div>
    )
  }

  private renderNotSticky() {
    return (
      <div style={styles.notSticky.containerOuter}>
        <div style={styles.notSticky.containerInner}>
          { this.renderLeftSide(false) }
          { this.renderRightSide(false) }
        </div>
      </div>
    );
  }
}

