/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Container for the Menu feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as React from 'react';
import { connect } from 'react-redux';

import { IState } from 'state/state.type';

import MenuView, { IMenuViewProps } from './menu.view';
import * as actions from './state/menu.actions';


interface IPropTypes extends IMenuViewProps {
  onLoadRequestScroll: () => void;
  startListenForScroll: () => void;
  stopListenForScroll: () => void;
}

interface IMenuPublicProps {
  isBackMode?: boolean;
}

export class MenuContainer extends React.Component<IPropTypes> {
  public componentDidMount() {
    this.props.onLoadRequestScroll();
    this.props.startListenForScroll();
  }

  public componentWillUnmount() {
    this.props.stopListenForScroll();
  }

  /** Renders the component */
  public render() {
    return (
      <MenuView {...this.props} />
    )
  }
}

/** Maps the actions needed to the component props */
const mapDispatchToProps = {
  ...actions,
};

/** Maps the Redux store to the component props */
const mapStateToProps = (state: IState, ownProps: IMenuPublicProps) => ({
  ...state.features.menu,
  ...ownProps,
});

// Connect the Redux store to the component
export const ConnectedMenuContainer = connect(mapStateToProps, mapDispatchToProps)(MenuContainer);
