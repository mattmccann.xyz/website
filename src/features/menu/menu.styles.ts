/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Styling for the Menu feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import { CSSProperties } from 'react';

import * as v from 'styles/variables';


const lineHeightNotSticky = '3em';
const lineHeightSticky = '2.2em';

export default {
  anchorContainer: {
    float: 'left',
  } as CSSProperties,

  anchorFont: (isSticky: boolean) => ({
    color: isSticky ? v.COLOR_DARK_PRIMARY : v.COLOR_DARK_PRIMARY,
    fontSize: isSticky ? lineHeightSticky : lineHeightNotSticky,
  } as CSSProperties),

  backButton: (isSticky: boolean) => ({
    borderRadius: '5px',
    color: isSticky ? v.COLOR_DARK_PRIMARY : v.COLOR_DARK_PRIMARY,
    fontSize: '1.5em',
    paddingBottom: '4px',
    paddingTop: '4px',
    position: 'relative',
    top: '50%',
    transform: 'translateY(-50%)',
  }),

  leftSide: {
    float: 'left' as CSSProperties['float'],
  },

  letsChatButton: (isSticky: boolean) => ({
    color: isSticky ? v.COLOR_DARK_PRIMARY : v.COLOR_DARK_PRIMARY,
    position: 'relative',
    top: '50%',
    transform: 'translateY(-50%)',
  } as CSSProperties),

  link: (isSticky: boolean) => ({
    color: isSticky ? v.COLOR_DARK_SECONDARY : v.COLOR_DARK_SECONDARY,
    display: 'block',
    float: 'left',
    lineHeight: isSticky ? lineHeightSticky : lineHeightNotSticky,
    marginLeft: '2em',
  } as CSSProperties),

  notSticky: {
    containerOuter: {
      left: '50%',
      maxWidth: '80em',
      padding: '1.5em',
      position: 'absolute',
      transform: 'translateX(-50%)',
      width: '100%',
      zIndex: 2,
    } as CSSProperties,

    containerInner: {
      position: 'relative',
    } as CSSProperties,
  },

  rightSide: (isSticky: boolean) => ({
    float: 'right',
    height: isSticky ? lineHeightSticky : lineHeightNotSticky,
    verticalAlign: 'middle',
  } as CSSProperties),

  sticky: {
    containerInner: {
      color: v.COLOR_LIGHT_PRIMARY,
      display: 'inline-block',
      maxWidth: '80em',
      paddingBottom: '0.5em',
      paddingLeft: '0.5em',
      paddingRight: '0.5em',
      paddingTop: '1em',
      width: '100%',
    } as CSSProperties,

    containerOuter: {
      backgroundColor: v.COLOR_LIGHT_PRIMARY,
      borderBottom: `1px solid ${v.COLOR_GRAY_PRIMARY}`,
      left: 0,
      position: 'fixed',
      textAlign: 'center',
      top: 0,
      width: '100%',
      zIndex: 1000,
    } as CSSProperties,
  }
};
