/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the Menu view
 * @copyright &copy; 2018 R. Matt McCann
 */

import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';

import MenuView from 'features/menu/menu.view';


describe('features/menu/view', () => {
  let component: ShallowWrapper;
  let onClickAbout: () => void;
  let onClickAnchor: () => void;
  let onClickBack: () => void;
  let onClickLetsChat: () => void;
  let onClickProjects: () => void;
  let onClickTestimonials: () => void;

  beforeEach(() => {
    onClickAbout = jest.fn().mockName('onClickAbout');
    onClickAnchor = jest.fn().mockName('onClickAnchor');
    onClickBack = jest.fn().mockName('onClickBack');
    onClickLetsChat = jest.fn().mockName('onClickLetsChat');
    onClickProjects = jest.fn().mockName('onClickProjects');
    onClickTestimonials = jest.fn().mockName('onClickTestimonials');

    component = shallow(<MenuView
      isSticky={true}
      onClickAbout={onClickAbout}
      onClickAnchor={onClickAnchor}
      onClickBack={onClickBack}
      onClickLetsChat={onClickLetsChat}
      onClickProjects={onClickProjects}
      onClickTestimonials={onClickTestimonials}
    />);
  });

  describe('render', () => {
    describe('is sticky', () => {
      describe('is back mode', () => {
        beforeEach(() => {
          component.setProps({ isBackMode: true });
        });

        it('renders as expected', () => {
          expect(component).toMatchSnapshot();
        });
      });

      describe('is not back mode', () => {
        it('renders as expected', () => {
          expect(component).toMatchSnapshot();
        });
      });
    });

    describe('is not sticky', () => {
      beforeEach(() => {
        component.setProps({ isSticky: false });
      });

      describe('is back mode', () => {
        beforeEach(() => {
          component.setProps({ isBackMode: true });
        });

        it('renders as expected', () => {
          expect(component).toMatchSnapshot();
        });
      });

      describe('is not back mode', () => {
        it('renders as expected', () => {
          expect(component).toMatchSnapshot();
        });
      });
    });
  });

  it('binds the onClickAbout handler', () => {
    const link = component.find('[id="about"]');

    expect(link.props().onClick).toBe(onClickAbout);
  });

  it('binds the onClickAnchor handler', () => {
    const link = component.find('[id="anchor"]');

    expect(link.props().onClick).toBe(onClickAnchor);
  });

  it('binds the onClickLetsChat handler', () => {
    const button = component.find('Button');

    expect(button.props().onClick).toBe(onClickLetsChat);
  });

  it('binds the onClickProjects handler', () => {
    const link = component.find('[id="projects"]');

    expect(link.props().onClick).toBe(onClickProjects);
  });

  it('binds the onClickTestimonials handler', () => {
    const link = component.find('[id="testimonials"]');

    expect(link.props().onClick).toBe(onClickTestimonials);
  });
});
