import { ConnectedMenuContainer, MenuContainer } from './menu.container';
import MenuView from './menu.view';

export {
  ConnectedMenuContainer,
  ConnectedMenuContainer as Menu,
  MenuContainer,
  MenuView
};
