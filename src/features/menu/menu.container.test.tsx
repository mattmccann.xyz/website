/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the Menu container
 * @copyright &copy; 2018 R. Matt McCann
 */

import { ShallowWrapper } from 'enzyme';
import * as React from 'react';

import { Menu, MenuContainer } from 'features/menu';
import shallowWithStore from 'mocks/shallow-with-store';


describe('features/menu/container', () => {
  let component: ShallowWrapper;
  let instance: MenuContainer;
  let startListenForScroll: () => void;
  let stopListenForScroll: () => void;

  beforeEach(() => {
    startListenForScroll = jest.fn();
    stopListenForScroll = jest.fn();

    component = shallowWithStore(<Menu />, {
      features: {
        menu: {
          isSticky: false,
        },
      },
    });
    component.setProps({
      startListenForScroll, stopListenForScroll,
    });

    instance = component.instance() as MenuContainer;
  });

  it('starts scroll listen on mount', () => {
    instance.componentDidMount();

    expect(startListenForScroll).toHaveBeenCalled();
  });

  it('stops scroll listen on unmount', () => {
    instance.componentWillUnmount();

    expect(stopListenForScroll).toHaveBeenCalled();
  });

  describe('render', () => {
    it('renders as expected', () => {
      expect(component).toMatchSnapshot();
    });
  });
});
