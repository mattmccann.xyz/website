/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief State constants for the Menu feature
 * @copyright &copy; 2018 R. Matt McCann
 */

export const CLEAR_REQUEST_SCROLL = 'MENU_CLEAR_REQUEST_SCROLL';
export const MAKE_NOT_STICKY = 'MENU_MAKE_NOT_STICKY';
export const MAKE_STICKY = 'MENU_MAKE_STICKY';
export const REQUEST_SCROLL = 'MENU_REQUEST_SCROLL';
