/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the Menu state actions
 * @copyright &copy; 2018 R. Matt McCann
 */

import { goBack, push } from 'connected-react-router'

import mockDispatch from 'mocks/mock-dispatch';
import mockServices from 'mocks/mock-services';
import { Dispatch, GetState, IServices } from 'state/thunk.type';

import * as actions from './menu.actions';
import * as c from './menu.constants';


describe('features/menu/state/actions', () => {
  let addEventListener: (event: string, cb: () => void) => void;
  let dispatch: Dispatch;
  let getState: GetState;
  let removeEventListener: (event: string, cb: () => void) => void;
  let services: IServices;

  beforeEach(() => {
    getState = jest.fn();
    services = mockServices();
    dispatch = mockDispatch(getState, services);

    addEventListener = jest.fn();
    removeEventListener = jest.fn();
    services.window = {
      addEventListener,
      removeEventListener,
    } as Window;
  });

  it('clearRequestScroll', () => {
    expect(actions.clearRequestScroll()).toEqual({ type: c.CLEAR_REQUEST_SCROLL });
  });

  it('onLoadRequestScroll', () => {
    (getState as any).mockReturnValue({
      router: { location: { pathname: '/about' } },
    });

    dispatch(actions.onLoadRequestScroll());

    expect(dispatch).toHaveBeenCalledWith({ type: c.REQUEST_SCROLL, payload: '/about'});
  });

  describe('onClickAnchor', () => {
    it('scrolls if clicked within the context of the landing page', () => {
      (getState as any).mockReturnValue({
        router: { location: { pathname: '/' } },
      });

      dispatch(actions.onClickAnchor());

      expect(dispatch).toHaveBeenCalledWith({ type: c.REQUEST_SCROLL, payload: '/' });
    });

    it('pops the current location outside of landing page', () => {
      (getState as any).mockReturnValue({
        router: { location: { pathname: '/lets-chat' } },
      });

      dispatch(actions.onClickAnchor());

      expect(dispatch).toHaveBeenCalledWith(goBack());
    });
  });

  it('onClickAbout', () => {
    expect(actions.onClickAbout()).toEqual({ type: c.REQUEST_SCROLL, payload: '/about' });
  });

  it('onClickBack', () => {
    dispatch(actions.onClickBack());

    expect(dispatch).toHaveBeenCalledWith(goBack());
  });

  it('onClickLetsChat', () => {
    dispatch(actions.onClickLetsChat());

    expect(dispatch).toHaveBeenCalledWith(push('/lets-chat'));
  });

  it('onClickProjects', () => {
    expect(actions.onClickProjects()).toEqual({ type: c.REQUEST_SCROLL, payload: '/projects' });
  });

  it('onClickTestimonials', () => {
    expect(actions.onClickTestimonials()).toEqual({ type: c.REQUEST_SCROLL, payload: '/testimonials' });
  });

  describe('startListenForScroll', () => {
    it('binds the scroll listener', () => {
      dispatch(actions.startListenForScroll());

      expect((addEventListener as any).mock.calls[0].length).toBe(2);
      expect((addEventListener as any).mock.calls[0][0]).toEqual('scroll');
    });

    describe('is already sticky', () => {
      beforeEach(() => {
        (getState as any).mockReturnValue({
          features: {
            menu: {
              isSticky: true,
            },
          },
        });

        // Bind the event listener
        dispatch(actions.startListenForScroll());
      });

      it('does not dispatch the make sticky action when below threshold', () => {
        services.window = {
          scrollY: 200,
        } as Window;

        (addEventListener as any).mock.calls[0][1]();

        expect(dispatch).not.toHaveBeenCalledWith({ type: c.MAKE_STICKY });
      });

      it('dispatches the make not sticky action when above threshold', () => {
        services.window = {
          scrollY: 50,
        } as Window;

        (addEventListener as any).mock.calls[0][1]();

        expect(dispatch).toHaveBeenCalledWith({ type: c.MAKE_NOT_STICKY });
      });
    });

    describe('is not already sticky', () => {
      beforeEach(() => {
        (getState as any).mockReturnValue({
          features: {
            menu: {
              isSticky: false,
            },
          },
        });

        // Bind the event listener
        dispatch(actions.startListenForScroll());
      });

      it('dispatches the make sticky action when below threshold', () => {
        services.window = {
          scrollY: 200,
        } as Window;

        (addEventListener as any).mock.calls[0][1]();

        expect(dispatch).toHaveBeenCalledWith({ type: c.MAKE_STICKY });
      });

      it('does not dispatch the make not sticky action when above threshold', () => {
        services.window = {
          scrollY: 50,
        } as Window;

        (addEventListener as any).mock.calls[0][1]();

        expect(dispatch).not.toHaveBeenCalledWith({ type: c.MAKE_NOT_STICKY });
      });
    });
  });

  it('stopListenForScroll unbinds listener', () => {
    dispatch(actions.stopListenForScroll());

    expect((removeEventListener as any).mock.calls[0].length).toBe(2);
    expect((removeEventListener as any).mock.calls[0][0]).toEqual('scroll');
  });
});
