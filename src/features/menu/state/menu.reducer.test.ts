/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the Menu feature reducer
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as actions from './menu.actions';
import * as c from './menu.constants';
import { initialState, reducer } from './menu.reducer';


describe('features/menu/state/reducer', () => {
  it('initializes the state', () => {
    const expectedState = initialState();
    const newState = reducer(undefined, { type: 'NOT_HANDLED' });

    expect(newState).toEqual(expectedState);
  });

  it('reduces CLEAR_REQUEST_SCROLL', () => {
    const expectedState = initialState();
    const oldState = Object.assign(initialState(), { requestedRouteScroll: 'derp' });

    const newState = reducer(oldState, actions.clearRequestScroll());

    expect(newState).toEqual(expectedState);
  });

  it('reduces MAKE_NOT_STICKY', () => {
    const expectedState = initialState();
    const oldState = Object.assign(initialState(), { isSticky: true });

    const newState = reducer(oldState, { type: c.MAKE_NOT_STICKY });

    expect(newState).toEqual(expectedState);
  });

  it('reduces MAKE_STICKY', () => {
    const expectedState = Object.assign(initialState(), { isSticky: true });

    const newState = reducer(undefined, { type: c.MAKE_STICKY });

    expect(newState).toEqual(expectedState);
  });

  it('reduces REQUEST_ROUTE_SCROLL', () => {
    const expectedState = Object.assign(initialState(), { requestedRouteScroll: 'derp' });

    const newState = reducer(undefined, { type: c.REQUEST_SCROLL, payload: 'derp' });

    expect(newState).toEqual(expectedState);
  });

  it('passes along state for unmatched action type', () => {
    const oldState = Object.assign(initialState(), { isSticky: true });

    const newState = reducer(oldState, { type: 'NOT_HANDLED' });

    expect(newState).toEqual(oldState);
  });
});
