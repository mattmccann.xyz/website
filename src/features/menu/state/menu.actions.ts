/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief State actions for the Menu feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import { goBack, push } from 'connected-react-router';

import { Dispatch, GetState, IServices } from 'state/thunk.type';

import * as c from './menu.constants';


export const clearRequestScroll = () => ({ type: c.CLEAR_REQUEST_SCROLL });

const makeSticky = () => ({ type: c.MAKE_STICKY });

export const onLoadRequestScroll = () => (dispatch: Dispatch, getState: GetState) => {
  const location = getState().router.location.pathname;

  dispatch({ type: c.REQUEST_SCROLL, payload: location });
};

let scrollHandler: () => void;

export const onClickAnchor = () => (dispatch: Dispatch, getState: GetState) => {
  // If the we are using the anchor to move around the landing page
  if (['/', '/about', '/projects', '/testimonials'].indexOf(getState().router.location.pathname) !== -1) {
    // Scroll back to the the top
    dispatch({type: c.REQUEST_SCROLL, payload: '/'});
  }
  // If we are not on the landing page
  else {
    // Pop off this page and go back
    dispatch(goBack());
  }
};

export const onClickAbout = () => ({ type: c.REQUEST_SCROLL, payload: '/about' });

export const onClickBack = () => (dispatch: Dispatch) => {
  dispatch(goBack());
};

export const onClickLetsChat = () => (dispatch: Dispatch) => {
  // Navigate to the lets-chat page
  dispatch(push('/lets-chat'));
};

export const onClickProjects = () => ({ type: c.REQUEST_SCROLL, payload: '/projects' });

export const onClickTestimonials = () => ({ type: c.REQUEST_SCROLL, payload: '/testimonials' });

export const startListenForScroll = () => (dispatch: Dispatch, getState: GetState, services: IServices) => {
  scrollHandler = () => {
    const stickyThreshold = 100;
    const { isSticky } = getState().features.menu;

    // If the user has scrolled sufficiently down and the menu is not already stuck
    if (services.window.scrollY > stickyThreshold && !isSticky) {
      dispatch(makeSticky());
    }

    // If the user has scrolled sufficiently up and the menu is already sticky
    if (services.window.scrollY < stickyThreshold && isSticky) {
      dispatch(makeNotSticky());
    }
  };

  services.window.addEventListener('scroll', scrollHandler);
};

export const stopListenForScroll = () => (dispatch: Dispatch, getState: GetState, services: IServices) => {
  services.window.removeEventListener('scroll', scrollHandler);
};

const makeNotSticky = () => ({ type: c.MAKE_NOT_STICKY });

