/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief State reducer for the Menu feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import { IAction } from 'state/action.type';

import * as c from './menu.constants';


/** The state for the menu feature */
export interface IMenuState {
  /** True if the menu is sticking to the top of the window */
  isSticky: boolean;

  /** The section we are requesting to scroll to */
  requestedRouteScroll?: string;
}

/** Initial state for the feature */
export const initialState = (): IMenuState => ({
  isSticky: false,
});

/**
 * Reducer for the feature state.
 *
 * @param state The state we are updating
 * @param action The action we are reducing
 * @returns The updated state
 */
export const reducer = (state = initialState(), action: IAction): IMenuState => {
  switch (action.type) {
    case c.CLEAR_REQUEST_SCROLL: {
      const newState = { ...state };

      delete newState.requestedRouteScroll;

      return newState;
    }

    case c.MAKE_NOT_STICKY:
      return {
        ...state,
        isSticky: false,
      };

    case c.MAKE_STICKY:
      return {
        ...state,
        isSticky: true,
      };

    case c.REQUEST_SCROLL:
      return {
        ...state,
        requestedRouteScroll: action.payload,
      };

    default:
      return state;
  }
};
