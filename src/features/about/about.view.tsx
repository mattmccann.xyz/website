/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief View for the About feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as React from 'react';
import { Element } from 'react-scroll';

import styles from './about.styles';


export default () => (
  <div>
    <Element name="about-element" />
    <div style={styles.containerOuter} id="/about">
      <div style={styles.containerInner}>
        <h1 style={styles.title}>
          Hi, I'm Matt. It's great to meet you!
        </h1>

        <p style={styles.body}>
          In the past 9 years, I've led developer teams, been awarded a patent, and founded a startup that hit
          $500k in revenue in its first 18 months. I've written production code
          for embedded platforms, distributed cloud platforms, and everything in between.
          I am a startup enthusiast, a software fanatic, and I love what I do.
        </p>
      </div>
    </div>
  </div>
);
