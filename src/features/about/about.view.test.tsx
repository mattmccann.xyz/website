/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the About view
 * @copyright &copy; 2018 R. Matt McCann
 */

import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';

import { About } from 'features/about';


describe('features/about/view', () => {
  let component: ShallowWrapper;

  beforeEach(() => {
    component = shallow(<About />);
  });

  it('renders as expected', () => {
    expect(component).toMatchSnapshot();
  });
});
