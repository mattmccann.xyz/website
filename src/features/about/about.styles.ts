/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Styles for the About feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import { CSSProperties } from "react";

import * as v from 'styles/variables';


export default {
  body: {
    color: v.COLOR_LIGHT_PRIMARY,
    fontFamily: 'Quicksand, sans-serif',
    fontSize: '1.15em',
    fontWeight: 300 as number,
    lineHeight: '1.6em',
    marginTop: '1.5em',
  } as CSSProperties,

  containerOuter: {
    backgroundColor: v.COLOR_DARK_PRIMARY,
    marginTop: '-8em',
    textAlign: 'center',
    width: '100%',
  } as CSSProperties,

  containerInner: {
    marginLeft: 'auto',
    marginRight: 'auto',
    maxWidth: '55em',
    paddingBottom: '15em',
    paddingLeft: '1em',
    paddingRight: '1em',
    paddingTop: '10em',
    textAlign: 'center',
  } as CSSProperties,

  title: {
    color: v.COLOR_LIGHT_PRIMARY,
    fontFamily: '"Permanent Marker", sans-serif',
    fontSize: '2.0em',
    fontWeight: 400,
    marginTop: '2em',
  } as CSSProperties,
}
