/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Reducer for the features state
 * @copyright &copy; 2018 R. Matt McCann
 */

import { combineReducers } from 'redux';

import { ICompaniesState, reducer as companies } from './companies/state/companies.reducer';
import { ILetsChatState, reducer as letsChat } from './lets-chat/state/lets-chat.reducer';
import { IMenuState, reducer as menu } from './menu/state/menu.reducer';
import { IMyWorkState, reducer as myWork } from './my-work/state/my-work.reducer';
import { ITestimonialsState, reducer as testimonials } from './testimonials/state/testimonials.reducer';
import { IWorkFilterState, reducer as workFilter } from './work-filter/state/work-filter.reducer';

export interface IFeaturesState {
  companies: ICompaniesState;
  letsChat: ILetsChatState;
  menu: IMenuState;
  myWork: IMyWorkState;
  testimonials: ITestimonialsState;
  workFilter: IWorkFilterState;
}

export const reducer = combineReducers({
  companies,
  letsChat,
  menu,
  myWork,
  testimonials,
  workFilter,
});
