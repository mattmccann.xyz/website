/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the FinalCTA container
 * @copyright &copy; 2018 R. Matt McCann
 */

import { ShallowWrapper } from 'enzyme';
import * as React from 'react';

import { FinalCta, FinalCtaContainer } from 'features/final-cta';
import shallowWithStore from 'mocks/shallow-with-store';


describe('features/final-cta/container', () => {
  let component: ShallowWrapper;

  beforeEach(() => {
    component = shallowWithStore(<FinalCta />, { });
  });

  describe('onClickReachOut', () => {
    it('navigates to the lets chat page', () => {
      const push = jest.fn();
      component.setProps({ push });

      const instance = component.instance() as FinalCtaContainer;
      instance.onClickReachOut();

      expect(push).toHaveBeenCalledWith('/lets-chat');
    });
  });

  describe('render', () => {
    it('renders as expected', () => {
      expect(component).toMatchSnapshot();
    });
  });
});
