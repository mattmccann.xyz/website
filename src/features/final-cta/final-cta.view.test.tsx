/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the FinalCTA view
 * @copyright &copy; 2018 R. Matt McCann
 */

import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';

import FinalCtaView from 'features/final-cta/final-cta.view';


describe('features/final-cta/view', () => {
  let component: ShallowWrapper;
  let onClickReachOut: () => void;

  beforeEach(() => {
    onClickReachOut = jest.fn();
    component = shallow(<FinalCtaView onClickReachOut={onClickReachOut} />);
  });

  it('renders as expected', () => {
    expect(component).toMatchSnapshot();
  });

  it('binds the onClickLetsChat handler', () => {
    const button = component.find('Button');

    expect(button.props().onClick).toBe(onClickReachOut);
  });
});
