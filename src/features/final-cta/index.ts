import { ConnectedFinalCtaContainer, FinalCtaContainer } from './final-cta.container';

export {
  ConnectedFinalCtaContainer as FinalCta,
  FinalCtaContainer,
};
