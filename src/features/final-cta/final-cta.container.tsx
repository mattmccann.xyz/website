/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Container for the FinalCTA feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import { push } from 'connected-react-router'
import * as React from 'react';
import { connect } from 'react-redux';

import FinalCtaView from './final-cta.view';


interface IPropTypes {
  push: (route: string) => void;
}

export class FinalCtaContainer extends React.Component<IPropTypes> {
  public render() {
    return (
      <FinalCtaView onClickReachOut={this.onClickReachOut} />
    );
  }

  public onClickReachOut = () => {
    // Navigate to the Let's Chat page
    this.props.push('/lets-chat');
  };
}

/** Maps the actions needed to the component props */
const mapDispatchToProps = {
  /** Used to navigate to other pages */
  push,
};

/** Maps the Redux store to the component props */
const mapStateToProps = undefined;

// Connect the Redux store to the component
export const ConnectedFinalCtaContainer = connect(mapStateToProps, mapDispatchToProps)(FinalCtaContainer);
