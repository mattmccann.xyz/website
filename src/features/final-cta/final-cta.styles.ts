/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Styling for the FinalCTA feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import { CSSProperties } from 'react';

import * as v from 'styles/variables';


const height = 100;
const translateY = 160 * 0.65;

export default {
  button: {
    color: v.COLOR_LIGHT_PRIMARY,
    fontFamily: 'Quicksand, sans-serif',
    fontSize: '1.25em',
    fontWeight: 800,
    left: '50%',
    position: 'absolute',
    top: '50%',
    transform: 'translate(-50%, -50%)',
    width: '10em',
  } as CSSProperties,

  containerInner: {
    backgroundColor: v.COLOR_DARK_SECONDARY,
    borderRadius: '12px',
    boxShadow: '0 5px 5px 0 rgba(0,0,0,0.2),0 0 0 1px #293347',
    color: v.COLOR_LIGHT_PRIMARY,
    paddingBottom: '30px',
    paddingLeft: '1em',
    paddingRight: '1em',
    paddingTop: '30px',
  } as CSSProperties,

  containerOuter: {
    marginLeft: 'auto',
    marginRight: 'auto',
    maxWidth: '70em',
    paddingLeft: '1em',
    paddingRight: '1em',
    transform: `translateY(${translateY}px)`, // Straddle the CTA over the top edge of the footer
  } as CSSProperties,

  ctaText: {
    fontFamily: 'Quicksand, sans-serif',
    left: '50%',
    lineHeight: '2em',
    position: 'absolute',
    top: '50%',
    transform: 'translate(-50%, -50%)',
    width: '16em',
  } as CSSProperties,

  tagLine: {
    fontFamily: '"Permanent Marker", sans-serif',
    fontSize: '2.5em',
    fontWeight: 800,
    left: '50%',
    position: 'absolute',
    top: '50%',
    transform: 'translate(-50%, -50%)',
  } as CSSProperties,

  verticalCenter: {
    height: `${height}px`,
    position: 'relative',
    width: '100%',
  } as CSSProperties,
};
