/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief View for the FinalCTA feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as React from 'react';

import { Button } from 'components/button';

import styles from './final-cta.styles';

/** Properties for the view */
interface IPropTypes {
  /** The callback handler triggered when the user clicks the Reach Out button */
  onClickReachOut: () => void;
}

export default (props: IPropTypes) => (
  <div style={styles.containerOuter}>
    <div style={styles.containerInner}>
      <div className="row">
        <div className="col-12 col-sm-6 col-md-4 text-center" style={styles.verticalCenter}>
          <div style={styles.tagLine}>
            Code Solved
          </div>
        </div>

        <div className="col-12 col-sm-6 col-md-4 text-center" style={styles.verticalCenter}>
          <div style={styles.ctaText}>
            Interested in working together?
            Let's chat - the first beer is on me.
          </div>
        </div>

        <div className="col-12 col-md-4" style={styles.verticalCenter}>
          <Button onClick={props.onClickReachOut} style={styles.button}>
            Reach Out
          </Button>
        </div>
      </div>
    </div>
  </div>
);
