/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the Footer feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';

import { Footer } from 'features/footer';


describe('features/footer', () => {
  let component: ShallowWrapper;

  beforeEach(() => {
    component = shallow(<Footer isOverlayedMode={false} />);
  });

  describe('is overlayed mode', () => {
    beforeEach(() => {
      component.setProps({ isOverlayedMode: true });
    });

    it('renders as expected', () => {
      expect(component).toMatchSnapshot();
    });
  });

  describe('is not overlayed mode', () => {
    it('renders as expected', () => {
      expect(component).toMatchSnapshot();
    });
  });
});
