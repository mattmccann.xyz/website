/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Styles for the Footer feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import { CSSProperties } from "react";

import * as v from 'styles/variables';


export default {
  containerOuter: {
    backgroundColor: v.COLOR_DARK_PRIMARY,
    textAlign: 'center',
    width: '100%',
  } as CSSProperties,

  containerInner: (isOverlayedMode?: boolean) => ({
    color: v.COLOR_LIGHT_SECONDARY,
    fontFamily: 'Quicksand, sans-serif',
    marginLeft: 'auto',
    marginRight: 'auto',
    maxWidth: '35em',
    paddingBottom: '3em',
    paddingTop: isOverlayedMode ? '14em' : '5em',
    textAlign: 'center',
  } as CSSProperties),

  copyright: {
    lineHeight: '2em',
  } as CSSProperties,

  dot: {
    display: 'inline-block',
    marginLeft: 'auto',
    marginRight: 'auto',
  } as CSSProperties,

  links: {
    marginBottom: '3em',
    marginTop: '3em',
  } as CSSProperties,

  tagLine: {
    fontSize: '1.5em',
    fontWeight: 400,
    lineHeight: '1.5em',
    marginTop: '1.5em',
  } as CSSProperties,
}
