/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief View for the Footer feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as React from 'react';

import { DotLink } from 'components/dot-link';
import { VisualAnchor } from 'components/visual-anchor';

import styles from './footer.styles';


interface IFooterProps {
  isOverlayedMode?: boolean;
}

export default (props: IFooterProps) => (
  <div style={styles.containerOuter}>
    <div style={styles.containerInner(props.isOverlayedMode)}>
      <div className="row">
        <div className="col-12">
          <VisualAnchor light />
        </div>
      </div>

      <div className="row">
        <div className="col-12">
          <h3 style={styles.tagLine}>
            Striving for awesome code<br />
            one day at a time.
          </h3>
        </div>
      </div>

      <div className="row" style={styles.links}>
        <div className="col-2">
          <DotLink href="https://www.linkedin.com/in/r-matt-mccann-0494b512" icon="fab fa-linkedin" style={styles.dot} />
        </div>

        <div className="col-2">
          <DotLink href="https://medium.com/@mccann.matt" icon="fab fa-medium" style={styles.dot} />
        </div>

        <div className="col-2">
          <DotLink href="https://gitlab.com/mccann.matt" icon="fab fa-gitlab" style={styles.dot} />
        </div>

        <div className="col-2">
          <DotLink href="https://www.instagram.com/the.data_chef/" icon="fab fa-instagram" style={styles.dot} />
        </div>

        <div className="col-2">
          <DotLink href="https://github.com/mcmonster" icon="fab fa-github" style={styles.dot} />
        </div>

        <div className="col-2">
          <DotLink href="mailto:me@mattmccann.xyz" icon="far fa-envelope" style={styles.dot} />
        </div>
      </div>

      <div className="row">
        <div className="col-12">
          <p style={styles.copyright}>
            Built with love in Columbus, OH<br /> R. Matt McCann &copy; 2018
          </p>
        </div>
      </div>
    </div>
  </div>
);

