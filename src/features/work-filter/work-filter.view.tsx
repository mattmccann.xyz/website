/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief View for the WorkFilter feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import Radium from "radium";
import * as React from "react";

import SkillTag from 'components/skill-tag';

import styles from './work-filter.styles';


export interface IWorkFilterProps {
  /** Callback handler triggered when the user changes the tag search input */
  onChangeTagSearch: (tagFragment: string) => void;

  /** Callback handler triggered onClick for the tag box */
  onClickTagBox: () => void;

  /** True if the tag box is revealed */
  isTagBoxRevealed: boolean;

  /** Callback handler triggered onClick for a suggestion */
  onClickSuggestion: (suggestion: string) => void;

  /** Callback handler triggered onMouseEnter for the prompt */
  onMouseEnterPrompt: () => void;

  /** Callback handler triggered onMouseExit for the tag box */
  onMouseLeaveTagBox: () => void;

  /** The list of suggested matching tags */
  suggestions: string[];

  /** The current input value for the tag search */
  tagSearch: string;

  /** The currently selected tags */
  tags: string[];
}

export class WorkFilterView extends React.Component<IWorkFilterProps> {
  public render() {
    return (
      <div>
        { this.renderPrompt() }
        { this.renderTagBox() }
      </div>
    );
  }

  /** Wrapper that handles the view event unpacking */
  private onChangeTagSearch = (event: React.FormEvent<HTMLInputElement>) => {
    const tagFragment = event.currentTarget.value;

    this.props.onChangeTagSearch(tagFragment);
  };

  /** Wrapper that handles the view event unpacking */
  private onClickSuggestion = (event: React.FormEvent<HTMLDivElement>) => {
    const suggestion = event.currentTarget.innerHTML;

    this.props.onClickSuggestion(suggestion);
  };

  /** Renders an individual suggestion */
  private renderSuggestion(suggestion: string) {
    const { onClickSuggestion } = this;

    return (
      <div id={suggestion} key={suggestion} onClick={onClickSuggestion} style={styles.tagBox.suggestions.entry}>
        {suggestion}
      </div>
    );
  }

  /** Optionally renders the suggestions list */
  private renderSuggestions() {
    const { suggestions } = this.props;

    if (suggestions.length) {
      return (
        <div style={styles.tagBox.suggestions.container}>
          { suggestions.map(suggestion => (this.renderSuggestion(suggestion)))}
        </div>
      )
    } else {
      return undefined;
    }
  }

  /** Optionally renders the tag box */
  private renderTagBox() {
    const { onChangeTagSearch } = this;
    const {isTagBoxRevealed, onClickTagBox, onMouseLeaveTagBox, tagSearch } = this.props;

    if (isTagBoxRevealed) {
      return (
        <div id="tagBox" style={styles.tagBox.container} onClick={onClickTagBox} onMouseLeave={onMouseLeaveTagBox}>
          <div className="row">
            <div className="col-12">
              <input id="suggestionInput"
                style={styles.tagBox.input} type="text" placeholder="&nbsp;&nbsp;Enter skill, language, technology..."
                value={tagSearch} onChange={onChangeTagSearch}
              />
              {this.renderSuggestions()}
            </div>
          </div>
          <div className="row" style={styles.tagBox.tags}>
            <div className="col-12">
              {this.renderTags()}
            </div>
          </div>
        </div>
      );
    } else {
      return undefined;
    }
  }

  /** Renders the selected tags */
  private renderTags() {
    return this.props.tags.map(tag => <SkillTag key={tag} skill={tag} />);
  }

  /** Optionally renders the "Interested?" prompt */
  private renderPrompt() {
    const {isTagBoxRevealed, onMouseEnterPrompt} = this.props;

    if (isTagBoxRevealed) {
      return undefined;
    } else {
      return (
        <div id="prompt" style={{...styles.prompt, ...styles.promptHover}} onMouseEnter={onMouseEnterPrompt}>
          Interested in a particular skill or technology?
        </div>
      );
    }
  }
}

export default Radium(WorkFilterView);
