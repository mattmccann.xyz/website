/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the WorkFilter container
 * @copyright &copy; 2018 R. Matt McCann
 */

import { ShallowWrapper } from 'enzyme';
import * as React from 'react';

import WorkFilter from 'features/work-filter';
import shallowWithStore from 'mocks/shallow-with-store';


describe('features/work-filter/container', () => {
  let component: ShallowWrapper;

  beforeEach(() => {
    component = shallowWithStore(<WorkFilter />, {
      features: {
        workFilter: {
          derp: 'derp',
        }
      },
      resources: {
        tags: {
          value: ['tag1', 'tag2'],
        },
      },
    });
  });

  it('renders as expected', () => {
    expect(component).toMatchSnapshot();
  });
});
