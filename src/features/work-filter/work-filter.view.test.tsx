/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the ProjectCard component
 * @copyright &copy; 2018 R. Matt McCann
 */

import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';

import { WorkFilterView } from './work-filter.view';


describe('features/work-filter/view', () => {
  let component: ShallowWrapper;
  let isTagBoxRevealed: boolean;
  let onChangeTagSearch: (tagFragment: string) => void;
  let onClickTagBox: () => void;
  let onClickSuggestion: (suggestion: string) => void;
  let onMouseEnterPrompt: () => void;
  let onMouseLeaveTagBox: () => void;
  let suggestions: string[];
  let tags: string[];
  let tagSearch: string;

  beforeEach(() => {
    isTagBoxRevealed = false;
    onChangeTagSearch = jest.fn();
    onClickTagBox = jest.fn();
    onClickSuggestion = jest.fn();
    onMouseEnterPrompt = jest.fn();
    onMouseLeaveTagBox = jest.fn();
    suggestions = [];
    tagSearch = '';
    tags = [];

    component = shallow(<WorkFilterView
      onChangeTagSearch={onChangeTagSearch} onClickTagBox={onClickTagBox} isTagBoxRevealed={isTagBoxRevealed}
      onClickSuggestion={onClickSuggestion} onMouseEnterPrompt={onMouseEnterPrompt}
      onMouseLeaveTagBox={onMouseLeaveTagBox} suggestions={suggestions} tagSearch={tagSearch} tags={tags}
    />);
  });

  describe('prompt is revealed', () => {
    it('renders the prompt as expected', () => {
      expect(component).toMatchSnapshot();
    });

    it('dispatches the mouse enter handler on mouse enter', () => {
      component.find('[id="prompt"]').simulate('mouseEnter');

      expect(onMouseEnterPrompt).toHaveBeenCalled();
    });
  });

  describe('tag box is revealed', () => {
    beforeEach(() => {
      component.setProps({ isTagBoxRevealed: true });
    });

    it('renders the tag box as expected', () => {
      expect(component).toMatchSnapshot();
    });

    it('dispatches the click handler when clicked', () => {
      component.find('[id="tagBox"]').simulate('click');

      expect(onClickTagBox).toHaveBeenCalled();
    });

    it('dispatches the mouse leave handler when mouse leaves', () => {
      component.find('[id="tagBox"]').simulate('mouseLeave');

      expect(onMouseLeaveTagBox).toHaveBeenCalled();
    });

    it('dispatches the change tag search handler on change', () => {
      const input = component.find('[id="suggestionInput"]');

      input.simulate('change', {
        currentTarget: {
          value: 'Rea',
        },
      });

      expect(onChangeTagSearch).toHaveBeenCalledWith('Rea');
    });

    it('populates the tag search input with the tag search value', () => {
      component.setProps({ tagSearch: 'Rea' });
      const input = component.find('[id="suggestionInput"]');

      expect(input.props().value).toEqual('Rea');
    });

    describe('renders suggestions', () => {
      it('gracefully renders 0 suggestions', () => {
        expect(component).toMatchSnapshot();
      });

      it('renders multiple suggestions', () => {
        component.setProps({ suggestions: ['reactjs', 'react-native']});

        expect(component).toMatchSnapshot();
      });

      it('dispatches the click suggestion handler when suggestion is clicked', () => {
        component.setProps({ suggestions: ['reactjs', 'react-native']});
        component.find('[id="reactjs"]').simulate('click', {
          currentTarget: {
            innerHTML: 'reactjs',
          },
        });

        expect(onClickSuggestion).toHaveBeenCalledWith('reactjs');
      });
    });

    describe('render tags', () => {
      it('renders 0 suggestions', () => {
        expect(component).toMatchSnapshot();
      });

      it('renders multiple suggestions', () => {
        component.setProps({ tags: ['reactjs', 'react-native'] });

        expect(component).toMatchSnapshot();
      });
    });
  });
});
