/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Redux reducer for the WorkFilter feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import { IAction } from 'state/action.type';

import * as c from './work-filter.constants';


export interface IWorkFilterState {
  /** State of the fetch suggestions network request */
  fetchSuggestions: {
    error?: Error,
    isComplete: boolean,
    isError: boolean,
    isInProgress: boolean,
  }

  /** True when the user has clicked the tag box at least once */
  hasClickedTagBox: boolean;

  /** True when the mouse is over the feature */
  isMouseEntered: boolean;

  /** True when the tag box is revealed */
  isTagBoxRevealed: boolean;

  /** List of possibly matching tags */
  suggestions: string[];

  /** The input value for the tag search */
  tagSearch: string;
}

/** Initial state for the feature */
export const initialState = (): IWorkFilterState => ({
  fetchSuggestions: {
    isComplete: false,
    isError: false,
    isInProgress: false,
  },
  hasClickedTagBox: false,
  isMouseEntered: false,
  isTagBoxRevealed: false,
  suggestions: [],
  tagSearch: '',
});

/**
 * Reducer for the feature state.
 *
 * @param state The state we are updating
 * @param action The action we are reducing
 * @returns The updated state
 */
export const reducer = (state = initialState(), action: IAction): IWorkFilterState => {
  switch (action.type) {
    case c.CHANGED_TAG_SEARCH:
      return {
        ...state,
        // If the tag fagment is empty, clear the suggestions list
        suggestions: action.payload ? state.suggestions : [],
        tagSearch: action.payload,
      };

    case c.CLICKED_SUGGESTION:
      return {
        ...state,
        suggestions: [],
        tagSearch: '',
      };

    case c.CLICKED_TAG_BOX:
      return {
        ...state,
        hasClickedTagBox: true,
        isTagBoxRevealed: true,
      };

    case c.FETCH_SUGGESTIONS_COMPLETED:
      return {
        ...state,
        fetchSuggestions: {
          isComplete: true,
          isError: false,
          isInProgress: false,
        },
        suggestions: [...action.payload],
      };

    case c.FETCH_SUGGESTIONS_FAILED:
      return {
        ...state,
        fetchSuggestions: {
          error: action.payload,
          isComplete: true,
          isError: true,
          isInProgress: false,
        },
      };

    case c.FETCH_SUGGESTIONS_STARTED:
      return {
        ...state,
        fetchSuggestions: {
          isComplete: false,
          isError: false,
          isInProgress: true,
        },
      };

    case c.MOUSE_ENTERED_PROMPT:
      return {
        ...state,
        isMouseEntered: true,
        isTagBoxRevealed: true,
      };

    case c.MOUSE_LEFT_TAG_BOX:
      return {
        ...state,
        isMouseEntered: false,
        isTagBoxRevealed: state.hasClickedTagBox,
      };

    default:
      return state;
  }
};

