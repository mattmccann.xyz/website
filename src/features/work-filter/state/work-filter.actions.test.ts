/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the WorkFilter feature state actions
 * @copyright &copy; 2018 R. Matt McCann
 */

import mockDispatch from 'mocks/mock-dispatch';
import mockServices from 'mocks/mock-services';
import * as tagConstants from 'state/resources/tags/tags.constants';
import { Dispatch, IServices } from 'state/thunk.type';

import * as actions from './work-filter.actions';
import * as c from './work-filter.constants';


describe('features/work-filter/state/actions', () => {
  let dispatch: Dispatch;
  let getState: any;
  let services: IServices;

  beforeEach(() => {
    getState = jest.fn();
    services = mockServices();
    dispatch = mockDispatch(getState, services);
  });

  describe('onChangeTagSearch', () => {
    let expectedSuggestions: string[];

    beforeEach(() => {
      expectedSuggestions = ['tag1', 'tag2'];

      const suggestMock: any = services.api.tags().suggest;
      suggestMock.mockReturnValue(Promise.resolve(expectedSuggestions));
    });

    it('dispatches the tag search change action', async () => {
      const actionPromise = dispatch(actions.onChangeTagSearch('C+'));

      expect(dispatch).toHaveBeenCalledWith({ type: c.CHANGED_TAG_SEARCH, payload: 'C+' });

      await actionPromise;
    });

    describe('on empty tag fragment', () => {
      it('does not dispatch the fetch suggestions action', async () => {
        await dispatch(actions.onChangeTagSearch(''));

        expect(dispatch).not.toHaveBeenCalledWith({ type: c.FETCH_SUGGESTIONS_STARTED });
      });
    });

    describe('on non-empty tag fragment', () => {
      it('immediately dispatches the fetch suggestions started action', async () => {
        const actionPromise = dispatch(actions.onChangeTagSearch('C+'));

        expect(dispatch).toHaveBeenCalledWith({ type: c.FETCH_SUGGESTIONS_STARTED });

        await actionPromise;
      });

      it('calls the suggest api end point', async () => {
        await dispatch(actions.onChangeTagSearch('C+'));

        expect(services.api.tags().suggest).toHaveBeenCalledWith('C+');
      });

      describe('on suggest api call success', async () => {
        it('dispatches the fetch suggestions completed action', async () => {
          const suggestions = await dispatch(actions.onChangeTagSearch('C+'));

          expect(dispatch).toHaveBeenCalledWith({
            payload: expectedSuggestions, type: c.FETCH_SUGGESTIONS_COMPLETED,
          });
          expect(suggestions).toEqual(expectedSuggestions);
        });
      });

      describe('on suggest api call failure', () => {
        let error: Error;

        beforeEach(() => {
          error = new Error('oh no!');

          const suggestMock: any = services.api.tags().suggest;
          suggestMock.mockReturnValue(Promise.reject(error));
        });

        it('dispatches the fetch suggestions failed action', async () => {
          try {
            await dispatch(actions.onChangeTagSearch('C+'));

            expect(false).toBe(true); // Should have propagated error
          } catch (caughtError) {
            expect(caughtError).toEqual(error);

            expect(dispatch).toHaveBeenCalledWith({type: c.FETCH_SUGGESTIONS_FAILED, payload: error});
          }
        });
      });
    });
  });

  describe('onClickSuggestion', () => {
    it('dispatches the clicked suggestion action', () => {
      dispatch(actions.onClickSuggestion('C+'));

      expect(dispatch).toHaveBeenCalledWith({ type: c.CLICKED_SUGGESTION });
    });

    it('dispatches the add tag action', () => {
      dispatch(actions.onClickSuggestion('C+'));

      expect(dispatch).toHaveBeenCalledWith({ type: tagConstants.ADD, payload: 'C+' });
    });
  });

  it('onClickTagBox', () => {
    dispatch(actions.onClickTagBox());

    expect(dispatch).toHaveBeenCalledWith({ type: c.CLICKED_TAG_BOX });
  });

  it('onMouseEnterPrompt', () => {
    dispatch(actions.onMouseEnterPrompt());

    expect(dispatch).toHaveBeenCalledWith({ type: c.MOUSE_ENTERED_PROMPT });
  });

  it('onMouseLeaveTagBox', () => {
    dispatch(actions.onMouseLeaveTagBox());

    expect(dispatch).toHaveBeenCalledWith({ type: c.MOUSE_LEFT_TAG_BOX });
  });
});
