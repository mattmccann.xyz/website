/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Redux actions for the WorkFilter feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import { IAction } from 'state/action.type';
import { add as addTag } from 'state/resources/tags/tags.actions';
import { Dispatch, GetState, IServices } from 'state/thunk.type';

import * as c from './work-filter.constants';


const changeTagSearchInput = (tagFragment: string): IAction => ({
  payload: tagFragment, type: c.CHANGED_TAG_SEARCH,
});

const fetchSuggestionsCompleted = (suggestions: string[]): IAction => ({
  payload: suggestions, type: c.FETCH_SUGGESTIONS_COMPLETED,
});

const fetchSuggestionsFailed = (error: Error): IAction => ({
  payload: error, type: c.FETCH_SUGGESTIONS_FAILED,
});

const fetchSuggestionsStarted = () : IAction => ({ type: c.FETCH_SUGGESTIONS_STARTED });

const fetchSuggestions = (tagFragment: string) =>
  async (dispatch: Dispatch, getState: GetState, services: IServices): Promise<string[]> => {
    // Immediately announce that we are starting to fetch the suggestion list
    dispatch(fetchSuggestionsStarted());

    try {
      // Call the suggestions api endpoint
      const suggestions = await services.api.tags().suggest(tagFragment);

      // Announce the completed fetch
      dispatch(fetchSuggestionsCompleted(suggestions));

      return suggestions;
    } catch (error) {
      // Announce the error
      dispatch(fetchSuggestionsFailed(error));

      // Propagate the error
      throw error;
    }
  };

/** Action creator triggered when the user changes the tag search input. Fetches a suggestion list of matching tags */
export const onChangeTagSearch = (tagFragment: string) => (dispatch: Dispatch) => {
  // Dispatch the change to the tag search input
  dispatch(changeTagSearchInput(tagFragment));

  // Fetch the suggestions for this tag fragment if the tag fragment isn't empy
  if (tagFragment) {
    return dispatch(fetchSuggestions(tagFragment));
  }
};

/** Action creator triggered when the user clicks a tag suggestion */
export const onClickSuggestion = (suggestion: string) => (dispatch: Dispatch) => {
  // Dispatch the clicked suggestion action to clear the input and suggestions
  dispatch({ type: c.CLICKED_SUGGESTION });

  // Add the clicked suggestion as a tag
  dispatch(addTag(suggestion));
};

/** Action creator triggered when the user clicks the tag box */
export const onClickTagBox = (): IAction => ({ type: c.CLICKED_TAG_BOX });

/** Action creator triggered when the mouse enters the prompt */
export const onMouseEnterPrompt = (): IAction => ({ type: c.MOUSE_ENTERED_PROMPT });

/** Action creator triggered when the mouse exits the tag box */
export const onMouseLeaveTagBox = (): IAction => ({ type: c.MOUSE_LEFT_TAG_BOX });
