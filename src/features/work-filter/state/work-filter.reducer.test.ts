/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the WorkFilter feature's state reducer
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as actions from './work-filter.actions';
import * as c from './work-filter.constants';
import { initialState, reducer } from './work-filter.reducer';


describe('features/work-filter/state/reducer', () => {
  it('initializes the state', () => {
    const expectedState = initialState();
    const newState = reducer(undefined, { type: 'NOT_HANDLED' });

    expect(newState).toEqual(expectedState);
  });

  describe('reduces CHANGED_TAG_SEARCH', () => {
    it('clears suggestions when tag fragment is empty', () => {
      const expectedState = initialState();
      const oldState = Object.assign(initialState(), { suggestions: ['tag1'], tagSearch: 'ta' });

      const newState = reducer(oldState, { type: c.CHANGED_TAG_SEARCH, payload: '' });

      expect(newState).toEqual(expectedState);
    });

    it('does not clear suggestions when tag fragment is not empty', () => {
      const expectedState = Object.assign(initialState(), { suggestions: ['tag1'], tagSearch: 't' });
      const oldState = Object.assign(initialState(), { suggestions: ['tag1'], tagSearch: 'ta' });

      const newState = reducer(oldState, { type: c.CHANGED_TAG_SEARCH, payload: 't' });

      expect(newState).toEqual(expectedState);
    });

  });

  it('reduces CLICKED_SUGGESTION', () => {
    const expectedState = initialState();
    const oldState = Object.assign(initialState(), { suggestions: ['tag1'], tagSearch: 'ta' });

    const newState = reducer(oldState, { type: c.CLICKED_SUGGESTION });

    expect(newState).toEqual(expectedState);
  });

  it('reduces CLICKED_TAG_BOX', () => {
    const expectedState = Object.assign(initialState(), { hasClickedTagBox: true, isTagBoxRevealed: true });

    const newState = reducer(undefined, actions.onClickTagBox());

    expect(newState).toEqual(expectedState);
  });

  it('reduces FETCH_SUGGESTIONS_COMPLETED', () => {
    const suggestions = ['tag1', 'tag2'];
    const expectedState = Object.assign(initialState(), {
      fetchSuggestions: {
        isComplete: true,
        isError: false,
        isInProgress: false,
      },
      suggestions,
    });

    const newState = reducer(undefined, { type: c.FETCH_SUGGESTIONS_COMPLETED, payload: suggestions });

    expect(newState).toEqual(expectedState);
  });

  it('reduces FETCH_SUGGESTIONS_FAILED', () => {
    const error = new Error('oh no!');
    const expectedState = Object.assign(initialState(), {
      fetchSuggestions: {
        error,
        isComplete: true,
        isError: true,
        isInProgress: false,
      },
    });

    const newState = reducer(undefined, { type: c.FETCH_SUGGESTIONS_FAILED, payload: error });

    expect(newState).toEqual(expectedState);
  });

  it('reduces FETCH_SUGGESTIONS_STARTED', () => {
    const expectedState = Object.assign(initialState(), {
      fetchSuggestions: {
        isComplete: false,
        isError: false,
        isInProgress: true,
      },
    });

    const newState = reducer(undefined, { type: c.FETCH_SUGGESTIONS_STARTED });

    expect(newState).toEqual(expectedState);
  });

  it('reduces MOUSE_ENTERED_PROMPT', () => {
    const expectedState = Object.assign(initialState(), { isMouseEntered: true, isTagBoxRevealed: true });

    const newState = reducer(undefined, actions.onMouseEnterPrompt());

    expect(newState).toEqual(expectedState);
  });

  describe('reduces MOUSE_LEFT_TAG_BOX', () => {
    it('hides the tag box when no clicked tag box', () => {
      const expectedState = initialState();
      const oldState = Object.assign(initialState(), { isMouseEntered: true });

      const newState = reducer(oldState, actions.onMouseLeaveTagBox());

      expect(newState).toEqual(expectedState);
    });

    it('keeps the tag box revealed when clicked', () => {
      const expectedState = Object.assign(initialState(), { hasClickedTagBox: true, isTagBoxRevealed: true });
      const oldState = Object.assign(initialState(), { hasClickedTagBox: true, isMouseEntered: true });

      const newState = reducer(oldState, actions.onMouseLeaveTagBox());

      expect(newState).toEqual(expectedState);
    });
  });

  it('passes along state for unmatched action type', () => {
    const oldState = Object.assign(initialState(), { isTagBoxRevealed: true });

    const newState = reducer(oldState, { type: 'NOT_HANDLED' });

    expect(newState).toEqual(oldState);
  });
});
