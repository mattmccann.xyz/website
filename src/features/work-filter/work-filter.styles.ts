/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Styles for the WorkFilter feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import Radium from "radium";
import { CSSProperties } from "react";
import { fadeOut } from "react-animations";


export default {
  prompt: {
    animationDuration: '0.2s',
    animationName: Radium.keyframes(fadeOut, 'fadeOut'),
    animationTimingFunction: 'linear',
  } as CSSProperties,

  promptHover: {
    ':hover': {
      cursor: 'pointer',
    },
  } as CSSProperties,

  tagBox: {
    container: {
      marginLeft: 'auto',
      marginRight: 'auto',
      width: '30em',
    } as CSSProperties,

    input: {
      width: '20em',
    } as CSSProperties,

    suggestions: {
      container: {
        border: '1px solid black',
        marginLeft: 'auto',
        marginRight: 'auto',
        textAlign: 'left',
        width: '20em',
      } as CSSProperties,

      entry: {
        ':hover': {
          backgroundColor: 'green',
          color: 'white',
        },
        paddingLeft: '1em',
      } as CSSProperties,
    },

    tags: {
      height: '2em',
      paddingTop: '1em',
    } as CSSProperties,
  },
}
