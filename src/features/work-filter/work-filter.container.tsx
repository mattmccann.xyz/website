/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Container for the WorkFilter feature
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as React from 'react';
import { connect } from 'react-redux';

import { IState } from 'state/state.type';

import * as actions from './state/work-filter.actions';
import WorkFilterView, { IWorkFilterProps } from './work-filter.view';


const WorkFilterContainer = (props: IWorkFilterProps) => (
  <WorkFilterView {...props} />
);

/** Maps the actions needed to the component props */
const mapDispatchToProps = {
  ...actions,
};

/** Maps the redux store to the component props */
const mapStateToProps = (state: IState) => ({
  ...state.features.workFilter,
  tags: [...state.resources.tags.value],
});

// Connect the Redux store to the component
export default connect(mapStateToProps, mapDispatchToProps)(WorkFilterContainer);
