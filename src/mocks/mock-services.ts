/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Mock implementation of the services injected into Thunks
 * @copyright &copy; 2018 R. Matt McCann
 */

import {IReactDOM, IScroller, IServices} from 'state/thunk.type';


const mockApi = () => {
  const googleAnalytics = {
    list: jest.fn(),
  };
  const messages = {
    create: jest.fn(),
  };
  const projects = {
    list: jest.fn(),
  };
  const tags = {
    suggest: jest.fn(),
  };
  const testimonials = {
    list: jest.fn(),
  };

  return {
    googleAnalytics: () => googleAnalytics,
    messages: () => messages,
    projects: () => projects,
    tags: () => tags,
    testimonials: () => testimonials,
  };
};

export default (): IServices => ({
  api: mockApi(),
  document: {} as Document,
  reactDOM: {} as IReactDOM,
  scroller: {} as IScroller,
  window: {} as Window,
});
