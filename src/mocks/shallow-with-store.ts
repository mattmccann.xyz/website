/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Helper function that bootstraps the shallow render with a store
 * @copyright &copy; 2018 R. Matt McCann
 */

import { shallow } from 'enzyme';
import * as React from 'react';
import { createMockStore } from 'redux-test-utils';


export default <T>(element: React.ReactElement<T>, state: object) => { // TODO: Add state type
  const context = {
    store: createMockStore(state),
  };

  return shallow<T>(element, { context }).dive();
}
