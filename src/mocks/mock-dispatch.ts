/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Mock implementation of the Redux dispatch function
 * @copyright &copy; 2018 R. Matt McCann
 */

import { GetState, IServices } from 'state/thunk.type';


export default (getState: GetState, services: IServices) => {
  const dispatch = jest.fn();

  dispatch.mockImplementation(async (arg) => {
    if (typeof arg === 'function') {
      return arg(dispatch, getState, services);
    }

    return arg;
  });

  return dispatch;
}

