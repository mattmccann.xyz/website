/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Mock implementation of axios
 * @copyright &copy; 2018 R. Matt McCann
 */

export default () => ({
  get: jest.fn(),
});
