/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Mock implementation of firestore
 * @copyright &copy; 2018 R. Matt McCann
 */

export default class MockFirestore {
  public added: any = [];
  public data: any = {};

  public collection = (collectionName: string) => {
    const rawData = this.data[collectionName];

    const theCollection: any = {
      add: async (record: any) => {
        this.added.push(record);
      },
      get: async () => ({
        forEach: (cb: (record: any) => void) => {
          rawData.forEach((rawDatum: any, id: number) => {
            cb({ id: String(id), data: () => rawDatum });
          });
        },
      }),
      orderBy: jest.fn(),
      where: jest.fn(),
    };
    theCollection.orderBy.mockReturnValue(theCollection);
    theCollection.where.mockReturnValue(theCollection);

    return theCollection;
  };
};
