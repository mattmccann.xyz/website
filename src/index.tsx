import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { App } from 'app';

import './index.css';
import registerServiceWorker from './registerServiceWorker';


const rootElement = document.getElementById("root");
if (rootElement != null && rootElement.hasChildNodes()) {
  ReactDOM.hydrate(<App />, rootElement);
} else {
  ReactDOM.render(<App />, rootElement);
}
registerServiceWorker();
