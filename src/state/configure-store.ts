/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Configures the Redux store for the app
 */

import axios from 'axios';
import { connectRouter, routerMiddleware } from 'connected-react-router'
import * as firebase from 'firebase/app';
import 'firebase/firestore'; // Required for side effects
import { createBrowserHistory } from 'history';
import * as reactDOM from 'react-dom';
import * as ReactGA from 'react-ga';
import { scroller } from 'react-scroll';
import { applyMiddleware, createStore } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';

import api from 'services/api';
import GoogleAnalytics from 'services/google-analytics';
import gaMiddleware from 'state/google-analytics';

import rootReducer from './root-reducer';


// The history object we are going to bind to the store
export const history = createBrowserHistory();

// Let's have the logger also give us state diffs for ease of reading
const logger = createLogger({
  diff: true,
});

// Initialize Axios
const axiosInstance = axios.create({
  baseURL: 'https://us-central1-mattmccann-xyz.cloudfunctions.net/api',
});

// Initialize Firebase
firebase.initializeApp({
  apiKey: "AIzaSyACNEQw2Neg2Y7Rz80SducYfPhtVfYgU9Y",
  authDomain: "mattmccann-xyz.firebaseapp.com",
  databaseURL: "https://mattmccann-xyz.firebaseio.com",
  messagingSenderId: "62837645469",
  projectId: "mattmccann-xyz",
  storageBucket: "mattmccann-xyz.appspot.com",
});

// Initialize Firestore
const db = firebase.firestore();
db.settings({
  timestampsInSnapshots: true
});

// Initialize the google analytics adapter
const googleAnalytics = new GoogleAnalytics(ReactGA);

// This services map allows us to inject services into our thunks for testing convenience
const services = {
  api: api(axiosInstance, db),
  document,
  reactDOM,
  scroller,
  window,
};

export const configureStore = (initialState = {}) => createStore(
  connectRouter(history)(rootReducer),
  initialState,
  applyMiddleware(
    routerMiddleware(history), // for dispatching history actions
    thunk.withExtraArgument(services),
    gaMiddleware(services.api, googleAnalytics),
    logger, // Logs the state changes in the store
  ),
);
