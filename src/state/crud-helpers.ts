/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Redux action and reducer helpers for standard CRUD operations
 * @copyright &copy; 2018 R. Matt McCann
 */

import { IAction } from './action.type';
import { Dispatch, GetState, IServices } from './thunk.type';


/**
 * Action creator that builds the list completed action.
 *
 * @template T The type of the resource being listed
 * @param type The type string to use for this action.
 * @param listedResource The resources that were listed
 * @returns The action
 */
function listCompleted<T>(type: string, listedResource: T[]) {
  return { payload: listedResource, type };
}

/**
 * Action creator that builds the list failed action.
 *
 * @param type The type string to use for this action.
 * @param error The error triggering the failure
 * @returns The action
 */
const listFailed = (type: string, error: any) => ({ payload: error, type });

/**
 * Action creator that builds the list started action.
 *
 * @param type The type string to use for this action
 * @returns The action
 */
const listStarted = (type: string) => ({ type });

/**
 * Generic thunk implementation of a list api call.
 *
 * @template T The type of the resource being listed
 * @param listApi The list API function to call inside the thunk
 * @param startedType The type string for the list started action
 * @param failedType The type string for the list failed action
 * @param completedType The type string for the list completed action
 * @returns The generic list thunk
 */
export function list<T>(
  listApi: (services: IServices) => Promise<T[]>, startedType: string, failedType: string, completedType: string
) {
  return () => async (dispatch: Dispatch, getState: GetState, services: IServices): Promise<T[]> => {
    // Immediately announce that we have started the request
    dispatch(listStarted(startedType));

    try {
      const resources: T[] = await listApi(services);

      // Announce that we have completed the fetch
      dispatch(listCompleted<T>(completedType, resources));

      return resources;
    } catch (error) {
      // Announce that the fetch failed
      dispatch(listFailed(failedType, error));

      // Propagate the error
      throw error;
    }
  }
}

/**
 * The generic state details for the list operation.
 */
export interface IListState<T> {
  /** Status of the list network request */
  list: {
    error?: Error;
    isComplete: boolean;
    isError: boolean;
    isInProgress: boolean;
  },

  /** List of the resource */
  value: T[],
}


/** Initial state for the resource */
export function initialListState<T>(): () => IListState<T> {
  return (): IListState<T> => ({
    list: {
      isComplete: false,
      isError: false,
      isInProgress: false,
    },
    value: [],
  });
}

/**
 * Generic state reducer for the list operation.
 *
 * @template T The resource type
 * @param startedType Type constant for the list started action
 * @param failedType Type constant for the list failed action
 * @param completedType Type constant for the list completed action
 */
export function listReducer<T>(
  startedType: string, failedType: string, completedType: string,
) {
  return (state = initialListState<T>()(), action: IAction): IListState<T> => {
    switch (action.type) {
      case completedType:
        return {
          ...state,
          list: {
            isComplete: true,
            isError: false,
            isInProgress: false,
          },
          value: [...action.payload],
        };

      case failedType:
        return {
          ...state,
          list: {
            error: action.payload,
            isComplete: true,
            isError: true,
            isInProgress: false,
          },
        };

      case startedType:
        return {
          ...state,
          list: {
            isComplete: false,
            isError: false,
            isInProgress: true,
          },
        };

      default:
        return state;
    }
  };
}
