/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief State type definition for the app state
 * @copyright &copy; 2018 R. Matt McCann
 */

import { IFeaturesState } from 'features/features.reducer';

import { IDeviceState } from './device/device.reducer';
import { IResourcesState } from './resources/resources.reducer';


export interface IState {
  device: IDeviceState,
  features: IFeaturesState,
  resources: IResourcesState,
  router: {
    location: {
      pathname: string,
    },
  };
}
