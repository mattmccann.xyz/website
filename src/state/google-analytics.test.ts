/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the google analytics middleware
 */

import mockServices from 'mocks/mock-services';
import { IApi } from 'services/api';

import gaMiddleware from './google-analytics';


describe('state/google-analytics', () => {
  let api: IApi;
  let ga: any;

  beforeEach(() => {
    api = mockServices().api;
    (api.googleAnalytics().list as any).mockReturnValue({
      then: (cb: any) => cb({
        'ACTION_TYPE': {
          action: "clicked-something",
          category: "the-category",
        },
      }),
    });

    ga = {
      sendEvent: jest.fn(),
      sendPageView: jest.fn(),
    }
  });

  describe('middleware construction', () => {
    it('starts the fetch of the google analytics mapping', () => {
      const middleware = gaMiddleware(api, ga);

      expect(api.googleAnalytics().list).toHaveBeenCalled();
      expect(middleware).toBeDefined();
    });
  });

  describe('middleware handling', () => {
    let middleware: any;
    let next: () => void;

    beforeEach(() => {
      middleware = gaMiddleware(api, ga);
      next = jest.fn().mockReturnValue('nextAction!');
    });

    describe('action is thunk', () => {
      it('passes on the action', () => {
        const action = jest.fn().mockReturnValue('derp!');

        const rv = middleware()(next)(action);

        expect(action).toHaveBeenCalled();
        expect(ga.sendEvent).not.toHaveBeenCalled();
        expect(next).toHaveBeenCalledWith('derp!');
        expect(rv).toEqual('nextAction!');
      });
    });

    describe('action is not a thunk', () => {
      describe('mappings not yet fetched', () => {
        beforeEach(() => {
          (api.googleAnalytics().list as any).mockReturnValue({
            then: (cb: any) => cb(undefined),
          });
          middleware = gaMiddleware(api, ga);
        });

        it('does not explode', () => {
          const action = { type: 'derp' };

          const rv = middleware()(next)(action)

          expect(ga.sendEvent).not.toHaveBeenCalled();
          expect(next).toHaveBeenCalledWith(action);
          expect(rv).toEqual('nextAction!');
        });
      });

      describe('mapping is fetched and action has mapping', () => {
        const action = {
          payload: {
            bob: {
              villa: 'this olde home',
            },
          },
          type: 'ACTION_TYPE',
        };

        it('dispatches event without jsonpath', () => {
          const rv = middleware()(next)(action);

          expect(ga.sendEvent).toHaveBeenCalledWith('the-category', 'clicked-something');
          expect(next).toHaveBeenCalledWith(action);
          expect(rv).toEqual('nextAction!');
        });

        it('dispatches event with jsonpath', () => {
          (api.googleAnalytics().list as any).mockReturnValue({
            then: (cb: any) => cb({
              'ACTION_TYPE': {
                action: "$['type']",
                category: "$['payload']['bob']['villa']",
              },
            }),
          });
          middleware = gaMiddleware(api, ga);

          const rv = middleware()(next)(action);

          expect(ga.sendEvent).toHaveBeenCalledWith('this olde home', 'ACTION_TYPE');
          expect(next).toHaveBeenCalledWith(action);
          expect(rv).toEqual('nextAction!');
        });

        it('gracefully dispatches event with unmatched jsonpath', () => {
          (api.googleAnalytics().list as any).mockReturnValue({
            then: (cb: any) => cb({
              'ACTION_TYPE': {
                action: "$['typify']",
                category: "$['payload']['bob']['barker']",
              },
            }),
          });
          middleware = gaMiddleware(api, ga);

          const rv = middleware()(next)(action);

          expect(ga.sendEvent).toHaveBeenCalledWith('nomatch', 'nomatch');
          expect(next).toHaveBeenCalledWith(action);
          expect(rv).toEqual('nextAction!');
        });
      });

      describe('mapping is fetched but action has no mapping', () => {
        it('does not dispatch a ga event', () => {
          const action = {
            type: 'not_match',
          };

          const rv = middleware()(next)(action);

          expect(ga.sendEvent).not.toHaveBeenCalled();
          expect(next).toHaveBeenCalledWith(action);
          expect(rv).toEqual('nextAction!');
        });
      });

      describe('action is location change', () => {
        it('dispatches the ga page view', () => {
          const action = {
            payload: {
              location: {
                pathname: 'page',
              },
            },
            type: '@@router/LOCATION_CHANGE',
          };

          middleware()(next)(action);

          expect(ga.sendPageView).toHaveBeenCalledWith('page');
        });
      });
    });
  });
});
