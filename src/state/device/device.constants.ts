/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Redux contants for the device state
 * @copyright &copy; 2018 R. Matt McCann
 */

export const UPDATE_INNER_WIDTH = 'DEVICE_UPDATE_INNER_WIDTH';
export const UPDATE_PAGE_Y_OFFSET = 'DEVICE_UPDATE_PAGE_Y_OFFSET';
