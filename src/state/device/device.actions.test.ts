/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the Device state actions
 * @copyright &copy; 2018 R. Matt McCann
 */

import mockDispatch from 'mocks/mock-dispatch';
import mockServices from 'mocks/mock-services';
import { Dispatch, GetState, IServices } from 'state/thunk.type';

import * as actions from './device.actions';
import * as c from './device.constants';


describe('state/device/actions', () => {
  let addEventListener: any;
  let dispatch: Dispatch;
  let getState: GetState;
  let removeEventListener: any;
  let services: IServices;

  beforeEach(() => {
    getState = jest.fn();
    services = mockServices();
    dispatch = mockDispatch(getState, services);

    addEventListener = jest.fn();
    removeEventListener = jest.fn();
    services.window = {
      addEventListener,
      innerWidth: 200,
      pageYOffset: 10,
      removeEventListener,
    } as Window;
  });

  describe('listenForResize', () => {
    it('dispatches the initial inner width', () => {
      dispatch(actions.listenForResize());

      expect(dispatch).toHaveBeenCalledWith({ type: c.UPDATE_INNER_WIDTH, payload: 200 });
    });

    it('binds the resize listener', () => {
      dispatch(actions.listenForResize());

      expect(addEventListener.mock.calls[0].length).toBe(2);
      expect(addEventListener.mock.calls[0][0]).toEqual('resize');
    });

    it('listener dispatches the inner width update', () => {
      // Bind the listener
      dispatch(actions.listenForResize());

      // Setup a new inner width
      (services.window as any).innerWidth = 300;

      // Execute the listener
      addEventListener.mock.calls[0][1]();

      expect(dispatch).toHaveBeenCalledWith({ type: c.UPDATE_INNER_WIDTH, payload: 300 });
    });
  });

  describe('listenForScroll', () => {
    it('dispatches the initial page offset', () => {
      dispatch(actions.listenForScroll());

      expect(dispatch).toHaveBeenCalledWith({ type: c.UPDATE_PAGE_Y_OFFSET, payload: 10 });
    });

    it('binds the scroll listener', () => {
      dispatch(actions.listenForScroll());

      expect(addEventListener.mock.calls[0].length).toBe(2);
      expect(addEventListener.mock.calls[0][0]).toEqual('scroll');
    });

    it('debounces the scroll listener', async () => {
       // Bind the listener
      dispatch(actions.listenForScroll());

      // Setup a new inner width
      (services.window as any).pageYOffset = 20;

      // Execute the listener a couple times
      addEventListener.mock.calls[0][1]();
      addEventListener.mock.calls[0][1]();
      addEventListener.mock.calls[0][1]();
      addEventListener.mock.calls[0][1]();
      addEventListener.mock.calls[0][1]();
      addEventListener.mock.calls[0][1]();

      // Wait for the debounce to release
      await new Promise(resolve => setTimeout(() => resolve(), 150));

      // Expect three calls - the initial dispatch used to bind the listener, initial listener call,
      // and then one debounced call
      expect(dispatch).toHaveBeenCalledTimes(3);
      expect(dispatch).toHaveBeenCalledWith({ type: c.UPDATE_PAGE_Y_OFFSET, payload: 20 });
    });
  });

  it('unlistenForResize removes event listener', () => {
    dispatch(actions.unlistenForResize());

    expect(removeEventListener.mock.calls[0].length).toBe(2);
    expect(removeEventListener.mock.calls[0][0]).toEqual('resize');
  });

  it('unlistenForScroll removes event listener', () => {
    dispatch(actions.unlistenForScroll());

    expect(removeEventListener.mock.calls[0].length).toBe(2);
    expect(removeEventListener.mock.calls[0][0]).toEqual('scroll');
  });
});
