/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Redux actions for the device state
 * @copyright &copy; 2018 R. Matt McCann
 */

// @ts-ignore
import { debounce } from 'debounce';

import { Dispatch, GetState, IServices } from 'state/thunk.type';

import * as c from './device.constants';


let onResize: () => void;

let onScroll: () => void;

export const listenForResize = () => (dispatch: Dispatch, getState: GetState, services: IServices) => {
  onResize = () => {
    const innerWidth = services.window.innerWidth;

    dispatch(updateInnerWidth(innerWidth));
  };

  dispatch(updateInnerWidth(services.window.innerWidth));

  services.window.addEventListener('resize', onResize);
};

export const listenForScroll = () => (dispatch: Dispatch, getState: GetState, services: IServices) => {
  onScroll = debounce(() => {
    const pageYOffset = services.window.pageYOffset;

    dispatch(updatePageYOffset(pageYOffset));
  }, 100);

  dispatch(updatePageYOffset(services.window.pageYOffset));

  services.window.addEventListener('scroll', onScroll);
};

export const unlistenForResize = () => (dispatch: Dispatch, getState: GetState, services: IServices) => {
  services.window.removeEventListener('resize', onResize);
};

export const unlistenForScroll = () => (dispatch: Dispatch, getState: GetState, services: IServices) => {
  services.window.removeEventListener('scroll', onScroll);
};

const updateInnerWidth = (innerWidth: number) => ({ type: c.UPDATE_INNER_WIDTH, payload: innerWidth });

const updatePageYOffset = (pageYOffset: number) => ({ type: c.UPDATE_PAGE_Y_OFFSET, payload: pageYOffset });
