/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the Device reducer
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as c from './device.constants';
import { initialState, reducer } from './device.reducer';


describe('state/device/reducer', () => {
  it('initializes the state', () => {
    const expectedState = initialState();

    const newState = reducer(undefined, { type: 'NOT_HANDLED' });

    expect(newState).toEqual(expectedState);
  });

  it('reduces UPDATE_INNER_WIDTH', () => {
    const expectedState = Object.assign(initialState(), { innerWidth: 200 });

    const newState = reducer(undefined, { type: c.UPDATE_INNER_WIDTH, payload: 200 });

    expect(newState).toEqual(expectedState);
  });

  it('reduces UPDATE_PAGE_Y_OFFSET', () => {
    const expectedState = Object.assign(initialState(), { pageYOffset: 300 });

    const newState = reducer(undefined, { type: c.UPDATE_PAGE_Y_OFFSET, payload: 300 });

    expect(newState).toEqual(expectedState);
  });

  it('passes along state for unmatched action types', () => {
    const oldState = Object.assign(initialState(), { innerWidth: 200 });

    const newState = reducer(oldState, { type: 'NOT_HANDLED' });

    expect(newState).toEqual(oldState);
  });
});
