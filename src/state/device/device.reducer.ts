/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Redux reducer for the device state
 * @copyright &copy; 2018 R. Matt McCann
 */

import { IAction } from 'state/action.type';

import * as c from './device.constants';


/** Type definition for the device state */
export interface IDeviceState {
  /** The inner width of the window */
  innerWidth: number;

  /** The current scroll offset of the window */
  pageYOffset: number;
}

/** Initial state for the device state */
export const initialState = (): IDeviceState => ({
  innerWidth: 0,
  pageYOffset: 0,
});

/**
 * Reducer for the resource state.
 *
 * @param state The state we are updating
 * @param action The action we are reducing
 * @returns The updated state
 */
export const reducer = (state = initialState(), action: IAction): IDeviceState => {
  switch (action.type) {
    case c.UPDATE_INNER_WIDTH:
      return {
        ...state,
        innerWidth: action.payload,
      };

    case c.UPDATE_PAGE_Y_OFFSET:
      return {
        ...state,
        pageYOffset: action.payload,
      };

    default:
      return state;
  }
};
