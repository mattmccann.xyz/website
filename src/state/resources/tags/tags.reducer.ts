/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Redux reducer for the Tags resource
 * @copyright &copy; 2018 R. Matt McCann
 */

import { IAction } from 'state/action.type';

import * as c from './tags.constants';


export interface ITagsState {
  /** List of the currently active skill tags */
  value: string[],
}

/** Initial state for the feature */
export const initialState = (): ITagsState => ({
  value: [],
});

/**
 * Reducer for the resource state.
 *
 * @param state The state we are updating
 * @param action The action we are reducing
 * @returns The updated state
 */
export const reducer = (state = initialState(), action: IAction): ITagsState => {
  switch (action.type) {
    case c.ADD: {
      const tags = [...state.value];

      if (tags.indexOf(action.payload) === -1) {
        tags.push(action.payload);
      }

      tags.sort();

      return {
        value: tags,
      };
    }

    case c.REMOVE: {
      const tags = [...state.value];
      const index = tags.indexOf(action.payload);

      if (index !== -1) {
        tags.splice(index, 1);
      }

      return {
        value: tags,
      };
    }

    default:
      return state;
  }
};
