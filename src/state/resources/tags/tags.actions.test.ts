/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the Tags resource actions
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as actions from './tags.actions';
import * as c from './tags.constants';


describe('state/resources/tags/actions', () => {
  it('add', () => {
    const action = actions.add('myTag');

    expect(action).toEqual({ type: c.ADD, payload: 'myTag' });
  });

  it('remove', () => {
    const action = actions.remove('myTag');

    expect(action).toEqual({ type: c.REMOVE, payload: 'myTag' });
  });
});
