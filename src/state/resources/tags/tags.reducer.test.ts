/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the Tags resource reducer
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as actions from './tags.actions';
import { initialState, reducer } from './tags.reducer';


describe('state/resources/tags/reducer', () => {
  it('initializes the state', () => {
    const expectedState = initialState();
    const newState = reducer(undefined, { type: 'NOT_HANDLED' });

    expect(newState).toEqual(expectedState);
  });

  describe('reduces ADD', () => {
    it('adds new tag', () => {
      const expectedState = Object.assign(initialState(), { value: ['someTag'] });

      const newState = reducer(undefined, actions.add('someTag'));

      expect(newState).toEqual(expectedState);
    });

    it('maintains sorted order', () => {
      const expectedState = Object.assign(initialState(), { value: ['a', 'someTag', 'z'] });
      const oldState = Object.assign(initialState(), { value: ['a', 'z'] });

      const newState = reducer(oldState, actions.add('someTag'));

      expect(newState).toEqual(expectedState);
    });

    it('does not add duplicate tag', () => {
      const expectedState = Object.assign(initialState(), { value: ['someTag'] });
      const oldState = Object.assign(initialState(), { value: ['someTag'] });

      const newState = reducer(oldState, actions.add('someTag'));

      expect(newState).toEqual(expectedState);
    });
  });

  describe('reduces REMOVE', () => {
    it('removes existing tag', () => {
      const expectedState = Object.assign(initialState(), { value: ['a', 'b', 'd']});
      const oldState = Object.assign(initialState(), { value: ['a', 'b', 'c', 'd']});

      const newState = reducer(oldState, actions.remove('c'));

      expect(newState).toEqual(expectedState);
    });

    it('does not blow up on missing tag', () => {
      const expectedState = Object.assign(initialState(), { value: ['a', 'b', 'd']});
      const oldState = Object.assign(initialState(), { value: ['a', 'b', 'd']});

      const newState = reducer(oldState, actions.remove('c'));

      expect(newState).toEqual(expectedState);
    });
  });

  it('passes along state for unmatched action types', () => {
    const oldState = Object.assign(initialState(), { value: ['someTag'] });

    const newState = reducer(oldState, { type: 'NOT_HANDLED' });

    expect(newState).toEqual(oldState);
  });
});
