/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Redux actions for the Tags resource
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as c from './tags.constants';


/** Action handler triggered to add a selected skill tag */
export const add = (tag: string) => ({ type: c.ADD, payload: tag });

/** Action handler triggered to remove a selected skill tag */
export const remove = (tag: string) => ({ type: c.REMOVE, payload: tag });
