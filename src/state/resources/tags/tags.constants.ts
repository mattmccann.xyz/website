/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Redux constants for the Tags resource
 * @copyright &copy; 2018 R. Matt McCann
 */

export const ADD = 'TAGS_ADD';
export const REMOVE = 'TAGS_REMOVE';
