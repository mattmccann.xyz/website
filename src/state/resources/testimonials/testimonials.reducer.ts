/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Redux reducer for the Testimonials resource
 * @copyright &copy; 2018 R. Matt McCann
 */

import { ITestimonial } from 'services/api';
import { IListState, initialListState, listReducer } from 'state/crud-helpers'

import * as c from './testimonials.constants';


/** State type for the resource */
export interface ITestimonialsState extends IListState<ITestimonial> { }

/** Initial state for the resource */
export const initialState = initialListState<ITestimonial>();

/** State reducer for the resource */
export const reducer = listReducer<ITestimonial>(c.LIST_STARTED, c.LIST_FAILED, c.LIST_COMPLETED);
