/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the Testimonials resource reducer
 * @copyright &copy; 2018 R. Matt McCann
 */

import { ITestimonial } from 'services/api';

import * as c from './testimonials.constants';
import { initialState, reducer } from './testimonials.reducer';


describe('state/resources/testimonials/reducer', () => {
  it('initializes the state', () => {
    const expectedState = initialState();

    const newState = reducer(undefined, { type: 'NOT_HANDLED' });

    expect(newState).toEqual(expectedState);
  });

  it ('reduces LIST_COMPLETED', () => {
    const projects: ITestimonial[] = [{
      '$id': '0',
      author: 'Bob Villa',
      company: 'This Olde Home',
      photo: 'https://thisoldehome.org/bob',
      quote: 'The best, stupendous!',
      title: 'Handyman',
    }];
    const expectedState = Object.assign(initialState(), {
      list: {
        isComplete: true,
        isError: false,
        isInProgress: false,
      },
      value: projects,
    });

    const newState = reducer(undefined, { type: c.LIST_COMPLETED, payload: projects });

    expect(newState).toEqual(expectedState);
  });

  it('reduces LIST_FAILED', () => {
    const error = new Error('error!');
    const expectedState = Object.assign(initialState(), {
      list: {
        error,
        isComplete: true,
        isError: true,
        isInProgress: false,
      },
    });

    const newState = reducer(undefined, { type: c.LIST_FAILED, payload: error });

    expect(newState).toEqual(expectedState);
  });

  it('reduces LIST_STARTED', () => {
    const expectedState = Object.assign(initialState(), {
      list: {
        isComplete: false,
        isError: false,
        isInProgress: true,
      },
    });

    const newState = reducer(undefined, { type: c.LIST_STARTED });

    expect(newState).toEqual(expectedState);
  });

  it('passes along state for unmatched action types', () => {
    const oldState = Object.assign(initialState(), {
      list: {
        isComplete: true,
        isError: false,
        isInProgress: false,
      },
    });

    const newState = reducer(oldState, { type: 'NOT_HANDLED' });

    expect(newState).toEqual(oldState);
  });
});
