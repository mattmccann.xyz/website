/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Redux constants for the Testimonials resource
 * @copyright &copy; 2018 R. Matt McCann
 */

export const LIST_COMPLETED = 'TESTIMONIALS_LIST_COMPLETED';
export const LIST_FAILED = 'TESTIMONIALS_LIST_FAILED';
export const LIST_STARTED = 'TESTIMONIALS_LIST_STARTED';
