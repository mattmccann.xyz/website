/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Redux actions for the Testimonials resource
 * @copyright &copy; 2018 R. Matt McCann
 */

import { ITestimonial } from 'services/api';
import * as crudHelpers from 'state/crud-helpers';
import { IServices } from 'state/thunk.type';

import * as c from './testimonials.constants';


/**
 * Thunk that lists the resource records.
 */
export const list = crudHelpers.list<ITestimonial>(
  (services: IServices) => services.api.testimonials().list(),
  c.LIST_STARTED, c.LIST_FAILED, c.LIST_COMPLETED,
);
