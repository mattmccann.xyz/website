/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Redux reducer for the Projects resource
 * @copyright &copy; 2018 R. Matt McCann
 */

import { IProject } from 'services/api';
import { IListState, initialListState, listReducer } from 'state/crud-helpers'

import * as c from './projects.constants';


/** State type for the resource */
export interface IProjectsState extends IListState<IProject> { }

/** Initial state for the resource */
export const initialState = initialListState<IProject>();

/** State reducer for the resource */
export const reducer = listReducer<IProject>(c.LIST_STARTED, c.LIST_FAILED, c.LIST_COMPLETED);
