/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the Project resource state actions
 * @copyright &copy; 2018 R. Matt McCann
 */

import mockDispatch from 'mocks/mock-dispatch';
import mockServices from 'mocks/mock-services';
import { IServices } from 'state/thunk.type';

import * as actions from './projects.actions';
import * as c from './projects.constants';


describe('state/resources/projects/actions', () => {
  let dispatch: (arg: any) => void;
  let getState;
  let services: IServices;

  beforeEach(() => {
    getState = jest.fn();

    services = mockServices();
    (services.api.projects().list as any).mockReturnValue(Promise.resolve([]));

    dispatch = mockDispatch(getState, services);
  });

  describe('list', () => {
    it('immediately dispatches the started action', async () => {
      const listPromise = dispatch(actions.list());

      expect(dispatch).toHaveBeenCalledWith({ type: c.LIST_STARTED });

      await listPromise;
    });

    it('calls the list api', async () => {
      await dispatch(actions.list());

      expect(services.api.projects().list).toHaveBeenCalled();
    });

    describe('on success', () => {
      it('dispatches the completed action', async () => {
        await dispatch(actions.list());

        expect(dispatch).toHaveBeenCalledWith({
          payload: [], type: c.LIST_COMPLETED,
        });
      });
    });

    describe('on failure', () => {
      const error = new Error('error!');

      beforeEach(() => {
        (services.api.projects().list as any).mockReturnValue(Promise.reject(error));
      });

      it('dispatches the error action', async () => {
        try {
          await dispatch(actions.list());

          expect(false).toBe(true); // Should have propagated error
        } catch (caughtError) {
          expect(caughtError).toEqual(error);

          expect(dispatch).toHaveBeenCalledWith({
            payload: error, type: c.LIST_FAILED,
          });
        }
      });
    });
  });
});
