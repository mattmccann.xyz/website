/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Redux actions for the Projects resource
 * @copyright &copy; 2018 R. Matt McCann
 */

import { IProject } from 'services/api';
import * as crudHelpers from 'state/crud-helpers';
import { IServices } from 'state/thunk.type';

import * as c from './projects.constants';


/**
 * Thunk that lists the resource records.
 */
export const list = crudHelpers.list<IProject>(
  (services: IServices) => services.api.projects().list(),
  c.LIST_STARTED, c.LIST_FAILED, c.LIST_COMPLETED,
);
