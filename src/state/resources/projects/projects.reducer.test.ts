/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Unit tests for the Projects resource reducer
 * @copyright &copy; 2018 R. Matt McCann
 */

import { IProject } from 'services/api';

import * as c from './projects.constants';
import { initialState, reducer } from './projects.reducer';


describe('state/resources/projects/reducer', () => {
  it('initializes the state', () => {
    const expectedState = initialState();

    const newState = reducer(undefined, { type: 'NOT_HANDLED' });

    expect(newState).toEqual(expectedState);
  });

  it ('reduces LIST_COMPLETED', () => {
    const projects: IProject[] = [{
      $id: 'projectId2',
      backgroundImageType: 'png',
      backgroundImageUrl: 'https://imgs.com/backgroundImageUrl2',
      briefDescription: 'A brief description2',
      company: 'This Olde Home2',
      companyTitle: 'Chief Handyman2',
      dateRange: 'September 2050 - Present2',
      description: 'Something something lorem ipsum2',
      tags: ['juggling', 'striving', 'hacking2'],
      title: 'Rebuilt This Olde Home2',
    }];
    const expectedState = Object.assign(initialState(), {
      list: {
        isComplete: true,
        isError: false,
        isInProgress: false,
      },
      value: projects,
    });

    const newState = reducer(undefined, { type: c.LIST_COMPLETED, payload: projects });

    expect(newState).toEqual(expectedState);
  });

  it('reduces LIST_FAILED', () => {
    const error = new Error('error!');
    const expectedState = Object.assign(initialState(), {
      list: {
        error,
        isComplete: true,
        isError: true,
        isInProgress: false,
      },
    });

    const newState = reducer(undefined, { type: c.LIST_FAILED, payload: error });

    expect(newState).toEqual(expectedState);
  });

  it('reduces LIST_STARTED', () => {
    const expectedState = Object.assign(initialState(), {
      list: {
        isComplete: false,
        isError: false,
        isInProgress: true,
      },
    });

    const newState = reducer(undefined, { type: c.LIST_STARTED });

    expect(newState).toEqual(expectedState);
  });

  it('passes along state for unmatched action types', () => {
    const oldState = Object.assign(initialState(), {
      list: {
        isComplete: true,
        isError: false,
        isInProgress: false,
      },
    });

    const newState = reducer(oldState, { type: 'NOT_HANDLED' });

    expect(newState).toEqual(oldState);
  });
});
