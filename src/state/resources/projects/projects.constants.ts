/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Redux constants for the Projects resource
 * @copyright &copy; 2018 R. Matt McCann
 */

export const LIST_COMPLETED = 'PROJECTS_LIST_COMPLETED';
export const LIST_FAILED = 'PROJECTS_LIST_FAILED';
export const LIST_STARTED = 'PROJECTS_LIST_STARTED';
