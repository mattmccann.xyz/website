/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Reducer for the resources state
 * @copyright &copy; 2018 R. Matt McCann
 */

import { combineReducers } from 'redux';

import { IProjectsState, reducer as projects } from './projects/projects.reducer';
import { ITagsState, reducer as tags } from './tags/tags.reducer';
import { ITestimonialsState, reducer as testimonials } from './testimonials/testimonials.reducer';


export interface IResourcesState {
  projects: IProjectsState;
  tags: ITagsState;
  testimonials: ITestimonialsState;
}

export const reducer = combineReducers({
  projects,
  tags,
  testimonials,
});
