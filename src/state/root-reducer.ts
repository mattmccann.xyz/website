/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Root reducer for the redux state
 */

import { combineReducers } from 'redux';

import { reducer as features } from 'features/features.reducer';

import { reducer as device } from './device/device.reducer';
import { reducer as resources } from './resources/resources.reducer';


export default combineReducers({
  device,
  features,
  resources,
});
