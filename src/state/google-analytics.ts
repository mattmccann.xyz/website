/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Middleware that monitors the action stream and dispatches Google Analytics events
 */

import * as jsonpath from 'jsonpath';

import { IApi, IGaEvent } from 'services/api';
import GoogleAnalytics from 'services/google-analytics';


/**
 * This middleware compares the provided action against a map of action types to
 * analytics events, and dispatches if a match is found.
 *
 * @param api API interface used to fetch the map of redux action to google analytics event tags
 * @param ga Interface used to execute the google analytics events
 */
export default (api: IApi, ga: GoogleAnalytics) => {
  let gaEvents: Map<string, IGaEvent>;

  // Fetch the map of redux actions to google analytics event tags. No need to hold up the rest of the app
  // fetching the events map
  api.googleAnalytics().list().then((events) => {
    gaEvents = events;
  });

  return () => (next: any) => (action: any) => {
    // If this is thunk, just pass off its evaluation
    if (typeof action === 'function') {
      return next(action());
    }

    // If this action is mapped to a google analytics event
    if (gaEvents && gaEvents[action.type]) {
      let actionLabel = gaEvents[action.type].action;
      let categoryLabel = gaEvents[action.type].category;

      // If the action label is a JSONPath query, extract the label value
      if (actionLabel.startsWith('$')) {
        actionLabel = jsonpath.query(action, actionLabel);

        // JSONPath returns an array of results. If we've gotten results, let's extract
        actionLabel = actionLabel.length ? actionLabel[0] : 'nomatch';
      }

      // If the category label is a JSONPath query, extract the label value
      if (categoryLabel.startsWith('$')) {
        categoryLabel = jsonpath.query(action, categoryLabel);

        // JSONPath returns an array of results. If we've gotten results, let's extract
        categoryLabel = categoryLabel.length ? categoryLabel[0] : 'nomatch';
      }

      // Dispatch the analytics event
      ga.sendEvent(categoryLabel, actionLabel);
    }

    if (action.type === '@@router/LOCATION_CHANGE') {
      ga.sendPageView(action.payload.location.pathname);
    }

    return next(action);
  };
}
