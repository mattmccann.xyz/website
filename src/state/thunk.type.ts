/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Thunk type definition for the app state
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as React from 'react';

import { IApi } from 'services/api';

import { IAction } from './action.type';
import { IState } from './state.type';

export type Dispatch = (actionOrThunk: IAction | ThunkAsync | ThunkSync) => any;

export type GetState = () => IState;

export interface IReactDOM {
  findDOMNode: (node: React.ReactNode) => HTMLElement;
}

export interface IScroller {
  /**
   * Triggers the browser to scroll to the react-scroll element with the provided label.
   *
   * @param scrollElement The "name" prop of the react-scroll element to bring into view
   * @param options @see react-scroll
   */
  scrollTo: (scrollElement: string, options: any) => void;
}

export interface IServices {
  api: IApi;
  document: Document;
  reactDOM: IReactDOM;
  scroller: IScroller;
  window: Window;
}

export type ThunkAsync = (dispatch: Dispatch, getState: GetState, services: IServices) => Promise<any>;

export type ThunkSync = (dispatch: Dispatch, getState: GetState, services: IServices) => any;
