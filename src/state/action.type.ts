/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief Action type definition for the app state
 * @copyright &copy; 2018 R. Matt McCann
 */

export interface IAction {
  /** Contents of the action */
  payload?: any;

  /** The action type label */
  type: string;
}



