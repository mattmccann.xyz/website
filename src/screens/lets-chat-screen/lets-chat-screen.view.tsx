/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief View for the LetsChat screen
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as React from 'react';

import { Footer } from 'features/footer';
import { LetsChat } from 'features/lets-chat';
import { Menu } from 'features/menu';


export default () => (
  <div>
    <Menu isBackMode />
    <LetsChat />
    <Footer />
  </div>
);
