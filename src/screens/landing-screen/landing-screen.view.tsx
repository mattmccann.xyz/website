/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief View for the Landing screen
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as React from 'react';

import { About } from 'features/about';
import { Companies } from 'features/companies';
import { FinalCta } from 'features/final-cta';
import { Footer } from 'features/footer';
import { HeroBanner } from 'features/hero-banner';
import { Menu } from 'features/menu';
import { MyWork } from 'features/my-work';
import { Testimonials } from 'features/testimonials';


export default () => (
  <div>
    <Menu />
    <HeroBanner />
    <About />
    <Companies />
    <MyWork />
    <Testimonials />
    <FinalCta />
    <Footer isOverlayedMode />
  </div>
);
