/**
 * @blame R. Matt McCann <me@mattmccann.xyz>
 * @brief View for the 404 screen
 * @copyright &copy; 2018 R. Matt McCann
 */

import * as React from 'react';


export default () => (
  <div>404!</div>
);
