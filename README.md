# The MattMcCann.xyz Portfolio Site

Greetings and welcome. This portfolio site was built using React+Redux and leverages Firebase as the data source.

To stand up the site locally:

```
npm install
npm run start
```

To run tests and evaluate coverage:

```
# Run just the test without waatching
npm run test-once 

# Collects and reports coverage
npm run coverage 
```

To build for production and serve locally:

```
npm run build
npm run start-prod
```

To deploy (really ought not to be done manually - that's what Gitlab's CI/CD is for!)

```
npm run deploy
```
